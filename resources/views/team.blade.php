<!doctype html>
<html lang="en">
<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->
    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">

    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <style>
        .page-content-wrap {
            padding: 75px 0;
        }


        .tribe-events-category-tech-events .tribe-events-tooltip {
            position: absolute;
            bottom: 20%;
            left: 0;
            margin-left: -2px;
            padding: 20px;
            background: #fff;
            color: #777;
            text-align: left;
            width: 420px;
            z-index: 1000;
            opacity: 0;
            visibility: hidden;
            transition: all .4s ease;
            -webkit-box-shadow: 0px 8px 15px 0px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 8px 15px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 8px 15px 0px rgba(0, 0, 0, 0.1);
        }


    </style>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Our Team</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Our Team</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->


    <div id="content" class="page-content-wrap">


        <div class="container">

            {{--            <div class="content-element5">--}}
            {{--                <h3 class="align-center">What We Offer</h3>--}}

            <p class="content-element4">
                We are a collective group that have at their core the philosophy that wellbeing starts at the
                physical and culminates in the transcendental! Each member of the team is on a spiritual path of
                discovery. We feel that we could help empower others through this collective to understand how to
                live life in tune with cosmic rhythm and balance....
            </p>
            {{--            </div>--}}

            <?php

            $team = [
                [
                    'file' => 'pravin.png', 'name' => 'PRAVIN GOENKA',
                    'description' => 'Pravin is passionate about wellness. He wished to realize his dream of a wellness project with a team of like minded individuals to structure a cohesive team. He practices yoga and meditation in his personal time. The Honorary Consul General of the Republic of Estonia in Karnataka and a Businessman too, Pravin is the main driving force at Rtambhara.',
                    'role' => 'Managing Director and Key Promoter at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'rohan.png', 'name' => 'ROHAN NAIK',
                    'description' => 'Rohan is a keen spiritual learner, and is aligned to Rtambhara to define his personal spiritual path. He has been meeting with several gurus and visited spaces of peace and spirituality in the last few years. He plays a key role in management, strategy and operations at Rtambhara. In his personal capacity, he is a successful businessman in the engineering industry.',
                    'role' => 'Director of Operations at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'vijay.png', 'name' => 'VIJAY SARAF',
                    'description' => 'Vijay is a regular yoga practitioner for over 15 years, and deeply spiritual. A chartered accountant by training, Vijay will be pivotal in creating value through financial strategy. As a businessman, he has a robust business in the space of petroleum and chemical dealings and imports.',
                    'role' => 'Director of Finance at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'anoop.png', 'name' => 'ANOOP SHARMA',
                    'description' => 'Anoop is a supporter of the Rtambhara way of life. He believes in the cause of wellness as a practitioner himself of yoga, vipasana and travels to other spiritual retreats. He is the director of one of India’s largest spice manufacturing companies.',
                    'role' => 'Promoter at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'premnath.png', 'name' => 'PREMNATH',
                    'description' => 'Prem has had a deep interest in spirituality and yoga for a long time. He has visited several spiritual energy centers across the world. Prem will create robust models for marketing and creating and optimizing IT solutions at Rtambhara. With over two decades of business acumen in the IT industry, he plays a key role at Rtambhara.',
                    'role' => 'Director of Marketing and IT at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'suchit.png', 'name' => 'SUCHIT JHUNJHUNWALA',
                    'description' => 'Suchit is embarking on a journey of spiritual learning. He is the finance brain in the team at Rtambhara. His office will be involved in an financial operational capacity and be pivotal to the modeling and success of Rtambhara. A practicing chartered accountant, Suchit is vital to the group.',
                    'role' => 'Financial Controller at Rtambhara', 'category' => 'promoter'],
                [
                    'file' => 'karnan.png', 'name' => 'KARAN VAIDYA',
                    'description' => 'Karan has completed his MIT MBA Thesis on the Moksh Model that incorporates wellness through ancient teachings, employee centric hospitality and Green Architecture. He will be collaborating with the team at Rtambhara to further our vision in Nepal, where he is The Honorary Consul General of the Republic of Latvia.',
                    'role' => 'Collaborator at Rtambhara', 'category' => 'promoter'],

                [
                    'file' => 'khrishnapani.png', 'name' => 'KRISHNAPHANI KESIRAJU',
                    'description' => 'Driving the spiritual direction at Rtambhara, and modernizing ancient scripture to make it contextual to today’s living, Krishnaphani Kesiraju is the Program director and Chief Faculty at Rtambhara. He has forty years of experience in industry and academia. Study and research in Ancient Indian philosophical and spiritual literature are his passion. He is a keen student of Vedanta, Theosophy, and Jiddu Krishnamurthi’s teachings. He is instrumental to the shape and form the courses will take.',
                    'role' => 'Program Director at Rtambhara', 'category' => 'faculty'],
                [
                    'file' => 'asit.png', 'name' => 'ASIT GHOSH',
                    'description' => 'Driving the relationship between spirituality and business at Rtambhara, Asit is an accomplished corporate trainer with an experience over two decades. He uses multiple techniques including spiritual modules. Asit has led several national and international retreats. Trained under Deepak Chopra , Eckhart Tolle, Ramdass, he comes with the newest developments on the Spiritually oriented wellness and healing programs. He is one of our lead facilitators and banks of wisdom at Rtambhara.',
                    'role' => 'Chief Faculty at Rtambhara', 'category' => 'faculty'],
                [
                    'file' => 'bina.png', 'name' => 'BINA MIRCHANDANI',
                    'description' => 'Driving the relationship between spirituality and happiness at Rtambhara, Bina has experience of over 40 years in brand & retail strategy , combined with 25 years of study in Spiritual teachings. She also serves on the board of the "School Of Ancient Wisdom" in Bangalore. She has conducted "Happiness Differential“ workshops /sessions for well known corporates, institutions and social groups. The beacon of a happy mindset and a spiritual life, Bina is Chief Faculty at Rtambhara.',
                    'role' => 'Chief Faculty at Rtambhara', 'category' => 'faculty'],
                [
                    'file' => 'umesh.jpg', 'name' => 'UMESH DHAGAT',
                    'description' => 'Our specialist on the practice of Yoga, Umesh has had a unique spiritual journey. In the year 2006, Umesh was guided by a voice to give up all material attachments. As a result he gave up his flourishing business and devoted himself full time to sharing his knowledge of Yoga. He has been practicing Yoga and Sahaj Marg Meditation since 1977. At Present, he teaches Yoga, Meditation and the rest of his time is devoted to Satsangs, gaining and spreading spiritual knowledge. At Rtambhara, he will be delivering dialect and practice of yoga pranayama and meditation.',
                    'role' => 'Faculty for Yoga at Rtambhara', 'category' => 'faculty'],

                [
                    'file' => 'vecram.png', 'name' => 'DR VECRAM ADDITHYEN',
                    'description' => 'The Ayurveda consultant at Rtambhara, Dr Vecram Addithyen is the founder at Ufa World Clinics in Bangalore and the Royal Academy of Home & Earth Management in India. He has over 14 years of experience in the medical and management fields, with particular reference to new research in neuroscience.',
                    'role' => 'Ayurveda Consultant at Rtambhara', 'category' => 'consultant'],
                [
                    'file' => 'antara.png', 'name' => 'ANTARA HAZARIKA',
                    'description' => 'Antara is the nutritional consultant and wellness chef at Rtambhara. She is an urban farmer and slow food activist who seeks to create a Healthy Food Revolution one plate at a time. Antara has been immersed in the interdisciplinary wellbeing fields of Yoga, Ayurveda and Kalarippyyattu since 2005.',
                    'role' => 'Nutritional Consultant at Rtambhara', 'category' => 'consultant'],
                [
                    'file' => 'anna.png', 'name' => 'ANNA ILINA',
                    'description' => 'The Spiritual Dance Consultant at Rtambhara, Anna has had a unique journey in her search for spirituality which has lead her to India and the team at Rtambhara. Yoga and Wellbeing are the core philosophy behind her dance practice at Rtambhara. She will be teaching participants to connect inward through the medium of dance.',
                    'role' => 'Spiritual Dance Consultant at Rtambhara', 'category' => 'consultant'],
                [
                    'file' => 'anushka.png', 'name' => 'ANUSHKA KALRO',
                    'description' => 'Anushka is the visual designer at Rtambhara. She practices yoga, art therapy, creative problem solving, and mindfulness techniques. She has a background in   mutli-disciplinary design, illustration, and entrepreneurship for emerging design economies.',
                    'role' => 'Design Consultant at Rtambhara', 'category' => 'consultant'
                ],
                [
                    'file' => 'achal_mehra.png', 'name' => 'ACHAL MEHRA',
                    'description' => 'Achal, an expert in Hatha Vinyasa has been practicing yoga for over 30 years. An engineer and MBA from IIM-A, he worked in senior management positions with Pepsi, Sony and GE and was CEO of AXN in South Asia.  His practice and teachings focus on breathing, awareness, acceptance and own body intelligence techniques to deepen the postures and experience meditation through them. At Rtambhara, he will contribute to the physical, emotional, and spiritual wellness of the community. ',
                    'role' => 'Yoga Consultant at Rtambhara', 'category' => 'consultant'
                ],
                [
                    'file' => 'suchitra_mitra.jpg', 'name' => 'SUCHITRA MITRA',
                    'description' => 'Suchitra Mitra, an accomplished dancer, teacher and choreographer, has specialized in Bharatanatyam and is in the field for over three decades. Besides being an accomplished dancer trained by some of the finest artists in the country, she is also a Hindustani Classical vocalist. Suchitra was felicitated as the ‘Woman of the Year’ in 2011, 2012, and 2016; and was awarded “KALA GURU SAMMAN” for her contribution in the world of Bharatanatyam. Culture, Arts and wellness are some of the things Suchitra brings to Rtambhara Wellness.',
                    'role' => 'Culture and Arts Consultant at Rtambhara', 'category' => 'consultant'
                ], [
                    'file' => 'raghu.jpg', 'name' => 'ACHARYA RAGHU GANAPATHY',
                    'description' => 'Acharya Ganapathy  is a scholar, practitioner and teacher of the Advaita (non-dual) Vedanta philosophy, the spiritual knowledge expounded in the Upanishads and Bhagawad Gita. He has formally learnt Veda mantras from Sringeri Shankara Mutt. Acharya Ganapathy has undergone intensive learnings on Vedanta and Advanced Sanskrit from Arsha Vidya Gurukulam, Coimbatore, the study included the Sanskrit commentaries of Adi Shankaracharya on the Vedantic texts and Bhagawad Gita. Along with these studies he learnt Sanskrit language with Paninian grammar. ',
                    'role' => 'Advaita Vedanta Philosophy Consultant at Rtambhara', 'category' => 'consultant'
                ], [
                    'file' => 'sneha.jpg', 'name' => 'SNEHA DEVANANDAN',
                    'description' => 'Sneha Devanandan is a flawless dancer, choreographer and teacher. She is one of the senior disciples of Natyarani Shanthala and Late Guru Mrs Padmini Ramachandran, Natyapriya Bangalore. She is an up and coming name in dance on the global front. She wishes to continue her journey in dance all her life, exploring all facets of art to provide humble contribution towards this divine dance form. At Rtambhara, she will contribute to the cultural immersion of the community.',
                    'role' => 'Culture and Arts Consultant at Rtambhara', 'category' => 'consultant'
                ],


            ];




            ?>

            <div id="options">
                <div id="filters" class="isotope-nav">
                    <button class="is-checked" data-filter="*">All</button>
                    <button data-filter=".promoter">Promoters/Collaborators</button>
                    <button data-filter=".faculty">Faculty</button>
                    <button data-filter=".consultant">Consultants</button>
                    {{--                    <button data-filter=".category_5">Vinyasa</button>--}}
                    {{--                    <button data-filter=".category_6">Yin</button>--}}
                </div>
            </div>

            <!-- Isotope -->
            <div class="isotope three-collumn clearfix team-holder"
                 data-isotope-options='{"itemSelector" : ".item","layoutMode" : "fitRows","transitionDuration":"0.7s","fitRows" : {"columnWidth":".item"}}'>

                <!-- Isotope item -->
                <?php foreach ($team as $member) {

                ?>
                <div class="item <?= $member['category'] ?>">

                    <div class="team-item tribe-events-category-tech-events">

                        <a href="#" class="member-photo">
                            <img src="/images/team/<?= $member['file']?>" alt="">
                        </a>
                        <div class="team-desc">
                            <div class="member-info">
                                <h5 class="member-name"><a href="#"><?=$member['name']?></a></h5>
                                <h6 class="member-position"><?=$member['role']?></h6>
                            </div>
                        </div>

                        <div class="tribe-events-tooltip" style="width: inherit">
                            <header>
                                <h6>{{$member['name']}}</h6>
                                <time datetime="2018-07-27">{{$member['role']}}</time>
                            </header>
                            <div class="clearfix">
                                <img src="/images/team/{{$member['file']}}"
                                     alt="" class="alignleft" height="130">
                                <p>{{$member['description']}}</p>
                            </div>
                        </div>

                        {{--                        <div class="tribe-events-tooltip">--}}
                        {{--                            <div class="clearfix">--}}
                        {{--                                <p>{{$member['description']}}</p>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>

                </div>

                <?php } ?>
                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_2">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img2.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Bradley Grosh</a></h5>--}}
                {{--                                <h6 class="member-position">Ashtanga Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_6">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img3.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Stella Healy</a></h5>--}}
                {{--                                <h6 class="member-position">Yin &amp; Vinyasa Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_4">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img4.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Olivia Franklin</a></h5>--}}
                {{--                                <h6 class="member-position">Hatha Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_5">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img5.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Alan Smith</a></h5>--}}
                {{--                                <h6 class="member-position">Vinyasa Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_3">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img6.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Inga North</a></h5>--}}
                {{--                                <h6 class="member-position">Gentle Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_6">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img7.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">John McCoist</a></h5>--}}
                {{--                                <h6 class="member-position">Yin & Vinyasa Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_2">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img8.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Caroline Beek</a></h5>--}}
                {{--                                <h6 class="member-position">Ashtanga Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

                {{--                <!-- Isotope item -->--}}
                {{--                <div class="item category_4">--}}

                {{--                    <div class="team-item">--}}

                {{--                        <a href="#" class="member-photo">--}}
                {{--                            <img src="/themes/wellness/images/360x390_img9.jpg" alt="">--}}
                {{--                        </a>--}}
                {{--                        <div class="team-desc">--}}
                {{--                            <div class="member-info">--}}
                {{--                                <h5 class="member-name"><a href="#">Alise Puse</a></h5>--}}
                {{--                                <h6 class="member-position">Hatha Yoga Teacher</h6>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}

                {{--                    </div>--}}

                {{--                </div>--}}

            </div>

            {{--            <div class="align-center">--}}
            {{--                <ul class="pagination">--}}
            {{--                    <li><a href="#" class="prev-page"></a></li>--}}
            {{--                    <li><a href="#">1</a></li>--}}
            {{--                    <li class="active"><a href="#">2</a></li>--}}
            {{--                    <li><a href="#">3</a></li>--}}
            {{--                    <li><a href="#" class="next-page"></a></li>--}}
            {{--                </ul>--}}
            {{--            </div>--}}

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

    <div id="popup-sign" class="popup var3">

        <div class="popup-inner">

            <button type="button" class="close-popup"></button>

            <h4 class="title">Sign Up For Free</h4>
            <p>Already have an account? <a href="#" class="link-text var2 popup-btn-login">Login Here</a></p>
            <a href="#" class="btn fb-btn btn-big">Sign Up With Facebook</a>
            <p class="content-element2">OR</p>
            <form class="content-element2">

                <input type="text" placeholder="Enter Your Email Address">
                <input type="text" placeholder="Password">
                <a href="#" class="btn btn-style-3 btn-big">Join Free Now!</a>

            </form>

            <p class="text-size-small">By signing up you agree to <a href="#" class="link-text">Terms of Service</a></p>

        </div>

    </div>

    <div id="popup-login" class="popup var3">

        <div class="popup-inner">

            <button type="button" class="close-popup"></button>

            <h4 class="title">Login</h4>
            <p>Don't have an account yet?<a href="#" class="link-text var2 popup-btn-sign">JOIN FOR FREE</a></p>
            <a href="#" class="btn fb-btn btn-big"> Login With Facebook</a>
            <p class="content-element2">OR</p>
            <form class="content-element1">

                <input type="text" placeholder="Enter Your Email Address">
                <input type="text" placeholder="Password">
                <a href="#" class="btn btn-style-3 btn-big">Login</a>

                <div class="input-wrapper">
                    <input type="checkbox" id="checkbox" name="checkbox">
                    <label for="checkbox">Remember me</label>
                </div>

            </form>

            <p class="text-size-small"><a href="#" class="link-text">Forgot your password?</a></p>

        </div>

    </div>

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
