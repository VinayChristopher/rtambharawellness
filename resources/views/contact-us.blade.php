<!doctype html>
<html lang="en">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

<!-- Basic Page Needs
================================================== -->

<title>Rtambhara Wellness</title>

<!--meta info-->
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

<link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/themes/wellness/css/fontello.css">
<link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
<link rel="stylesheet" href="/themes/wellness/css/style.css">
<link rel="stylesheet" href="/themes/wellness/css/responsive.css">
<link rel="stylesheet" href="/themes/wellness/css/custom.css">

<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: red;
        opacity: 1; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: red;
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
        color: red;
    }
    .contact-form .contact-item button.btn.btn-success.btn-refresh {
        border-radius: 50% !important;
        padding: 2px;
        letter-spacing: 0px;
        line-height: 17px;
        text-align: center;
    }
    .contact-form .contact-item button.btn.btn-success.btn-refresh i{
        margin-right: 2px !important;
    }
</style>
</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap banner-3" style="margin-top: 10%">

        <div class="container">
            <h1 class="page-title">Contact Us</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Contact Us</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" style="margin-top: 10px;">

        <div class="map-section content-element10">

            {{--        <div id="googleMap2" class="map-container"></div>--}}

            <div class="container">

                <div class="map-info">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">

                            <h4 class="title">UK</h4>

                            <div class="our-info vr-type">


                                <div class="info-item">
                                    <i class="licon-map-marker"></i>
                                    <span class="post">Postal Address</span>
                                    <a href="https://goo.gl/maps/7HK9k6GoKhKL5JGX9">
                                        <h8>
                                            9 Drakes Mews, Milton Keynes, MK8 0ER
                                        </h8>
                                    </a>
                                    {{--                                    <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249"--}}
                                    {{--                                       class="link-text">Get Direction</a>--}}
                                </div>
                                <div class="info-item">
                                    <i class="licon-telephone"></i>
                                    <span class="post">Phone Number</span>
                                    <h8 content="telephone=no">TBC</h8>
                                </div>
                                <div class="info-item">
                                    <i class="licon-at-sign"></i>
                                    <span class="post">Email Address</span>
                                    <h8><a href="mailto:info@rtambharawellness.com">info@rtambharawellness.com</a></h8>
                                </div>
                                <div class="info-item">
                                    <i class="licon-clock3"></i>
                                    <span class="post">Opening Hours</span>
                                    <h8>Monday - Saturday: <br> 8am - 9pm</h8>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-6 col-sm-12">

                            <h4 class="title">India</h4>

                            <div class="our-info vr-type">

                                <div class="info-item">
                                    <i class="licon-map-marker"></i>
                                    <span class="post">Postal Address</span>
                                    <a href="https://goo.gl/maps/7Rympiy3joLbheWt8">
                                        <h8>

                                            RTAMBHARA WELLNESS PRIVATE LIMITED. <br>No.6,M.G. Road, <br>Mittal Towers,
                                            <br>Block “B”-
                                            809. <br>Bangalore-560001, <br> India.

                                        </h8>
                                    </a>
                                    {{--                                    <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249"--}}
                                    {{--                                       class="link-text">Get Direction</a>--}}
                                </div>
                                <div class="info-item">
                                    <i class="licon-telephone"></i>
                                    <span class="post">Mobile/Whatsapp</span>
                                    <h8 content="telephone=no"><a href="#">+918431135431</a></h8>
                                </div>
                                <div class="info-item">

                                    <i class="licon-telephone"></i>
                                    <span class="post">Landline</span><br>
                                    <h8 content="telephone=no"><a href="#">+918040999096 </a></h8>
                                </div>
                                <div class="info-item">
                                    <i class="licon-at-sign"></i>
                                    <span class="post">Email Address</span>
                                    <h8><a href="mailto:info@rtambharawellness.com">info@rtambharawellness.com</a></h8>
                                </div>
                                <div class="info-item">
                                    <i class="licon-clock3"></i>
                                    <span class="post">Opening Hours</span>
                                    <h8>Monday - Saturday: <br> 8am - 9pm</h8>
                                </div>

                            </div>

                        </div>
{{--                        <div class="col-md-4 col-sm-12">--}}

{{--                            <h4 class="title">Far East Asia</h4>--}}

{{--                            <div class="our-info vr-type">--}}

{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-map-marker"></i>--}}
{{--                                    <span class="post">Postal Address</span>--}}
{{--                                    <h8>TBC</h8>--}}
{{--                                    --}}{{--                                    <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249"--}}
{{--                                    --}}{{--                                       class="link-text">Get Direction</a>--}}
{{--                                </div>--}}
{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-telephone"></i>--}}
{{--                                    <span class="post">Phone Number</span>--}}
{{--                                    <h8 content="telephone=no">TBC</h8>--}}
{{--                                </div>--}}
{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-at-sign"></i>--}}
{{--                                    <span class="post">Email Address</span>--}}
{{--                                    <h8><a href="mailto:info@rtambharawellness.com">info@rtambharawellness.com</a></h8>--}}
{{--                                </div>--}}
{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-clock3"></i>--}}
{{--                                    <span class="post">Opening Hours</span>--}}
{{--                                    <h8>Monday - Saturday: <br> 8am - 9pm</h8>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}
                    </div>
                </div>

            </div>

        </div>

        <div class="page-section parallax-section text-color-light" data-bg="/images/alvaro-unsplash.jpg" id="contact">

            <div class="container">

                <div class="content-element5">
    @if(Session::has('flash_message'))
        <div class="alert alert-success flashAlert">
            {{ Session::get('flash_message') }}
        </div>
    @endif
                    <div class="section-pre-title">have a question?</div>
                    <h2 class="section-title">Contact Us</h2>

                </div>

                <p class="content-element3">Feel free to send us any questions you may have. We are happy to answer
                    them.</p>
                <form id="contact-form" class="contact-form" method="POST">

                    {{csrf_field()}}

                    <div class="contact-item">

                        <label>Your Name (required)</label>
                        <input type="text" class="cf-name" name="cf-name" required="">

                    </div>

                    <div class="contact-item">

                        <label>Your Email (required)</label>
                        <input type="email" class="cf-email" name="cf-email" required="">

                    </div>

                    <div class="contact-item">

                        <label>Subject</label>
                        <input type="text" name="cf-subject">

                    </div>


                    <div class="contact-item">

                        <label>Inquiry Type</label>

                        <select type="text" name="cf-type" id="cf-type"
                                style="height: 35px; !important; font-size: medium">

                            <option value="general">General Request</option>
                            <option value="wellness-program">Rtambhara Wellness Program</option>

                        </select>

                    </div>

                    <div class="contact-item general" id="program" style="display: none">

                        <label>Programme </label>
                        <input type="text" name="cf-program" placeholder="Programme you are interested in? eg. Corporate - Spandana Programme or Retreat - Pravara Programme">

                    </div>

                    <div class="contact-item general" id="venue" style="display: none">

                        <label>Venue</label>
                        <input type="text" name="cf-venue"
                               placeholder="Which venue you are interested in? ">

                    </div>
                    <div class="contact-item general" id="participants" style="display: none">

                        <label>Number of participants.</label>
                        <input type="text" name="cf-participants" placeholder="Estimated number of participants">
                    </div>

                    <div class="contact-item">

                        <label>Your Message</label>
                        <textarea rows="4" name="cf-message"></textarea>
                        <span id="message-extra"
                              style="display:none;"> *Any special requirements / health needs / etc.</span>

                        {{--                        <span style="color: #FFFFFF">--}}
                        {{--                            *Please provide the following details when making an enquiry so that we can provide you with all the relevant information:--}}
                        {{--                            <ol style="list-style: ">--}}
                        {{--                                <li>Programme you are interested in (For example: Corporate - Spandana Programme or Retreat - Pravara Programme).</li>--}}
                        {{--                                <li> Which venue you are interested in.</li>--}}
                        {{--                                <li>Estimated number of participants.</li>--}}
                        {{--                                <li> Any special requirements / health needs / etc.</li>--}}
                        {{--                            </ol>--}}
                        {{--                            Thank you Rtambhara Wellness Team</span>--}}
                    </div>


                    <div class="contact-item">
                        <label>Captcha</label>
                        <input id="captcha" type="text" class="captchaForm form-control"
                               placeholder="Enter Captcha"
                               name="captcha" required>
                    </div>


                    <div class="contact-item captcha">
                        <span>{!! captcha_img('flat') !!}</span>
                        <button type="button" id="submitButton" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
                    </div>

                    <div class="contact-item">
                        <button type="submit" class="btn btn-style-3" data-type="submit">Submit</button>
                    </div>
                </form>

            </div>

        </div>

    </div>
    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

    @include('layouts.footer')

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

<script>
    document.addEventListener('input', function (event) {

        // Only run on our select menu
        if (event.target.id !== 'cf-type') return;

        if (event.target.value === 'general') {
            document.getElementById('program').style.display = 'none';
            document.getElementById('venue').style.display = 'none';
            document.getElementById('participants').style.display = 'none';
            document.getElementById('message-extra').style.display = 'none';

        } else {
            document.getElementById('program').style.display = 'block';
            document.getElementById('venue').style.display = 'block';
            document.getElementById('participants').style.display = 'block';
            document.getElementById('message-extra').style.display = 'block';
        }
        // Do stuff...

    }, false);
    $(document).ready(function(){
        $(".flashAlert").delay(5000).slideUp(300);
    });
</script>
</body>
</html>
