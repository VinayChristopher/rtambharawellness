<!doctype html>
<html lang="en">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->


    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <script src="https://unpkg.com/vue/dist/vue.min.js"></script>
    {{--    <script src="https://unpkg.com/vue-stripe-checkout/build/vue-stripe-checkout.js"></script>--}}


    <style>
        .custom-list {
            list-style: disc !important;
        }

        .custom-list > li:not(:last-child) {
            margin-bottom: 0;
        }
    </style>

    <script src="/js/cal.js"></script>
    <script>
        function dismiss(el) {
            el.parentNode.style.display = 'none';
        };
    </script>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap no-title" style="margin-top:10%">

        <div class="container">

            <ul class="breadcrumbs">

                <li><a href="index.html">Home</a></li>
                <li>Locations</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

        <div class="container">

            <div class="content-element">

                <div class="content-element4">
                    <div class="page-nav">
                        <div class="flex-item"><a href="#" class="info-btn prev-btn">All Events</a></div>
                    </div>
                </div>

                <div class="content-element5">
                    <div class="entry-box single-post">

                        <div class="entry">

                            <div class="content-element4">
                                <div class="entry-body">

                                    <h1 class="entry-title">Heading</h1>

                                </div>
                            </div>

                            <div class="content-element2">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12"><img src="image" alt="">
                                    </div>
                                    {{--                                    <div class="col-lg-4 col-md-12">--}}
                                    {{--                                        <div id="googleMap" class="map-container"></div>--}}
                                    {{--                                    </div>--}}
                                </div>

                            </div>

                            <div>
                                <p class="content-element3">
                                    KAIVALYA YOGA is established aiming to achieve Supreme Understanding spiritually. We
                                    are using total efforts to find the goal – ultimate realization. The word Kaivalya
                                    is ultimate realization as well, the wholeness. Kaivalya Yoga is created to balance
                                    life with as both legs walking; spiritual journey with worldly walks, does not
                                    matter what and wherever we are, synchronizing and connecting seeing that total
                                    being.
                                </p>
                                <p class="content-element3">
                                    Kaivalya Yoga is a company of very spiritual practitioners and Masters along this,
                                    well known spiritual and ritual researchers in different ethnical communities of
                                    Nepal.
                                </p>
                                <p class="content-element3">
                                    Kaivalya Yoga arranges and organizes Yoga and Meditation courses for beginners,
                                    intermediate and advanced practitioners separately with well known instructors
                                    experienced of 15-20 years.
                                </p>
                                <p class="content-element3">
                                    Also we are authorized, registered and experienced with Yoga Alliance US, every
                                    month we organize Hatha Yoga and Asthanga Yoga Teacher Training.
                                </p>
                                <p class="content-element3">
                                    We set Meditation classes for very spiritual seekers as Vipassana Meditation,
                                    Buddhist Meditation, Osho’s Meditation, Sanatana and Vedic Meditation.
                                </p>
                                <p class="content-element3">
                                    We are specialized in Nada Yoga, we have very special Professional Spiritual Sound
                                    Healing Courses in different levels as well Tibetan way of Sound Healing and Singing
                                    Bowl Courses.
                                </p>
                                <p class="content-element3">
                                    We have other unique programs as Traditional Usui Reiki Ryoho, Shamanic Tours,
                                    Cultural Tours and Researches.
                                </p>

                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list"
                                                style="columns: 2;margin-bottom: 0;  -webkit-columns: 2;-moz-columns: 2;">
                                                <li>Yoga</li>
                                                <li>Meditation</li>
                                                <li>Teacher Training</li>
                                                <li>Spiritual Tours</li>
                                                <li>Hiking</li>
                                                <li>Research,Study & Practice</li>
                                                <li>Philosophical Study</li>

                                            </ul>
                                            <ul class="custom-list" style="">
                                                <li>Website: <a href="http://kaivalyayoganepal.com/"
                                                                target="_blank"
                                                                class="link-text">http://kaivalyayoganepal.com</a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>KAIVALYA YOGA Kathmandu 44600</p>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <h4>Gallery</h4>

                <!-- Isotope -->
                <div class="portfolio-holder isotope fourth-collumn clearfix"
                     data-isotope-options='{"itemSelector" : ".item","layoutMode" : "masonry","transitionDuration":"0.7s","masonry" : {"columnWidth":".item"}}'>

                    <!-- Isotope item -->

                    @for($i=1 ;$i<6 ;$i++)
                        <div class="item goa ">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/goa{{$i}}.jpg" alt="">

                                    <a href="/images/locations/goa{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor

                    @for($i=6 ;$i<10 ;$i++)
                        <div class="item devaaya ">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/goa{{$i}}.jpg" alt="">

                                    <a href="/images/locations/goa{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor

                    @for($i=13 ;$i<21 ;$i++)
                        <div class="item shathayu ">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/soa{{$i}}.jpg" alt="">

                                    <a href="/images/locations/soa{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor

                    @for($i=1 ;$i<15 ;$i++)
                        <div class="item srilanka">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/sl{{$i}}.jpg" alt="">

                                    <a href="/images/locations/sl{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor

                    @for($i=1 ;$i<13 ;$i++)
                        <div class="item bangalore">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/soa{{$i}}.jpg" alt="">

                                    <a href="/images/locations/soa{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor
                    @for($i=1 ;$i<5 ;$i++)
                        <div class="item nagpur">

                            <div class="project">

                                <div class="project-image">

                                    <img src="/images/locations/nag{{$i}}.jpg" alt="">

                                    <a href="/images/locations/nag{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                    {{--                <div class="project-description">--}}

                                    {{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

                                    {{--                  <a href="#" class="project-cats">Poses</a>--}}

                                    {{--                </div>--}}

                                </div>

                            </div>

                        </div>
                    @endfor

                </div>

            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

    @include('layouts.footer')


</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


<script>

    // Vue.use(VueStripeCheckout, 'pk_test_jvbkvHL3HF4XhFVbyP9axp4o00PRxXGS23');


    var vm = new Vue({
        el: '#app',
        data() {
            return {
                full_nam: 'M F',
                program_id: '1',
                status: 'Closed',
                tokenFromPromise: {},
                tokenFromEvent: {},
                sampleCard: '4242 4242 4242 4242',
                image: '/images/logo.png',
                name: 'Rtahmbhara Wellness',
                description: '',
                currency: 'INR',
                amount: 100,
                quantity: '1',
                email: 'a@b.com',
                first_name: '',
                last_name: '',
                address: '',
                phone: '',


            }
        },

        computed: {
            finalAmount() {
                return this.amount * 100;
            }
        },
        mounted() {
            this.program_id = this.getMeta("program_id");
            // console.log(this.program_id);
        },
        methods: {
            getMeta(metaName) {
                const metas = document.getElementsByTagName('meta');

                for (let i = 0; i < metas.length; i++) {
                    if (metas[i].getAttribute('name') === metaName) {
                        return metas[i].getAttribute('content');
                    }
                }

                return '';
            },

            showModal() {
                // console.log("modal opened");
                var modal = document.getElementById("my-modal");
                modal.style.display = "block";
            },
            closeModal() {
                var modal = document.getElementById("my-modal");
                modal.style.display = "none";
                // console.log("modal closed");
                this.checkout();
            },


            async checkout() {
                this.tokenFromPromise = await this.$refs.checkoutRef.open();
            },


            done(token) {
                this.tokenFromEvent = token;

                console.log('-----------------------');
                console.log(token.token.id);

                var data = {
                    'token': token.token,
                    'program_id': this.program_id,
                    'email': this.email,
                    'quantity': this.quantity,
                    'first_name': this.first_name,
                    'last_name': this.last_name,
                    'address': this.address,
                    'phone': this.phone,
                };


                axios.post('/api/bookings', data)
                    .then(function (resp) {

                        console.log(resp);

                    })

            },
            opened() {
                this.status = 'Opened';
            },
            closed() {
                this.status = 'Closed';
            },
            submit(token) {
                console.log('token', token);
                console.log('Submit this token to your server to perform a stripe charge, or subscription.');
            },
        }
    });
</script>
</body>
</html>
