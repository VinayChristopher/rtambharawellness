<!doctype html>
<html lang="en">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

<!-- Basic Page Needs
================================================== -->

<title>Rtambhara Wellness</title>

<!--meta info-->
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

<link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/themes/wellness/css/fontello.css">
<link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
<link rel="stylesheet" href="/themes/wellness/css/style.css">
<link rel="stylesheet" href="/themes/wellness/css/responsive.css">
<link rel="stylesheet" href="/themes/wellness/css/custom.css">

<style>
    .img-title {
        width: 40%;
        padding: 2%;
        margin-bottom: 3%;
    }
</style>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap banner-3" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Our Corporate Programs</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Our Corporate Programs Details</li>
            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

        <div class="container">
            <div class="content-element">

                <div class="row">
                    <div class="col-md-6 col-sm-12">


                        <h5 class="title" style="margin-top: 6.5%">SPOORTHI - BE INSPIRED <span
                                style="font-size: medium">( 1 Day )</span></h5>

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title active">About the Program</h5>
                                <div class="a-content">
                                    <p>Spoorthi is a one day on-site/off-site that inspires organizations to find a
                                        deeper spiritual connect in the workplace. Work with us to highlight the most
                                        crucial challenges being faced by your organization, and co-create a program for
                                        the day from our myriad of offerings. The sessions are truly interactive and
                                        engaging for the participants.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Cost & Inclusions</h5>
                                <div class="a-content">
                                    <p>Interactive sessions on workplace happiness, wellbeing, and spirituality</p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>
                                </div>
                            </div>
                            {{--                            <div class="accordion-item">--}}
                            {{--                                <h5 class="a-title">Upcoming Events</h5>--}}
                            {{--                                <div class="a-content btn-center">--}}
                            {{--                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming--}}
                            {{--                                        Events</a>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12">


                        <h5 class="title" style="margin-top: 6.5%">SPANDANA - BE RESONANT <span
                                style="font-size: medium">( 2 Days )</span></h5>

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Spandana is a two day on-site/off-site that engages organizations in finding a
                                        balanced frequency at the workplace. Work with us to highlight the most crucial
                                        challenges being faced by your organization, and co-create a program including
                                        what is most relevant from our myriad of offerings. The sessions are truly
                                        interactive and engaging for the participants.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Cost & Inclusions</h5>
                                <div class="a-content">
                                    <p>Yoga, Pranayama and Meditation</p>
                                    <p>Interactive sessions on workplace happiness, wellbeing, and spirituality</p>
                                    <p>Connecting to Nature</p>
                                    <p>Cultural Connect – Music, Dance and Other Fine Arts</p>
                                    <p>Introspection</p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>
                                </div>
                            </div>
                            {{--                            <div class="accordion-item">--}}
                            {{--                                <h5 class="a-title">Upcoming Events</h5>--}}
                            {{--                                <div class="a-content btn-center">--}}
                            {{--                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming--}}
                            {{--                                        Events</a>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">


                        <h5 class="title" style="margin-top: 6.5%">SAMVEDANA - BE EMPATHETIC <span
                                style="font-size: medium">( 3 Days )</span></h5>

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>Samvedana is a three day on-site/off-site that engages organizations in creating
                                        an inclusive and empathetic work culture. Work with us to highlight the most
                                        crucial challenges being faced by your organization, and co-create a program
                                        including what is most relevant from our myriad of offerings. The sessions are
                                        truly interactive and engaging for the participants.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Cost & Inclusions</h5>
                                <div class="a-content">
                                    <p>Yoga, Pranayama and Meditation</p>
                                    <p>Interactive sessions on workplace happiness, wellbeing, and spirituality</p>
                                    <p>Connecting to Nature</p>
                                    <p>Cultural Connect – Music, Dance and Other Fine Arts</p>
                                    <p>Visit to a Historic/Cultural site to understand and feel high energy vibrations</p>
                                    <p>Introspection</p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>
                                </div>
                            </div>
                            {{--                            <div class="accordion-item">--}}
                            {{--                                <h5 class="a-title">Upcoming Events</h5>--}}
                            {{--                                <div class="a-content btn-center">--}}
                            {{--                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming--}}
                            {{--                                        Events</a>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->
</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
