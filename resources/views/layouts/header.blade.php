<nav id="mobile-advanced" class="mobile-advanced"></nav>

<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

<header id="header" class="header fixed-header sticky-header bamboo">

    <!-- searchform -->

    <div class="searchform-wrap">
        <div class="vc-child h-inherit">

            <form class="search-form">
                <button type="submit" class="search-button"></button>
                <div class="wrapper">
                    <input type="text" name="search" placeholder="Start typing...">
                </div>
            </form>

            <button class="close-search-form"></button>

        </div>
    </div>

    <!-- top-header -->

    <div class="top-header">

        <div class="flex-row align-items-center justify-content-between">

            <!-- logo -->

            <div class="logo-wrap">

                <a href="/" class="logo">
                    <img id="logo-dark" src="/images/logo_dark.png" alt="" width="280">
                    <img id="logo-light" src="/images/logo_light.png" alt="" width="280">
                </a>

            </div>

            <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->

            <!--main menu-->

            <div class="menu-holder" style="padding: 0;margin: 0">

                <div class="menu-wrap" style="padding: 0;margin: 0">

                    <div class="nav-item" style="padding: 0;margin: 0">

                        <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

                        <nav id="main-navigation" class="main-navigation" style="padding: 0;margin: 0">
                            <ul id="menu" class="clearfix">
                                <li id="index"><a href="/">Home</a>
                                    <!--sub menu-->
                                    {{--                                    <div class="sub-menu-wrap">--}}
                                    {{--                                        <ul>--}}
                                    {{--                                            <li class="current sub"><a href="#">Homepage Layouts</a>--}}
                                    {{--                                                <!--sub menu-->--}}
                                    {{--                                                <div class="sub-menu-wrap sub-menu-inner">--}}
                                    {{--                                                    <ul>--}}
                                    {{--                                                        <li class="current"><a href="/">Home - Studio</a></li>--}}
                                    {{--                                                        <li><a href="home_2.html">Home - Teacher</a></li>--}}
                                    {{--                                                        <li><a href="home_3.html">Home - Online Classes</a></li>--}}
                                    {{--                                                        <li><a href="home_4.html">Home - Teacher Trainings</a></li>--}}
                                    {{--                                                    </ul>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </li>--}}
                                    {{--                                            <li class="sub"><a href="#">Header Layouts</a>--}}
                                    {{--                                                <!--sub menu-->--}}
                                    {{--                                                <div class="sub-menu-wrap sub-menu-inner">--}}
                                    {{--                                                    <ul>--}}
                                    {{--                                                        <li><a href="/">Header 1</a></li>--}}
                                    {{--                                                        <li><a href="home_2.html">Header 2</a></li>--}}
                                    {{--                                                        <li><a href="home_3.html">Header 3</a></li>--}}
                                    {{--                                                        <li><a href="home_4.html">Header 4</a></li>--}}
                                    {{--                                                        <li><a href="home_5.html">Header 5</a></li>--}}
                                    {{--                                                    </ul>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </li>--}}
                                    {{--                                            <li class="sub"><a href="#">Footer Layouts</a>--}}
                                    {{--                                                <!--sub menu-->--}}
                                    {{--                                                <div class="sub-menu-wrap sub-menu-inner">--}}
                                    {{--                                                    <ul>--}}
                                    {{--                                                        <li><a href="index.html#footer" class="animated">Footer 1</a></li>--}}
                                    {{--                                                        <li><a href="home_2.html#footer" class="animated">Footer 2</a></li>--}}
                                    {{--                                                        <li><a href="home_3.html#footer" class="animated">Footer 3</a></li>--}}
                                    {{--                                                        <li><a href="home_4.html#footer" class="animated">Footer 4</a></li>--}}
                                    {{--                                                    </ul>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </div>--}}
                                </li>
                                <li id="about-us"><a href="/about-us">About Us</a>
                                    <!--sub menu-->
                                    {{--                                    <div class="sub-menu-wrap">--}}
                                    {{--                                        <ul>--}}
                                    {{--                                            <li><a href="/about-us">About Us</a></li>--}}
                                    {{--                                            <li><a href="about_me.html">About Me</a></li>--}}
                                    {{--                                            <li><a href="/team">Our Team</a></li>--}}
                                    {{--                                            <li><a href="team_member.html">Team Member Page</a></li>--}}
                                    {{--                                            <li><a href="pricing.html">Pricing</a></li>--}}
                                    {{--                                            <li><a href="faq.html">FAQ</a></li>--}}
                                    {{--                                            <li><a href="contact.html">Contact Us</a></li>--}}
                                    {{--                                            <li><a href="contact_me.html">Contact Me</a></li>--}}
                                    {{--                                            <li><a href="coming_soon.html">Coming Soon Page</a></li>--}}
                                    {{--                                            <li><a href="404_page.html">404 Page</a></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </div>--}}
                                </li>
                                <li id="team"><a href="/team">Our Team</a>
                                    <!--sub menu-->
                                    {{--                                    <div class="sub-menu-wrap">--}}
                                    {{--                                        <ul>--}}
                                    {{--                                            <li><a href="classes.html">Classes</a></li>--}}
                                    {{--                                            <li><a href="classes_desc.html">Class Descriptions</a></li>--}}
                                    {{--                                            <li><a href="classes_schedule.html">Class Schedule</a></li>--}}
                                    {{--                                            <li><a href="challenges.html">Challenges</a></li>--}}
                                    {{--                                            <li><a href="classes_single.html">Single Class Page</a></li>--}}
                                    {{--                                            <li><a href="challenge_single.html">Single Challenge Page</a></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </div>--}}
                                </li>
                                <li id="retreats-and-wellness"><a>
                                        Wellness Programs
                                        <div class="sub-menu-wrap" style="padding: 10px 0 ;">
                                            <ul>
                                                <li><a href="/corporate-program">Corporate Programs</a></li>
                                                <li><a href="/retreats-and-wellness">Retreats</a></li>
                                            </ul>
                                        </div>
                                    </a>
                                </li>
                                <li id="locations"><a href="/locations">Locations</a>
                                </li>
                                <li id="upcoming-events"><a href="/upcoming-events">Events</a>
                                    <!--sub menu-->
                                    <div class="sub-menu-wrap" style="padding: 10px 0 ;min-width: 200px">
                                        <ul>
                                            <li><a href="/past-events-gallery">Past Events</a></li>
                                        </ul>
                                    </div>

                                </li>
                                {{--                                <li><a href="#">Blog</a>--}}
                                {{--                                    <!--sub menu-->--}}
                                {{--                                    <div class="sub-menu-wrap">--}}
                                {{--                                        <ul>--}}
                                {{--                                            <li><a href="blog_classic.html">Classic</a></li>--}}
                                {{--                                            <li><a href="blog_masonry.html">Masonry</a></li>--}}
                                {{--                                            <li><a href="blog_small.html">With Small Thumbnails</a></li>--}}
                                {{--                                            <li><a href="blog_single.html">Single News Page</a></li>--}}
                                {{--                                        </ul>--}}
                                {{--                                    </div>--}}
                                {{--                                </li>--}}
                                <li id="contact-us"><a href="/contact-us">Contact Us</a>
                                    <!--sub menu-->
                                    {{--                                    <div class="sub-menu-wrap">--}}
                                    {{--                                        <ul>--}}
                                    {{--                                            <li><a href="shop_category.html">Category Page</a></li>--}}
                                    {{--                                            <li><a href="shop_single.html">Single Product Page</a></li>--}}
                                    {{--                                            <li><a href="shop_cart.html">Cart</a></li>--}}
                                    {{--                                            <li><a href="shop_checkout.html">Checkout</a></li>--}}
                                    {{--                                            <li><a href="shop_account.html">My Account</a></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                    </div>--}}
                                </li>
                            </ul>
                        </nav>

                        <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

                    </div>

                    <!-- search button -->
                    {{--                    <div class="search-holder"><button type="button" class="search-button"></button></div>--}}
                    {{--                    <!-- account button -->--}}
                    {{--                    <button type="button" class="account popup-btn-login"></button>--}}
                    {{--                    <!-- shop button -->--}}
                    {{--                    <div class="shop-cart">--}}

                    {{--                        <button class="sc-cart-btn dropdown-invoker"><span class="licon-cart"></span></button>--}}

                    {{--                        <div class="shopping-cart dropdown-window">--}}

                    {{--                            <div class="products-holder">--}}

                    {{--                                <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->--}}

                    {{--                                <div class="product">--}}

                    {{--                                    <button class="item-close"></button>--}}

                    {{--                                    <a href="#" class="product-image">--}}

                    {{--                                        <img src="/themes/wellness/images/78x78_img1.jpg" alt="">--}}

                    {{--                                    </a>--}}

                    {{--                                    <div class="product-description">--}}

                    {{--                                        <h6 class="product-title"><a href="#">Non Slip Yoga Mat</a></h6>--}}

                    {{--                                        <span class="product-price">1x$19.95</span>--}}

                    {{--                                    </div><!--/ .product-info -->--}}

                    {{--                                </div>--}}

                    {{--                                <!-- - - - - - - - - - - - - - End of Product - - - - - - - - - - - - - - - - -->--}}

                    {{--                                <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->--}}

                    {{--                                <div class="product">--}}

                    {{--                                    <button class="item-close"></button>--}}

                    {{--                                    <a href="#" class="product-image">--}}

                    {{--                                        <img src="/themes/wellness/images/78x78_img2.jpg" alt="">--}}

                    {{--                                    </a>--}}

                    {{--                                    <div class="product-description">--}}

                    {{--                                        <h6 class="product-title"><a href="#">Light Hard Foam Yoga Blocks</a></h6>--}}

                    {{--                                        <span class="product-price">1x$11.35</span>--}}

                    {{--                                    </div><!--/ .product-info -->--}}

                    {{--                                </div>--}}

                    {{--                                <!-- - - - - - - - - - - - - - End of Product - - - - - - - - - - - - - - - - -->--}}

                    {{--                            </div><!--/ .products-holder -->--}}

                    {{--                            <div class="sc-footer">--}}

                    {{--                                <div class="subtotal">Subtotal: <span class="total-price">$35.68</span></div>--}}

                    {{--                                <div class="vr-btns-set">--}}

                    {{--                                    <a href="#" class="btn btn-small">View Cart</a>--}}
                    {{--                                    <a href="#" class="btn btn-small btn-style-3">Checkout</a>--}}

                    {{--                                </div><!--/ .vr-btns-set -->--}}

                    {{--                            </div>--}}

                    {{--                        </div><!--/ .shopping-cart -->--}}

                    {{--                    </div>--}}
                    <a href="/upcoming-events" class="btn popup-btn btn-big btn-style-3">Book Now</a>

                </div>

            </div>

        </div>

    </div>

</header>
