<div class="breadcrumbs-wrap no-title" style="margin-top: 10%">

    <div class="container">

        <ul class="breadcrumbs">

            <li><a href="/">Home</a></li>
            <li href="/upcoming-events">Events</li>
            <li>{{$program->title}}</li>

        </ul>

    </div>

</div>

<!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

<div id="content" class="page-content-wrap">

    <div class="container">

        <div class="content-element">

            <div class="content-element4">
                <div class="page-nav">
                    <div class="flex-item"><a href="/upcoming-events" class="info-btn prev-btn">All Events</a></div>
                    <div class="flex-item">

                        <div class="btn-wrap">
                            <a class="btn btn-style-2"
                               onclick="ical_download('{{$program->title}}', 'rthambara.ics', 'Fri Jul 27 2019 08:00:00 GMT+0530', 'Fri Jul 29 2019 16:00:00 GMT+0530')">+
                                Google Calendar</a>
                            <a href="#" class="btn btn-style-2">+ iCal Export</a>
                        </div>

                    </div>
                </div>
            </div>

            <div class="content-element5" id="app">
                <div class="entry-box single-post">

                    <div class="entry">

                        <div class="content-element4">
                            <div class="entry-body">

                                <h1 class="entry-title">{{$program->title}}</h1>
                                <div class="our-info vr-type">

                                    <div class="info-item">
                                        <i class="licon-clock3"></i>
                                        <div class="wrapper">
                                            <span>{{$program->program_date}} for {{$program->duration}} Days</span>
                                            (<a href="/upcoming-events">See all</a>)
                                            <span class="product-price">Rs.{{$program->cost}}</span>
                                            <div>
                                                <vue-stripe-checkout
                                                    ref="checkoutRef"
                                                    :image="image"
                                                    :name="name"
                                                    :description="description"
                                                    :currency="currency"
                                                    :amount="amount"
                                                    @done="done"
                                                    @opened="opened"
                                                    @closed="closed"
                                                ></vue-stripe-checkout>
                                                <button @click="checkout">Checkout</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="content-element2">

                            <div class="row">
                                <div class="col-lg-12 col-md-12"><img src="/images/jay-unsplash.jpg" height="500"
                                                                      style="width: 100%" alt=""></div>
                                {{--                    <div class="col-lg-4 col-md-12"><div id="googleMap" class="map-container"></div></div>--}}
                            </div>

                        </div>


                        <div class="col-md-12 " style="margin-bottom: 20px">

                            <div class="row">
                                <h4 class="title">Intro</h4>
                                <p>Pravesa is an introductory program with Rtambhara Wellness. This program is
                                    designed
                                    to invite you on a journey of discovery enabling you to seek well being, wisdom,
                                    and
                                    spirituality.</p>

                            </div>
                            <div class="row">
                                <h4 class="title">About the Program</h4>
                                <p>A two night and three day getaway, Pravesa is the perfect way to rejuvenate. Take
                                    a break from the stress of everyday life and adopt simple changes for a happier
                                    lifestyle.</p>

                            </div>
                            <div class="row">
                                <h4 class="title">What to Expect</h4>
                                <p>You can expect a memorable 3 days of curated talks and activities that will delve
                                    into Wellness, Happiness and Understanding one’s ego, mind and your "inner”
                                    self. Learn Yoga, Laughter therapy and Meditation to positively impact your
                                    physical and mental well being.</p>
                                <p>
                                    Be prepared to cook up some delicious snacks from organic foods, literally from
                                    "Farm to Plate”. Visit a cow farm to understand why the cow is revered, and how
                                    much benefit one can get from Nature. And, be mesmerized by cultural programs
                                    every evening that will unleash your artistic and creative side.
                                </p>
                                <p>
                                    End the program with interactive sessions with the faculty to understand how you
                                    can continue on this journey. Take home valuable inputs that you can use for
                                    your daily living. And, walk away with the determination that would lead to
                                    "Transformation" and "Happier Living"
                                </p>
                            </div>
                            <div class="row">
                                <h4 class="title">Costs & Inclusions</h4>
                                <p>The pilot program for Pravesa is complimentary for friends, family and
                                    ambassadors of Rtambhara Wellness. The program will be at The School of Ancient
                                    Wisdom in Devanahalli, Bengaluru. Accommodation, satvik food and beverages, and
                                    program materials are included.</p>

                            </div>
                        </div>
                        <div class="content-element3">

                            <div class="row">
                                <div class="col-md-4 col-sm-12">

                                    <h6 class="event-title">Details</h6>

                                    <ul class="custom-list">

                                        <li><span>Date:</span> {{$program->program_date}}</li>
                                        <li><span>Time:</span> 3:30 PM - 5:00 PM</li>
                                        <li><span>Cost:</span> <span class="product-price">Rs.{{$program->cost}}</span></li>
                                        <li><span>Event Category:</span> <a href="#" class="link-text">Training</a>
                                        </li>
                                        <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>
                                        </li>

                                    </ul>

                                </div>
                                <div class="col-md-4 col-sm-12">

                                    <h6 class="event-title">Organizer</h6>

                                    <ul class="custom-list">

                                        <li><span>Organizer Name:</span> Company Name</li>
                                        <li content="telephone=no"><span>Phone:</span> (932) 733-3390</li>
                                        <li><span>Email:</span> <a href="#"
                                                                   class="link-text">info@companyname.com</a></li>
                                        <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>
                                        </li>

                                    </ul>

                                </div>
                                <div class="col-md-4 col-sm-12">

                                    <h6 class="event-title">Venue</h6>

                                    <ul class="custom-list">

                                        <li><span>Venue Name:</span> Name of Placement</li>
                                        <li><span>Address:</span> 8901 Marmora Road, Glasgow, D04 89GR.</li>
                                        <li><span>Email:</span> <a href="#"
                                                                   class="link-text">info@companyname.com</a></li>
                                        <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>
                                        </li>

                                    </ul>

                                </div>
                            </div>

                        </div>

                        <div class="share-wrap">

                            <span class="share-title">Share this:</span>
                            <ul class="social-icons var2 share">

                                <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i></a></li>
                                <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i></a></li>
                                <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i></a></li>
                                <li><a href="#" class="sh-pinterest"><i class="icon-pinterest"></i></a></li>
                                <li><a href="#" class="sh-mail"><i class="icon-mail"></i></a></li>

                            </ul>

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>

</div>
