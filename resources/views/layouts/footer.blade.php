<footer id="footer" class="footer" data-bg="/themes/wellness/images/1920x390_bg1.jpg">

    <div class="logo"><img src="/images/logo_light.png" alt="" width="300"></div>

    <div class="container wide-style">

        <!-- main footer -->
        <div class="main-footer">

            <div class="row">

                <div class="col-lg-4 col-md-12">

                    <div class="widget">

                        <h6 class="widget-title">Join Our Mailing List</h6>

                        <form id="newsletter" class="newsletter style-2">
                            <input type="email" name="newsletter-email" placeholder="Enter your email address">
                            <button type="submit" data-type="submit" class="btn btn-big btn-style-3">Sign Up</button>
                        </form>

                    </div>

                </div>

                <div class="col-lg-4 offset-lg-4 col-md-12">

                    <div class="widget">

                        <h6 class="widget-title">Stay Connected</h6>

                        <ul class="social-icons">

                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            {{--                            <li><a href="#"><i class="icon-gplus-3"></i></a></li>--}}
                            {{--                            <li><a href="#"><i class="icon-linkedin"></i></a></li>--}}
                            <li><a href="#"><i class="icon-instagram-5"></i></a></li>
                            {{--                            <li><a href="#"><i class="icon-youtube-play"></i></a></li>--}}
                            {{--                            <li><a href="#"><i class="icon-pinterest"></i></a></li>--}}

                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <ul class="menu-list">

            <li><a href="/">Home</a></li>
            <li><a href="/about-us">About Us</a></li>
            <li><a href="/team">Our Team</a></li>
            <li><a href="/retreats-and-wellness">Wellness Programs</a></li>
            <li><a href="/locations">Locations</a></li>
            {{--            <li><a href="#">Portfolio</a></li>--}}
            {{--            <li><a href="#">Elements</a></li>--}}
            <li><a href="/upcoming-events">Events</a></li>
            <li><a href="/contact-us">Contact Us</a></li>

        </ul>

        <div class="copyright">

            <p>Copyright © {{date("Y")}} Rtambhara Wellness. All Rights Reserved.</p>

        </div>

    </div>

</footer>
<script>
    window.onload = function () {
        // console.log( "ready!" );
        var pathname = window.location.pathname;
        console.log("pathname: " + pathname);

        if (pathname !== '' && pathname !== '/') {
            var menu_item;
            menu_item = pathname.split('/')[1];
            $('#' + menu_item).addClass("current");
            $('.sticky-header #' + menu_item).addClass("current");

        } else {
            $('#index').addClass("current");
            $('.sticky-header #index').addClass("current");

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

                $('.bamboo>.top-header>div>.logo-wrap>a>#logo-light').attr('src', '/images/logo_dark.png');
            }

        }


    };

</script>
