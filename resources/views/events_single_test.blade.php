<!doctype html>
<html lang="en">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->


    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.common.dev.js"></script>
    <script src="https://unpkg.com/vue-stripe-checkout/build/vue-stripe-checkout.js"></script>


    <style>
        h4.title {
            padding-top: 2%;
            padding-bottom: 1%;
            margin-bottom: 1% !important;
        }

        .popup .popup-inner {
            position: relative;
            margin-top: 1%;
            width: 930px;
            display: inline-block;
            padding: 26px;
            background: #fff;
        }

        .single-post .custom-list li .product-price a {
            color: goldenrod;
        }

        [class*="alert"]:before {
            font-family: 'linearicons';
            font-size: 30px;
            color: #fff;
            position: absolute;
            top: 25px;
            left: 30px;
            display: none;
        }
        p {
          margin-bottom: 20px !important;
        }
    </style>

    <script src="/js/cal.js"></script>
    <script>
        function dismiss(el) {
            el.parentNode.style.display = 'none';
        };
    </script>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap no-title" style="margin-top: 10%">

        <div class="container">

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li href="/upcoming-events">Events</li>
                <li>Pravesa - Rejuvenate Program</li>

            </ul>

        </div>


    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="app" class="page-content-wrap">

        <div class="container">

            <div class="content-element">

                <div>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Great! </strong>{!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                    onclick="dismiss(this);">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <?php Session::forget('success');?>

                    @endif


                    @if ($message = Session::get('error'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Oops! </strong>{!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                    onclick="dismiss(this);">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <?php Session::forget('error');?>

                    @endif


                </div>


                <div class="content-element4">
                    <div class="page-nav">
                        <div class="flex-item"><a href="/upcoming-events" class="info-btn prev-btn">All Events</a></div>
                        <div class="flex-item">

                            <div class="btn-wrap">
                                <a href="#" class="btn btn-big  btn-style-2"
                                   onclick="ical_download('Pravesa - Rejuvenate Program', 'rthambara.ics', 'Fri Jul 27 2019 08:00:00 GMT+0530', 'Fri Jul 29 2019 16:00:00 GMT+0530')">+
                                    iCal Export</a>


                                <span class="product-price"> <button
                                        class="btn popup-btn btn-big btn-style-3"
                                        @click.prevent="showModal">
                                        <i class="icon-cart mr-2">100 INR</i></button></span>
                                {{--                                <a href="#" class="btn btn-style-2">+ iCal Export</a>--}}
                            </div>

                        </div>
                    </div>
                </div>

                <div class="content-element5">
                    <div class="entry-box single-post">

                        <div class="entry">

                            <div class="content-element4">
                                <div class="entry-body">

                                    <h1 class="entry-title">Pravesa - Rejuvenate Program</h1>
                                    <div class="our-info vr-type">

                                        <div class="info-item">
                                            <i class="licon-clock3"></i>
                                            <div class="wrapper">
                                                <span><a href="#"
                                                         onclick="ical_download('Pravesa - Rejuvenate Program', 'rthambara.ics', 'Fri Jul 27 2019 08:00:00 GMT+0530', 'Fri Jul 29 2019 16:00:00 GMT+0530')">
                                                        Saturday 27th July, 2019 for 3 Days</a></span>
                                                (<a href="/upcoming-events">See all</a>)
                                                {{--                                                <span class="product-price fas fa-shopping-cart"> <button @click.prevent="checkout"--}}
                                                {{--                                                                                     class="btn popup-btn btn-big btn-style-3">100</button></span>--}}


                                                {{--                                                <span class="product-price"> <button--}}
                                                {{--                                                        class="btn popup-btn btn-big btn-style-3"--}}
                                                {{--                                                        @click.prevent="showModal"><i--}}
                                                {{--                                                            class="icon-cart mr-2">100 INR</i></button></span>--}}

                                                <div>
                                                    <vue-stripe-checkout
                                                        ref="checkoutRef"
                                                        :image="image"
                                                        :name="name"
                                                        :description="description"
                                                        :currency="currency"
                                                        :amount="finalAmount"
                                                        @done="done"
                                                        @opened="opened"
                                                        @closed="closed"
                                                    ></vue-stripe-checkout>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="content-element2">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12"><img src="/images/jay-unsplash.jpg" height="500"
                                                                          style="width: 100%" alt=""></div>
                                    {{--                    <div class="col-lg-4 col-md-12"><div id="googleMap" class="map-container"></div></div>--}}
                                </div>

                            </div>


                            <div class="row">


                                <div class="col-md-6 " style="margin-bottom: 1%">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="title">Intro</h4>
                                            <p>Prerana is a curated wellness retreat from Rtambhara Wellness.
                                                This retreat is designed to inspire you on a journey of discovery
                                                enabling you to seek well being, wisdom, and spirituality.</p>

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 2%">
                                        <div class="col-md-12">
                                            <h4 class="title">About the Program</h4>
                                            <p>You can expect a memorable 14 nights and 15 days of self
                                                discovery! If you are on the quest for spiritual knowledge, there
                                                is no better way to get introduced to the “Sanatana Dharma”
                                                that will be unfolded in the days following your arrival.</p>

                                        </div>

                                    </div>
                                    <div class="row" style="margin-top: 2%">
                                        <div class="col-md-12">
                                            <h4 class="title">Study Session Details</h4>
                                            <h6 class="title">WISDOM FROM ANCIENT SCRIPTURES - KRISHNAPHANI
                                                KESIRAJU</h6>

                                            <ul style="list-style: circle">
                                                <li>Do I know myself?</li>
                                                <li>Why am I here?</li>
                                                <li>Whence have I come and whither do I go?</li>
                                                <li>What do I do now?</li>
                                                <li>Mind and beyond</li>
                                                <li>“I” consciousness</li>
                                                <li>The constitution of Man</li>
                                                <li>Sanatana Dharma – The Eternal Principle - 1 & 2</li>
                                                <li>Sadhanachatushytayam - The 4 Accomplishments for</li>
                                                <li>Self realization -1 & 2</li>
                                                <li>Karma - Samnyasa – Jnana - Bhakti – 4 paths to</li>
                                                <li>liberation – 1 & 2</li>
                                            </ul>
                                            <br>
                                            <h6 class="title">LIVE A HAPPIER LIFE – BINA MIRCHANDANI</h6>
                                            <ul style="list-style: circle">
                                                <li>Wellness 9</li>
                                                <li>Your Happiness Quotient – 1 & 2</li>
                                                <li>You create your destiny</li>
                                                <li>Healing through connecting with Nature</li>
                                                <li>Purification of our physical and other bodies</li>
                                            </ul>


                                        </div>

                                    </div>
                                    <div class="row" style="margin-top: 2%">
                                        <div class="col-md-12">
                                            <h4 class="title">A Typical Day at Prerana </h4>
                                            <p>A typical day at Prerana would entail Yoga, Pranayama and
                                                Meditation by the beach or outdoors.</p>
                                            <p>You can expect to learn and dialogue about Wisdom from
                                                Ancient scriptures with Krishnaphani Kesiraju. You will
                                                engage in talks on life with Bina Mirchandani.</p>
                                            <p>
                                                Trips to Spiritual Centers, Historic energy centers, and
                                                other experiences in local culture.
                                            </p>
                                            <p>
                                                Cultural events and evenings free to explore the city and
                                                its nuances. Regular Detox Reviews will be part of your
                                                day. Introspection on your growth and learnings will help
                                                contextualize your time with Rtambhara.
                                            </p>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 " style="margin-bottom: 1%">
                                    <div class="row">
                                        <h4 class="title">What to Expect</h4>
                                        <p>Every day of the retreat, Krishnaphani Kesiraju, our Program
                                            director will take you through a slow and easy understanding
                                            of the Ancient texts, a gradual unfolding and ascension of your
                                            consciousness, which is the purpose of our life journey.</p>

                                        <p>
                                            Find your happiness quotient, how you can increase your
                                            happiness through simple life changes, and change your
                                            vibrations and impact those around you to create a better
                                            existence.
                                        </p>
                                        <p>
                                            Are you the creator of your destiny? Does nature help with
                                            healing? Is creativity good for a spiritual connect with your
                                            higher self? These and other questions will be answered by our
                                            Faculty, a group of knowledgeable individuals, who will share
                                            their life experiences as well.
                                        </p>
                                        <p>
                                            Learn Raj Yoga, Pranayam, Mudras and Meditation in the
                                            outdoors, revel in the beautiful beaches of Goa at sunrise and
                                            sunset! Nature walks too!
                                        </p>
                                        <p>
                                            Delve into Health, Wellness and Ayurveda and understand
                                            how it can enhance your physical existence.
                                        </p>
                                        <p>
                                            Start the program with an individualized detoxification diet
                                            and massage program, that will help restore the balance of
                                            your doshas, and with the added benefit of losing weight!
                                        </p>
                                        <p>
                                            Pamper yourself with spa and age defying therapies.
                                        </p>
                                        <p>
                                            Be prepared to cook up some delicious snacks from organic
                                            foods, literally from “Farm to Plate”!
                                        </p>
                                        <p>
                                            Visit Farming villages and engage in their community living,
                                            understand their traditions, culture and food. If you’re game
                                            for it, help the farmers, plant seedlings and saplings yourself!
                                        </p>
                                        <p>
                                            Engage in field visits to ancient historical sites and High
                                            energy centers to raise your vibrational frequency. Apart
                                            from scheduled trips to historical sites, we will visit flea
                                            markets, pottery markets and all that Goa is famous for!
                                        </p>
                                        <p>
                                            Be mesmerized by cultural programs every evening that will
                                            unleash your artistic and creative side and feel the connect
                                            with your higher potential.
                                        </p>
                                        <p>
                                            There will be plenty of time for rest and relaxation. Most
                                            evenings you will be free to explore Goa beaches, the main
                                            city of Panjim and its suburbs and enjoy the Goan culture,
                                            food and nightlife on your own as well!
                                        </p>
                                        <p>
                                            End the program with interactive sessions with the faculty to
                                            understand how you can continue on this journey.
                                        </p>
                                        <p>
                                            Take home valuable inputs that you can use in your daily life
                                            and walk away with a determination to walk the spiritual path
                                            and lead a happier life with “awareness”!
                                        </p>

                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-bottom: 1%">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="title">Other Activites</h4>
                                            <h6>CREATIVITY - CONNECT WITH YOUR INNER SELF</h6>
                                            <p>Music, Dance, Art, Pottery, and Gardening</p>
                                            <h6>ENERGY CENTRE FIELD VISITS</h6>
                                            <p>Churches and Temples, Historic sites, and Nature walks
                                                and trips</p>
                                            <h6>THE GOAN EXPERIENCE</h6>
                                            <p>Farming and village experience in Goa, Goan Lunch in a Local
                                                Home, Goa’s famous Flea Markets, Goan culture, music and
                                                dance, Famous beaches of Goa, Dolphin Watching, and Spice
                                                plantation tour with lunch.</p>
                                            <h6>DIET & NUTRITION FOR PRANIC LIVING</h6>
                                            <p>Farm to plate – Organic Living, Eat living foods for enhancing
                                                Prana, and Plant microgreens at home.</p>
                                            <h6>YOUR HEALTH</h6>
                                            <p>Detox</p>
                                            <p>Individual health checks and monitoring</p>
                                            <p>Ayurveda - Diagnosis and medication</p>
                                            <p>Ayurvedic massages will be chargeable</p>
                                        </div>



                                    </div>
                                    <div class="row">
                                        <h4 class="title">Costs & Inclusions</h4>
                                        <ul style="list-style: circle">
                                            <li>All programs , workshops , sight-seeing tours and activities
                                                    as mentioned including consultation with Ayurveda
                                                    specialist.</li>
                                            <li>
                                                    Accommodation in a 4/5 star property on twin sharing +
                                                    breakfast and lunch / dinner . Herbal coolers / tea all day .


                                                </li>
                                            <li>Welcome Kit</li>
                                            <li>Airport transfers</li>
                                            <li>
                                                    Services of a professional tour guide on some days. Tickets
                                                    costs for entry at sight-seeing places .

                                                </li>
                                            <li>
                                                    Internal transport costs for sight seeing as per our tour plan .
                                                </li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                            <div class="content-element3">

                                <div class="row">
                                    <div class="col-md-4 col-sm-12">

                                        <h6 class="event-title">Details</h6>

                                        <ul class="custom-list">

                                            <li><span>Date:</span><a href="#"
                                                                     onclick="ical_download('Pravesa - Rejuvenate Program', 'rthambara.ics', 'Fri Jul 27 2019 08:00:00 GMT+0530', 'Fri Jul 29 2019 16:00:00 GMT+0530')">
                                                    Saturday 27th July, 2019</a></li>
                                            {{--                                            <li><span>Time:</span> 3:30 PM - 5:00 PM</li>--}}
                                            <li><span>Cost:</span> <span class="product-price">
                                                    <a href="#" @click.prevent="showModal">
                                                        <i class="icon-cart mr-2">100 INR</i></a></span></li>
                                            <li><span>Level:</span> <a class="link-text">Beginner</a>
                                            </li>
                                            {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                            {{--                                            </li>--}}

                                        </ul>

                                    </div>
                                    {{--                                    <div class="col-md-4 col-sm-12">--}}

                                    {{--                                        <h6 class="event-title">Organizer</h6>--}}

                                    {{--                                        <ul class="custom-list">--}}

                                    {{--                                            <li><span>Organizer Name:</span> Company Name</li>--}}
                                    {{--                                            <li content="telephone=no"><span>Phone:</span> (932) 733-3390</li>--}}
                                    {{--                                            <li><span>Email:</span> <a href="#"--}}
                                    {{--                                                                       class="link-text">info@companyname.com</a></li>--}}
                                    {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                    {{--                                            </li>--}}

                                    {{--                                        </ul>--}}

                                    {{--                                    </div>--}}
                                    <div class="col-md-4 col-sm-12">

                                        <h6 class="event-title">Venue</h6>

                                        <ul class="custom-list">

                                            <li><span>Venue Name:</span>The School of Ancient Wisdom</li>
                                            <li><span>Address:</span>Devanahalli, Bengaluru</li>
                                            {{--                                            <li><span>Email:</span> <a href="#"--}}
                                            {{--                                                                       class="link-text">info@companyname.com</a></li>--}}
                                            {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                            {{--                                            </li>--}}

                                        </ul>

                                    </div>
                                </div>

                            </div>

                            {{--                            <div class="share-wrap">--}}

                            {{--                                <span class="share-title">Share this:</span>--}}
                            {{--                                <ul class="social-icons var2 share">--}}

                            {{--                                    <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-pinterest"><i class="icon-pinterest"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-mail"><i class="icon-mail"></i></a></li>--}}

                            {{--                                </ul>--}}

                            {{--                            </div>--}}

                        </div>

                    </div>

                    <div id="my-modal" class="popup">

                        <div class="popup-inner">

                            <button type="button" class="close-popup"></button>

                            <div class="pricing-tables-holder flex-row item-col-1">

                                <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->
                                <div class="pricing-col">

                                    <div class="pricing-table style-2">

                                        <header class="pt-header">
                                            <div class="col-md-12 col-sm-12">

                                                <h5>Billing Details</h5>
                                                {{--                                                <div class="alert alert-success" style="text-align: center">--}}
                                                {{--                                                    Online bookings via Card and PayPal will be made available at the--}}
                                                {{--                                                    soonest.<br>--}}
                                                {{--                                                    For now, please contact us at <a--}}
                                                {{--                                                        href="mailto:info@rtambharawellness.com">info@rtambharawellness.com.</a>--}}
                                                {{--                                                </div>--}}
                                                <form class="contact-form style-2" action="/paypal" method="POST">

                                                    {{csrf_field()}}
                                                    <input type="hidden" name="program_id" value="1">
                                                    <div class="contact-item">

                                                        <div class="row">
                                                            <div class="col-sm-6">

                                                                <label class="required">First Name</label>
                                                                <input name="first_name" type="text" required>

                                                            </div>
                                                            <div class="col-sm-6">

                                                                <label class="required">Last Name</label>
                                                                <input name="last_name" type="text">

                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="contact-item">

                                                        <label class="required">Address</label>
                                                        <input name="address" type="text" required>

                                                    </div>

                                                    <div class="contact-item">

                                                        <div class="row">
                                                            <div class="col-sm-6">

                                                                <label class="required">Phone</label>
                                                                <input name="phone" type="text" required>

                                                            </div>
                                                            <div class="col-sm-6">

                                                                <label class="required">Email</label>
                                                                <input name="email" type="email" required>

                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="contact-item">
                                                        {{--                                                        <button class="btn btn-style-3 " @click.prevent="closeModal">--}}
                                                        {{--                                                            <i class="fa fa-paypal">100INR</i>--}}
                                                        {{--                                                        </button>--}}

                                                        <span class="product-price">
                                                            <button class="btn popup-btn btn-big btn-style-3"
                                                                    type="submit">
                                                                        <i class="icon-paypal mr-10">100.00 INR</i>
                                                            </button>
                                                        </span>
                                                    </div>

                                                </form>

                                            </div>
                                        </header><!--/ .pt-header -->

                                    </div>

                                </div>
                                <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->
                            {{--                <div class="pricing-col">--}}

                            {{--                    <div class="pricing-table">--}}

                            {{--                        <div class="label">Limited Time Offer</div>--}}

                            {{--                        <header class="pt-header">--}}

                            {{--                            <h4 class="pt-title">Unlimited <br> membership</h4>--}}

                            {{--                            <div class="pt-price">$109</div>--}}

                            {{--                            <div class="section-pre-title style-2">per month</div>--}}

                            {{--                            <div class="pt-disc">Students - $90/mo</div>--}}

                            {{--                        </header><!--/ .pt-header -->--}}

                            {{--                        <footer class="pt-footer">--}}

                            {{--                            <a href="#" class="btn btn-style-3">Buy Now</a>--}}

                            {{--                            <p>Minimum 4 month auto-renew commitment</p>--}}

                            {{--                        </footer><!--/ .pt-footer -->--}}

                            {{--                    </div>--}}

                            {{--                </div>--}}
                            <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

    @include('layouts.footer')


</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


<script>

    Vue.use(VueStripeCheckout, 'pk_test_jvbkvHL3HF4XhFVbyP9axp4o00PRxXGS23');

    var vm = new Vue({
        el: '#app',
        data() {
            return {
                full_nam: 'M F',
                program_id: '1',
                status: 'Closed',
                tokenFromPromise: {},
                tokenFromEvent: {},
                sampleCard: '4242 4242 4242 4242',
                image: '/images/logo.png',
                name: 'Rtahmbhara Wellness',
                description: '',
                currency: 'INR',
                amount: 100,
                quantity: '1',
                email: 'a@b.com',
                first_name: '',
                last_name: '',
                address: '',
                phone: '',


            }
        },

        computed: {
            finalAmount() {
                return this.amount * 100;
            }
        },
        methods: {


            showModal() {
                // console.log("modal opened");
                var modal = document.getElementById("my-modal");
                modal.style.display = "block";
            },
            closeModal() {
                var modal = document.getElementById("my-modal");
                modal.style.display = "none";
                // console.log("modal closed");
                this.checkout();
            },


            async checkout() {
                this.tokenFromPromise = await this.$refs.checkoutRef.open();
            },


            done(token) {
                this.tokenFromEvent = token;

                console.log('-----------------------');
                console.log(token.token.id);

                var data = {
                    'token': token.token,
                    'program_id': this.program_id,
                    'email': this.email,
                    'quantity': this.quantity,
                    'first_name': this.first_name,
                    'last_name': this.last_name,
                    'address': this.address,
                    'phone': this.phone,
                };


                axios.post('/api/bookings', data)
                    .then(function (resp) {

                        console.log(resp);

                    })

            },
            opened() {
                this.status = 'Opened';
            },
            closed() {
                this.status = 'Closed';
            },
            submit(token) {
                console.log('token', token);
                console.log('Submit this token to your server to perform a stripe charge, or subscription.');
            },
        }
    });
</script>
</body>
</html>
