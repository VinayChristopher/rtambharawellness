<!doctype html>
<html lang="en">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->

    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">

    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <style>
        .icons-box-title {
            padding-top: 4%;
        }

        /*.icons-box.style-1.type-2 .icons-item .item-box > i .svg {*/
        /*    width: 100px;*/
        /*    height: 100px;*/
        /*}*/

        .justify {
            text-align: justify;
        }


        .item-carousel > .testimonial>p{
            color: black;
        }
    </style>


</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">About Us</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>About Us</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content">

        <div class="page-section">

            <div class="container">

                <div class="info-boxes flex-row item-col-2">

                    <div class="info-box">

                        <div class="box-img">
                            <img src="/images/logo_dark_square.png" alt="" width="360">
                        </div>

                        <h4 class="box-title"><a href="#">Our Philosophy</a></h4>
                        <p class="justify">
                            At Rtambhara Wellness, we aim to inspire people to uncover their transactional,
                            transformational and transcendental dimensions, dimensions that contribute to a happier and
                            more purposeful existence.
                        </p>
                        <p class="justify">
                            Through our programs, we aim to spread and nurture the wisdom and knowledge contained in our
                            Ancient Indian Scriptures "Sanatana Dharma." </p>

                        <p class="justify">
                            We believe these spiritual retreats that engage individuals in practicing holistic wellbeing
                            will empower people to live a life of Truth, Goodness, Beauty, and Contentment.
                        </p>
                        <p class="justify">
                            Eventually, we need to transcend our present level of consciousness to one that connects all
                            living beings.
                        </p>


                    </div>

                    {{--                    <div class="info-box">--}}

                    {{--                        <div class="box-img">--}}
                    {{--                            <img src="/images/logo_dark_square.png" alt="360">--}}
                    {{--                        </div>--}}

                    {{--                        <h4 class="box-title"><a href="#">Our Vision</a></h4>--}}
                    {{--                        <p>At Rtambhara Wellness, we are committed to building a community of happy individuals on a--}}
                    {{--                            journey to transform themselves physically, mentally and spiritually. Through our programs,--}}
                    {{--                            we aim to spread the knowledge and wisdom of ‘Sanatana Dharma’ including the Ancient Indian--}}
                    {{--                            Scriptures. We believe these spiritual retreats that engage individuals in practicing--}}
                    {{--                            holistic wellbeing will guide and empower people to live a life of Truth, Goodness, Beauty,--}}
                    {{--                            and Contentment, through raising one’s consciousness both at the individual and collective--}}
                    {{--                            levels.</p>--}}

                    {{--                    </div>--}}

                    <div class="info-box">

                        <div class="box-img">
                            <img src="/images/saffu-unsplash.jpg" alt="" width="360">
                        </div>

                        <h4 class="box-title"><a href="#">Our Values</a></h4>
                        <p class="justify">
                            Our core values are inspired by the Ṛgvedic injunction of making the universe
                            nobler in all its forms and expressions of life.
                        </p>
                        <p class="justify">
                            Our activities are created around these core values that in order for individuals to be in a
                            state of happiness, there should be a sense of harmony in various facets of their lives.
                        </p>
                        <p class="justify">
                            We are committed to helping individuals on their journey of self-discovery, through learning
                            and awareness, eventually leading to a life of “Fullness of Being”.
                        </p>

                    </div>

                </div>

            </div>

        </div>

        <!-- Page section -->
        <div class="page-section">

            <div class="container">

                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">

                        <h3 class="section-title">About Rtambhara Wellness</h3>

                        {{--                        <p class="content-element3 text-size-medium fw-medium">--}}
                        {{--                            Rtambhara means ”Truth and Right-Bearing.”--}}
                        {{--                        </p>--}}
                        {{--                        <p class="content-element4 " style="text-align: justify">--}}
                        {{--                            It refers to the ordered course of the manifested universe; the cosmic order which includes--}}
                        {{--                            all laws - natural, moral and &nbsp; spiritual - in their totality which are eternal and--}}
                        {{--                            inviolable.--}}
                        {{--                        </p>--}}
                        {{--                        <p class="content-element4 " style="text-align: justify">--}}
                        {{--                            There is also the aspect of Dynamic Equilibrium in the term Rtambhara. There is a constant--}}
                        {{--                            movement towards restoring and ascending to a higher level of Dynamic Equilibrium.--}}
                        {{--                        </p>--}}

                        <p class="content-element4 justify">
                            Rtambhara refers to the ordered course of the manifested universe; the cosmic order which
                            includes all laws - natural, moral and spiritual - in their totality which are eternal and
                            inviolable.
                        </p>
                        <p class="content-element4 justify">
                            Rtambhara Wellness is an endeavor to inspire those who are on a spiritual journey and want
                            to live their lives with meaning and purpose and uncover their inherent potential!
                        </p>
                        <a href="/team" class="btn btn-style-2">Meet Our Team</a>

                    </div>
                    <div class="col-lg-6 col-md-12">

                        <div class="video-holder">
                            {{--                <a href="https://www.youtube.com/embed/oX6I6vs1EFs?rel=0&amp;showinfo=0&amp;autohide=2&amp;controls=0&amp;playlist=J2Y_ld0KL-4&amp;enablejsapi=1" class="video-btn" data-fancybox="video"></a>--}}
                            <img src="/images/logo_dark.png" alt="" width="555">
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <!-- Page section -->
        <div class="page-section-bg">

            <div class="container">

                <div class="content-element5">
                    <h3 class="align-center">What We Offer</h3>

                    <p class="content-element4">
                        At Rtambhara, we envisage holistic wellness programs based on ancient wisdom "Sanatana Dharma"
                        and traditional therapeutic methods from India to restore the balance of the Body, Mind and
                        Atma. Learn a holistic way of life that brings the best of:
                    </p>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-12">

                        <div class="icons-box style-1 type-2">

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Spiritual-Learning.png"></i>

                                        <h5 class="icons-box-title">Spiritual learning</h5>
                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Yoga.png" alt=""></i>
                                        <h5 class="icons-box-title">Yoga</h5>
                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Meditation.png" alt=""></i>
                                        <h5 class="icons-box-title">Meditation</h5>
                                    </div>
                                </div>

                            </div>


                        </div>

                        {{--              <div class="services">--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Spiritual learning</h5>--}}
                        {{--                  <p>Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel.</p>--}}

                        {{--                </div>--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Yoga</h5>--}}
                        {{--                  <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.</p>--}}

                        {{--                </div>--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Meditation</h5>--}}
                        {{--                  <p>Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. </p>--}}

                        {{--                </div>--}}

                        {{--              </div>--}}

                    </div>
                    <div class="col-lg-6 col-md-12">

                        {{--              <div class="services">--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Ayurveda</h5>--}}
                        {{--                  <p>Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel.</p>--}}

                        {{--                </div>--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Satvik diet</h5>--}}
                        {{--                  <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.</p>--}}

                        {{--                </div>--}}

                        {{--                <div class="secvice-item">--}}

                        {{--                  <h5 class="service-title">Cultural exposure</h5>--}}
                        {{--                  <p>Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. </p>--}}

                        {{--                </div>--}}

                        {{--              </div>--}}


                        <div class="icons-box style-1 type-2">

                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Ayurveda.png" alt=""></i>
                                        <h5 class="icons-box-title">Ayurveda</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Satvik-Food.png" alt=""></i>
                                        <h5 class="icons-box-title">Satvik diet</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Cultural-Connect.png" alt=""></i>
                                        <h5 class="icons-box-title">Cultural exposure</h5>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

        <!-- Page section -->
    {{--        <div class="page-section">--}}

    {{--            <div class="container">--}}

    {{--                <div class="info-boxes flex-row item-col-2">--}}

    {{--                    <div class="info-box">--}}

    {{--                        <div class="box-img">--}}
    {{--                            <img src="/images/logo_dark_square.png" alt="" width="360">--}}
    {{--                        </div>--}}

    {{--                        <h4 class="box-title"><a href="#">Our Philosophy</a></h4>--}}
    {{--                        <p class="justify">--}}
    {{--                            At Rtambhara Wellness, we aim to inspire people to uncover their transactional,--}}
    {{--                            transformational and transcendental dimensions, dimensions that contribute to a happier and--}}
    {{--                            more purposeful existence.--}}
    {{--                        </p>--}}
    {{--                        <p class="justify">--}}
    {{--                            Through our programs, we aim to spread and nurture the wisdom and knowledge contained in our--}}
    {{--                            Ancient Indian Scriptures "Sanatana Dharma." </p>--}}

    {{--                        <p class="justify">--}}
    {{--                            We believe these spiritual retreats that engage individuals in practicing holistic wellbeing--}}
    {{--                            will empower people to live a life of Truth, Goodness, Beauty, and Contentment.--}}
    {{--                        </p>--}}
    {{--                        <p class="justify">--}}
    {{--                            Eventually, we need to transcend our present level of consciousness to one that connects all--}}
    {{--                            living beings.--}}
    {{--                        </p>--}}


    {{--                    </div>--}}

    {{--                    --}}{{--                    <div class="info-box">--}}

    {{--                    --}}{{--                        <div class="box-img">--}}
    {{--                    --}}{{--                            <img src="/images/logo_dark_square.png" alt="360">--}}
    {{--                    --}}{{--                        </div>--}}

    {{--                    --}}{{--                        <h4 class="box-title"><a href="#">Our Vision</a></h4>--}}
    {{--                    --}}{{--                        <p>At Rtambhara Wellness, we are committed to building a community of happy individuals on a--}}
    {{--                    --}}{{--                            journey to transform themselves physically, mentally and spiritually. Through our programs,--}}
    {{--                    --}}{{--                            we aim to spread the knowledge and wisdom of ‘Sanatana Dharma’ including the Ancient Indian--}}
    {{--                    --}}{{--                            Scriptures. We believe these spiritual retreats that engage individuals in practicing--}}
    {{--                    --}}{{--                            holistic wellbeing will guide and empower people to live a life of Truth, Goodness, Beauty,--}}
    {{--                    --}}{{--                            and Contentment, through raising one’s consciousness both at the individual and collective--}}
    {{--                    --}}{{--                            levels.</p>--}}

    {{--                    --}}{{--                    </div>--}}

    {{--                    <div class="info-box">--}}

    {{--                        <div class="box-img">--}}
    {{--                            <img src="/images/saffu-unsplash.jpg" alt="" width="360">--}}
    {{--                        </div>--}}

    {{--                        <h4 class="box-title"><a href="#">Our Values</a></h4>--}}
    {{--                        <p class="justify">--}}
    {{--                            Our core values are inspired by the Ṛgvedic injunction of making the universe--}}
    {{--                            nobler in all its forms and expressions of life.--}}
    {{--                        </p>--}}
    {{--                        <p class="justify">--}}
    {{--                            Our activities are created around these core values that in order for individuals to be in a--}}
    {{--                            state of happiness, there should be a sense of harmony in various facets of their lives.--}}
    {{--                        </p>--}}
    {{--                        <p class="justify">--}}
    {{--                            We are committed to helping individuals on their journey of self-discovery, through learning--}}
    {{--                            and awareness, eventually leading to a life of “Fullness of Being”.--}}
    {{--                        </p>--}}

    {{--                    </div>--}}

    {{--                </div>--}}

    {{--            </div>--}}

    {{--        </div>--}}

    <!-- Page section -->

        <div class="page-section-bg parallax-section" data-bg="/images/ben-unsplash.jpg"  style="margin-top: 10px ;margin-bottom: 10px ;">

            <div class="container">

                <div class="content-element5">
                    <h3 class="align-center">What Our Clients Say</h3>
                </div>

                <div class="carousel-type-1">

                    <div class="owl-carousel testimonial-holder style-2" data-max-items="1" data-autoplay="true">

                        <!-- Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial ">

                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>

                                    I feel enriched by the content. The Faculty enabled me to learn new concepts.
                                </p>

                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Iryna Oksiuk</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <!-- Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>

                                    I feel inspired to practice simple changes in my life. I was impressed by the
                                    quality of program materials.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Amit Dasgupta</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>

                                    The time and schedule were well planned and executed. I enjoyed my stay and
                                    accommodation.</p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mrs. Vidushi Jhunjhunwala</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    I enjoyed the food and beverages. I am excited to learn about other programs at
                                    Rtambhara.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Vineeta Lal</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    I found the complete program to be very inspirational. </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Shashi Kyal</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    I have learnt far more than I had expected in this short duration.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mrs. Sutapa Dasgupta</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    Overall a wonderful experience. The faculty & staff were very open and warm.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Devika</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    Rtambhara program is simply amazing.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Ajay Kumar J.M.</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    Liked the overall program, topics covered by all the speakers about yoga and
                                    meditation. </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Sameer Mirchandani</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    Had an enjoyable experience.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Shruthi Surendran</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    Need of the hour. Congratulations Rtambhara!!
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. M.R. Shashidhar</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>
                                    The program of Rtambhara has been a great break from the monotonous professional
                                    duties and inspired me to look beyond .This program had been extremely
                                    rejuvenating for me.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Honorary Consul General of Czech Republic Mr. Pushpak
                                            Prakash
                                        </div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                <p>

                                    When I entered I walked in as the Honorary Consul General of Peru but when I
                                    walk out I will be the brand ambassador for Rtambhara. Rtambhara is one of the
                                    ecosystem for holistic healing. Through Rtambhara we can upgrade ourselves. If
                                    you know yourself this nothing more you need to know and if you want to heal the
                                    soul, Rtambhara is the first step. Rtambhara wellness will be something bigger
                                    and higher. It’s a great path towards humanity.
                                </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Honorary Consul General of Peru Mr. Vikram Vishwanath
                                        </div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                    </div>

                </div>

            </div>

        </div>

        <!-- Page section -->
        {{--      <div class="page-section">--}}
        {{--        --}}
        {{--        <div class="container">--}}

        {{--          <div class="content-element5">--}}
        {{--            <h3 class="align-center">Inside The Studio</h3>--}}
        {{--          </div>--}}
        {{--          --}}
        {{--          <div class="carousel-type-2">--}}
        {{--            --}}
        {{--            <div class="owl-carousel portfolio-holder" data-max-items="3" data-item-margin="30">--}}
        {{--              --}}
        {{--              <!-- Owl item -->--}}
        {{--              <div class="project">--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--          --}}
        {{--                <div class="project-image">--}}
        {{--          --}}
        {{--                  <img src="/themes/wellness/images/360x286_img1.jpg" alt="">--}}
        {{--          --}}
        {{--                  <a href="images/360x286_img1.jpg" class="project-link" data-fancybox="group"></a>--}}
        {{--          --}}
        {{--                </div>--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--        --}}
        {{--              </div>--}}

        {{--              <!-- Owl item -->--}}
        {{--              <div class="project">--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--          --}}
        {{--                <div class="project-image">--}}
        {{--          --}}
        {{--                  <img src="/themes/wellness/images/360x286_img2.jpg" alt="">--}}
        {{--          --}}
        {{--                  <a href="images/360x286_img2.jpg" class="project-link" data-fancybox="group"></a>--}}
        {{--          --}}
        {{--                </div>--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--        --}}
        {{--              </div>--}}

        {{--              <!-- Owl item -->--}}
        {{--              <div class="project">--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--          --}}
        {{--                <div class="project-image">--}}
        {{--          --}}
        {{--                  <img src="/themes/wellness/images/360x286_img3.jpg" alt="">--}}
        {{--          --}}
        {{--                  <a href="images/360x286_img3.jpg" class="project-link" data-fancybox="group"></a>--}}
        {{--          --}}
        {{--                </div>--}}
        {{--          --}}
        {{--                <!-- - - - - - - - - - - - - - End of Project Image - - - - - - - - - - - - - - - - -->--}}
        {{--        --}}
        {{--              </div>--}}
        {{--          --}}
        {{--            </div>--}}
        {{--          --}}
        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}
        {{--      --}}
    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->


@include('layouts.footer')


<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

    <div id="popup-sign" class="popup var3">

        <div class="popup-inner">

            <button type="button" class="close-popup"></button>

            <h4 class="title">Sign Up For Free</h4>
            <p>Already have an account? <a href="#" class="link-text var2 popup-btn-login">Login Here</a></p>
            <a href="#" class="btn fb-btn btn-big">Sign Up With Facebook</a>
            <p class="content-element2">OR</p>
            <form class="content-element2">

                <input type="text" placeholder="Enter Your Email Address">
                <input type="text" placeholder="Password">
                <a href="#" class="btn btn-style-3 btn-big">Join Free Now!</a>

            </form>

            <p class="text-size-small">By signing up you agree to <a href="#" class="link-text">Terms of Service</a></p>

        </div>

    </div>

    <div id="popup-login" class="popup var3">

        <div class="popup-inner">

            <button type="button" class="close-popup"></button>

            <h4 class="title">Login</h4>
            <p>Don't have an account yet?<a href="#" class="link-text var2 popup-btn-sign">JOIN FOR FREE</a></p>
            <a href="#" class="btn fb-btn btn-big"> Login With Facebook</a>
            <p class="content-element2">OR</p>
            <form class="content-element1">

                <input type="text" placeholder="Enter Your Email Address">
                <input type="text" placeholder="Password">
                <a href="#" class="btn btn-style-3 btn-big">Login</a>

                <div class="input-wrapper">
                    <input type="checkbox" id="checkbox" name="checkbox">
                    <label for="checkbox">Remember me</label>
                </div>

            </form>

            <p class="text-size-small"><a href="#" class="link-text">Forgot your password?</a></p>

        </div>

    </div>

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
