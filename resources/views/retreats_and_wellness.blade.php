<!doctype html>
<html lang="en">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

<!-- Basic Page Needs
================================================== -->

<title>Rtambhara Wellness</title>

<!--meta info-->
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>


<link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/themes/wellness/css/fontello.css">
<link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
<link rel="stylesheet" href="/themes/wellness/css/style.css">
<link rel="stylesheet" href="/themes/wellness/css/responsive.css">
<link rel="stylesheet" href="/themes/wellness/css/custom.css">


<style>
    .img-title{
        width: 40%;
    }
</style>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap  banner-3" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Our Retreats & Wellness Programs</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Our Retreats & Wellness Programs</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

        <div class="container">

            <div class="challenges flex-row item-col-2">

                <div class="item-col">

                    <div class="challenge-item">

                        <div class="bg-img" data-bg="/images/retreats-headers/pravesa.jpg"></div>
                        <a href="#" class="link"></a>

                        <div class="inner">
{{--                            Praveśa--}}
                            <h3 class="item-title"><a href="/program-details"><img class="img-responsive img-title" src="/images/program-titles/white/pravesa.png"/><br>Rejuvenate Program</a></h3>
                            <div class="our-info style-3">

                                <div class="info-item">
                                    <i class="licon-play-circle"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">2 Nights & 3 Days</a>
                                    </div>
                                </div>
                                <div class="info-item">
                                    <i class="licon-signal"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">Beginner</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="item-col">

                    <div class="challenge-item">

                        <div class="bg-img" data-bg="/images/retreats-headers/prabodha.jpg"></div>
                        <a href="#" class="link"></a>
                        <div class="inner">
                            <h3 class="item-title"><a href="/program-details"><img class="img-responsive img-title" src="/images/program-titles/white/prabodha.png" alt="Prabōdha"/><br>Reflect Program</a></h3>
                            <div class="our-info style-3">

                                <div class="info-item">
                                    <i class="licon-play-circle"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">7 Nights & 8 Days</a>
                                    </div>
                                </div>
                                <div class="info-item">
                                    <i class="licon-signal"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">Intermediate</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="item-col">

                    <div class="challenge-item">

                        <div class="bg-img" data-bg="/images/retreats-headers/pravara.jpg"></div>
                        <a href="#" class="link"></a>
                        <div class="inner">
                            <h3 class="item-title"><a href="/program-details"><img class="img-responsive img-title" src="/images/program-titles/white/pravara.png" alt="Pravara"/><br>Restore Program</a></h3>

                            <div class="our-info style-3">

                                <div class="info-item">
                                    <i class="licon-play-circle"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">14 Nights & 15 Days</a>
                                    </div>
                                </div>
                                <div class="info-item">
                                    <i class="licon-signal"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">Advanced</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="item-col">

                    <div class="challenge-item">

                        <div class="bg-img" data-bg="/images/retreats-headers/praveena.jpg"></div>
                        <a href="#" class="link"></a>
                        <div class="inner">
                            <h3 class="item-title"><a href="/program-details"><img class="img-responsive img-title" src="/images/program-titles/white/pravina.png" alt="Pravīna"/><br>Remaster Program</a></h3>
                            <div class="our-info style-3">

                                <div class="info-item">
                                    <i class="licon-play-circle"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">21 Nights & 22 Days</a>
                                    </div>
                                </div>
                                <div class="info-item">
                                    <i class="licon-signal"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">Expert</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="item-col">

                    <div class="challenge-item">

                        <div class="bg-img" data-bg="/images/retreats-headers/prajna.jpg"></div>
                        <a href="#" class="link"></a>
                        <div class="inner">
                            <h3 class="item-title"><a href="/program-details"><img class="img-responsive img-title" src="/images/program-titles/white/prajna.png" alt="Prājña"/><br>Reconnect Program</a></h3>

                            <div class="our-info style-3">

                                <div class="info-item">
                                    <i class="licon-play-circle"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">30 Nights & 31 Days</a>
                                    </div>
                                </div>
                                <div class="info-item">
                                    <i class="licon-signal"></i>
                                    <div class="wrapper">
                                        <a href="/program-details">Practitioner</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

{{--                <div class="item-col">--}}

{{--                    <div class="challenge-item">--}}

{{--                        <div class="bg-img" data-bg="/images/kelly-unsplash.jpg"></div>--}}
{{--                        <a href="#" class="link"></a>--}}
{{--                        <div class="inner">--}}
{{--                            <h3 class="item-title"><a href="/program-details">Corporate or Special <br> Tailored Programs</a></h3>--}}
{{--                            <div class="our-info style-3">--}}

{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-play-circle"></i>--}}
{{--                                    <div class="wrapper">--}}
{{--                                        <a href="/program-details">21 Classes</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="info-item">--}}
{{--                                    <i class="licon-signal"></i>--}}
{{--                                    <div class="wrapper">--}}
{{--                                        <a href="/program-details">Intermediate </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}

{{--                </div>--}}

            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
