<!doctype html>
<html lang="en">
<head>
	<!-- Google Web Fonts
	================================================== -->


    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->

    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">

    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <style>

        .pravesa{

        }
    </style>
</head>

<body>

  <div class="loader"></div>

  <!--cookie-->
  <!-- <div class="cookie">
          <div class="container">
            <div class="clearfix">
              <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
              <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
            </div>
          </div>
        </div>-->

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      @include('layouts.header')

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="margin-top: 10%">

      <div class="container">

        <h1 class="page-title">Past Events</h1>

        <ul class="breadcrumbs">

          <li><a href="/">Home</a></li>
          <li>Past Events</li>
        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

      <div class="container full-width">

        <div id="options">
          <div id="filters" class="isotope-nav">
            <button class="is-checked" data-filter="*">All Events</button>
            <button data-filter=".pravesa">Pravesa - 27th & 28th July,2019 </button>
{{--            <button data-filter=".category_5">Video</button>--}}
{{--            <button data-filter=".category_6">Workshops</button>--}}
          </div>
        </div>

        <!-- Isotope -->
        <div class="portfolio-holder isotope fourth-collumn clearfix" data-isotope-options='{"itemSelector" : ".item","layoutMode" : "masonry","transitionDuration":"0.7s","masonry" : {"columnWidth":".item"}}'>

          <!-- Isotope item -->


            <div class="item pravesa">

                <div class="project">

                    <div class="project-image">
                        <div class="video-holder">
                            <a href="/videos/event-1/video_wellness.mp4" class="video-btn" data-fancybox="video"></a>
                            <img src="/videos/event-1/thumbnail.jpeg" alt="">
                        </div>

                    </div>

                </div>

            </div>

           @for($i=1 ;$i<26 ;$i++)
            <div class="item pravesa">

            <div class="project">

              <div class="project-image">

                <img src="/images/past-events/event-1/event1-{{$i}}.jpeg" alt="">

                <a href="/images/past-events/event-1/event1-{{$i}}.jpeg" class="project-link var2" data-fancybox="gallery"></a>

{{--                <div class="project-description">--}}

{{--                  <h5 class="project-title"><a href="#">Praesent Justo Dolor</a></h5>--}}

{{--                  <a href="#" class="project-cats">Poses</a>--}}

{{--                </div>--}}

              </div>

            </div>

          </div>
          @endfor



        </div>



          {{--        <div class="align-center">--}}
{{--          <ul class="pagination">--}}
{{--            <li><a href="#" class="prev-page"></a></li>--}}
{{--            <li><a href="#">1</a></li>--}}
{{--            <li class="active"><a href="#">2</a></li>--}}
{{--            <li><a href="#">3</a></li>--}}
{{--            <li><a href="#" class="next-page"></a></li>--}}
{{--          </ul>--}}
{{--        </div>--}}

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      @include('layouts.footer')

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
  <script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
  <script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
  <script src="/themes/wellness/js/libs/retina.min.js"></script>
  <script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
  <script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
  <script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
  <script src="/themes/wellness/plugins/mad.customselect.js"></script>
  <script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
  <script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="/themes/wellness/js/plugins.js"></script>
  <script src="/themes/wellness/js/script.js"></script>

</body>
</html>
