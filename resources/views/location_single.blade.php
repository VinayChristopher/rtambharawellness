<!doctype html>
<html lang="en">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->


    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <script src="https://unpkg.com/vue/dist/vue.min.js"></script>
    {{--    <script src="https://unpkg.com/vue-stripe-checkout/build/vue-stripe-checkout.js"></script>--}}


    <style>
        .custom-list{
            list-style: disc !important;
        }
        .custom-list > li:not(:last-child){
            margin-bottom: 0;
        }

    </style>

    <script src="/js/cal.js"></script>
    <script>
        function dismiss(el) {
            el.parentNode.style.display = 'none';
        };
    </script>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap no-title" style="margin-top:10%">

        <div class="container">

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Locations</li>
                <li>{{$location->heading}}</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap" style="padding-top:5%">

        <div class="container">

            <div class="content-element">

                <div class="content-element4">
                    <div class="page-nav">
                        <div class="flex-item"><a href="/locations" class="info-btn prev-btn">All Events</a></div>
                    </div>
                </div>

                <div class="content-element5">
                    <div class="entry-box single-post">

                        <div class="entry">

                            <div class="content-element4">
                                <div class="entry-body">

                                    <h3 class="entry-title">{{$location->heading}}</h3>

                                </div>
                            </div>

                            <div class="content-element2">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12"><img src="{{$location->main_image}}" alt="">
                                    </div>
{{--                                    <div class="col-lg-4 col-md-12">--}}
{{--                                        <div id="googleMap" class="map-container"></div>--}}
{{--                                    </div>--}}
                                </div>

                            </div>


                            {!! html_entity_decode($location->body) !!}

                        </div>

                    </div>
                </div>

                <h4>Gallery</h4>

                <!-- Isotope -->
                <div class="portfolio-holder isotope fourth-collumn clearfix"
                     data-isotope-options='{"itemSelector" : ".item","layoutMode" : "masonry","transitionDuration":"0.7s","masonry" : {"columnWidth":".item"}}'>

                    <!-- Isotope item -->

                    @for($i=1 ;$i<=$location->image_count ;$i++)
                        <div class="item goa ">

                            <div class="project">

                                <div class="project-image">

                                    <img src="{{$location->image_prefix}}{{$i}}.jpg" alt="" style="height: 200px">

                                    <a href="{{$location->image_prefix}}{{$i}}.jpg" class="project-link var2"
                                       data-fancybox="gallery"></a>

                                </div>

                            </div>

                        </div>
                    @endfor

                </div>

            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

    @include('layouts.footer')


</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


<script>

    // Vue.use(VueStripeCheckout, 'pk_test_jvbkvHL3HF4XhFVbyP9axp4o00PRxXGS23');


    var vm = new Vue({
        el: '#app',
        data() {
            return {
                full_nam: 'M F',
                program_id: '1',
                status: 'Closed',
                tokenFromPromise: {},
                tokenFromEvent: {},
                sampleCard: '4242 4242 4242 4242',
                image: '/images/logo.png',
                name: 'Rtahmbhara Wellness',
                description: '',
                currency: 'INR',
                amount: 100,
                quantity: '1',
                email: 'a@b.com',
                first_name: '',
                last_name: '',
                address: '',
                phone: '',


            }
        },

        computed: {
            finalAmount() {
                return this.amount * 100;
            }
        },
        mounted() {
            this.program_id = this.getMeta("program_id");
            // console.log(this.program_id);
        },
        methods: {
            getMeta(metaName) {
                const metas = document.getElementsByTagName('meta');

                for (let i = 0; i < metas.length; i++) {
                    if (metas[i].getAttribute('name') === metaName) {
                        return metas[i].getAttribute('content');
                    }
                }

                return '';
            },

            showModal() {
                // console.log("modal opened");
                var modal = document.getElementById("my-modal");
                modal.style.display = "block";
            },
            closeModal() {
                var modal = document.getElementById("my-modal");
                modal.style.display = "none";
                // console.log("modal closed");
                this.checkout();
            },


            async checkout() {
                this.tokenFromPromise = await this.$refs.checkoutRef.open();
            },


            done(token) {
                this.tokenFromEvent = token;

                console.log('-----------------------');
                console.log(token.token.id);

                var data = {
                    'token': token.token,
                    'program_id': this.program_id,
                    'email': this.email,
                    'quantity': this.quantity,
                    'first_name': this.first_name,
                    'last_name': this.last_name,
                    'address': this.address,
                    'phone': this.phone,
                };


                axios.post('/api/bookings', data)
                    .then(function (resp) {

                        console.log(resp);

                    })

            },
            opened() {
                this.status = 'Opened';
            },
            closed() {
                this.status = 'Closed';
            },
            submit(token) {
                console.log('token', token);
                console.log('Submit this token to your server to perform a stripe charge, or subscription.');
            },
        }
    });
</script>
</body>
</html>
