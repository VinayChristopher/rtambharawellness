<!doctype html>
<html lang="en">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->

    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>


    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">

    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/themes/wellness/plugins/revolution/css/settings.css">
{{--    <link rel="stylesheet" href="/themes/wellness/plugins/revolution/css/layers.css">--}}
    <link rel="stylesheet" href="/themes/wellness/plugins/revolution/css/navigation.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">


    <style>
        .icons-box-title {
            padding-top: 4%;

        }

        /*.icons-box.style-1.type-2 .icons-item .item-box > i .svg {*/
        /*    width: 100px;*/
        /*    height: 100px;*/
        /*}*/

        .slideDown div > div > div > div > div.nav-item > .main-navigation > ul > li > a {
            color: #333;
        }

        .main-navigation > ul > li > a {
            color: #ffffff;
        }

        .dark:after {
            content: '\A';
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.6);
            opacity: 0;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
        }

        .dark:hover:after {
            opacity: 1;
            text-decoration: black;
        }

        /*.info-boxes.style-2 .info-box:hover > p, .info-boxes.style-2 .info-box:hover > .btn {*/
        /*    opacity: 1;*/
        /*    color: black;*/
        /*    font-weight: bolder;*/
        /*}*/
        .info-box > p {
            font-weight: 400;
        }

        #logo-dark {
            display: none !important;
        }

        #logo-light {
            display: block !important;
        }

        .slideDown #logo-dark {
            display: block !important;
        }

        .slideDown #logo-light {
            display: none !important;
        }

        .justify {
            text-align: justify
        }

        .item-carousel > .testimonial>p{
            color: black;
        }

    </style>


</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Revolution Slider - - - - - - - - - - - - - - - - -->

    <div class="rev-slider-wrapper">

        <div id="rev-slider" class="rev-slider tp-simpleresponsive" data-version="5.0">

            <ul>

                <li data-transition="fade">

                    <img src="/images/abdura.jpg" class="rev-slidebg" alt="">

                    <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="30"
                         data-y="['middle','middle','middle','middle']" data-voffset="-110"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">
                        <div class="section-pre-title">welcome to, Rtambhara Wellness</div>
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                    <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme scaption-dark-large"
                         data-x="['left','left','left','left']" data-hoffset="30"
                         data-y="['middle','middle','middle','middle']" data-voffset="0"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">To Uncover, <br> Nurture & Transcend
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                    <!-- - - - - - - - - - - - - - Layer 3 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme"
                         data-x="['left','left','left','left']" data-hoffset="30"
                         data-y="['middle','middle','middle','middle']" data-voffset="130"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":750,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">
                        <a href="/retreats-and-wellness" class="btn btn-big btn-style-3">Our Programs</a>
                        <a href="/about-us" class="btn btn-big">More About Us</a>
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 3 - - - - - - - - - - - - - - - - -->

                </li>

                <li data-transition="fade">

                    <img src="/images/chinh-unsplash.jpg" class="rev-slidebg" alt="">

                    <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme"
                         data-x="['right','right','right','right']" data-hoffset="410"
                         data-y="['middle','middle','middle','middle']" data-voffset="-110"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">
                        {{--                <div class="section-pre-title">Student Special</div>--}}
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                    <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme scaption-dark-large"
                         data-x="['right','right','right','right']" data-hoffset="50"
                         data-y="['middle','middle','middle','middle']" data-voffset="0"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">Satvik Diet Regimen
                        {{--                <br><span>$39</span>--}}
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                    <!-- - - - - - - - - - - - - - Layer 3 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme"
                         data-x="['right','right','right','right']" data-hoffset="440"
                         data-y="['middle','middle','middle','middle']" data-voffset="130"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05"><a href="/retreats-and-wellness" class="btn btn-big">Learn More</a>
                    </div>

                    <!-- - - - - - - - - - - - - - End of Layer 3 - - - - - - - - - - - - - - - - -->

                </li>

                <li data-transition="fade">

                    <img src="/images/indian-yogi.jpg" class="rev-slidebg" alt="">

                    <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->

                    <div class="tp-caption tp-resizeme scaption-dark-large align-center"
                         data-x="['center','center','center','center']" data-hoffset="0"
                         data-y="['middle','middle','middle','middle']" data-voffset="-20"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                         data-responsive_offset="on"
                         data-elementdelay="0.05">
                        <div>Wide Range of Programs
                            {{--                <br> Workshops & Events--}}
                        </div>

                        <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->

                        <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->

                        <div class="tp-caption tp-resizeme"
                             data-x="['center','center','center','center']" data-hoffset="0"
                             data-y="['middle','middle','middle','middle']" data-voffset="120"
                             data-whitespace="nowrap"
                             data-frames='[{"delay":750,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                             data-responsive_offset="on"
                             data-elementdelay="0.05"><a href="#" class="btn btn-big btn-style-3">more Details</a>
                            {{--              <a href="#" class="btn btn-big">Download our APP</a>--}}
                        </div>

                        <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->

                    </div>
                </li>
            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - - End of Slider - - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->


    <div id="content">

        <!-- Info box -->
        <div class="info-boxes style-2 flex-row no-gutters item-col-4">

            <div class="info-box " data-bg="/images/saffu-p-unsplash.jpg" data-width="480">

                <h3 class="box-title"><a>True Wellbeing</a></h3>
                <p>
                    At Rtambhara, we believe that true wellbeing consists of getting our 8 dimensions of wellness in a
                    "personal harmony" as opposed to a balance, and finally transcending our 8 dimensions to the 9th
                    dimension, which is transcendental wellbeing. In this transcendental state, we are connected with
                    our Higher Selves and will experience unending bliss and a strong interconnectedness with all living
                    beings, which will aid the evolution of global consciousness!
                </p>
                {{--          <p>Aenean auctor wisi et urna. Aliquam <br> erat volutpat. Duis ac turpis. Donec sit <br> amet eros. Lorem ipsum dolor sit amet.</p>--}}
                {{--                <a href="/retreats-and-wellness" class="btn btn-style-2">Read More</a>--}}

            </div>

            <div class="info-box " data-bg="/images/page-11-bg.png">

                <h3 class="box-title"><a>Locations</a></h3>
                <p>Our carefully selected and curated programs not only bring you picture-perfect destinations amidst
                    hills and sun-kissed beaches,
                    <br>but offers comfortable and convenient accommodation with a dash of traditional and natural
                    elements.</p>
                {{--                <a href="/retreats-and-wellness" class="btn btn-style-2">Read More</a>--}}

            </div>

            <div class="info-box" data-bg="/images/page-12-bg.png">

                <h3 class="box-title"><a>Nutrition</a></h3>
                <p>Goodness in your food, adds to your wellness. This is given utmost importance at Rtambhara, where
                    carefully crafted menus and dishes are prepared from naturally ripened & organic fresh produce.
                    Following the Satvik diet regimen, our chefs will ensure you are served with healthy yet
                    flavorsome food.</p>
                {{--                <a href="/retreats-and-wellness" class="btn btn-style-2">Read More</a>--}}

            </div>

            <div class="info-box" data-bg="/images/page-13-bg.png">

                <h3 class="box-title"><a>Cultural Connect</a></h3>
                <p>Adding an element of fun, Rtambhara ensures you soak in the local culture by organizing a host of
                    activities such as folk dance performances, music, culinary classes, gardening and fine art
                    programs.<br>
                    We also encourage our guests to explore the destination and soak in the most that these lands have
                    to offer.
                </p>
                {{--                <a href="/retreats-and-wellness" class="btn btn-style-2">Read More</a>--}}

            </div>

        </div>

        <!-- Page section -->
        <div class="page-section half-bg-col var2 " style="margin-top: 10px">

            <div class="img-col-right">
                {{--                <a--}}
                {{--                    href="https://www.youtube.com/embed/oX6I6vs1EFs?rel=0&amp;showinfo=0&amp;autohide=2&amp;controls=0&amp;playlist=J2Y_ld0KL-4&amp;enablejsapi=1"--}}
                {{--                    class="video-btn" data-fancybox="video"></a>--}}
                {{--                --}}
                <div class="col-bg">
                    <img src="/images/logo_dark.png" width="960">
                </div>
            </div>

            <div class="container wide3">

                <div class="row align-content-center">

                    <div class="col-lg-6">

                        <h2>What is Rtambhara?</h2>

                        {{--                        <p class="content-element3 text-size-medium fw-medium">--}}
                        {{--                            Rtambhara means ”Truth and Right-Bearing.”--}}
                        {{--                        </p>--}}
                        <p class="content-element4">
                            Rtambhara refers to the ordered course of the manifested universe; the cosmic order which
                            includes all laws - natural, moral and spiritual - in their totality which are eternal and
                            inviolable.
                        </p>
                        <p class="content-element4">
                            Rtambhara Wellness is an endeavor to inspire those who are on a spiritual journey and want
                            to live their lives with meaning and purpose and uncover their inherent potential!
                        </p>
                        <a href="/about-us" class="btn btn-style-2">Read more</a>

                    </div>
                    <div class="col-lg-6"></div>

                </div>

            </div>

        </div>

        <!-- Page section -->
        <div class="page-section half-bg-col var2">

            <div class="img-col-left">
                <div class="col-bg" data-bg="/images/bekir-unsplash.jpg"></div>
            </div>

            <div class="container wide3">

                <div class="row align-content-center">

                    <div class="col-lg-6"></div>
                    <div class="col-lg-6">

                        <div class="content-element5">
                            <h2 class="section-title">What We Offer</h2>

                            <p class="content-element4">
                                At Rtambhara, we envisage holistic wellness programs based on ancient wisdom "Sanatana
                                Dharma" and traditional therapeutic methods from India to restore the balance of the
                                Body, Mind and Atma. Learn a holistic way of life that brings the best of:
                            </p>
                        </div>

                        <div class="icons-box style-1 type-2">

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Spiritual-Learning.png" ></i>

                                        <h5 class="icons-box-title">Spiritual learning</h5>
                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Yoga.png" alt=""></i>
                                        <h5 class="icons-box-title">Yoga</h5>
                                    </div>
                                </div>

                            </div>

                            <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Meditation.png" alt=""></i>
                                        <h5 class="icons-box-title">Meditation</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Ayurveda.png" alt=""></i>
                                        <h5 class="icons-box-title">Ayurveda</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Satvik-Food.png" alt=""></i>
                                        <h5 class="icons-box-title">Satvik diet</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="icons-wrap">

                                <div class="icons-item">
                                    <div class="item-box">
                                        <i><img class="svg" src="/images/icons/Cultural-Connect.png" alt=""></i>
                                        <h5 class="icons-box-title">Cultural exposure</h5>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- Page section -->
        <div class="page-section half-bg-col var2">

            <div class="img-col-right video-holder">
                <div class="col-bg" data-bg="/images/alvaro-unsplash.jpg"></div>
            </div>

            <div class="container wide3">

                <div class="row align-content-center">

                    <div class="col-lg-6">

                        <h2>The Rtambhara Community</h2>

                        {{--                        <p class="content-element3 text-size-medium fw-medium">Aenean auctor wisi et urna. Aliquam erat--}}
                        {{--                            volutpat. <br> Duis ac turpis. Integer rutrum ante eu lacus.</p>--}}
                        <p class="content-element4">
                            We are a collective group that have at their core the philosophy that wellbeing starts at
                            the physical and culminates in the transcendental! Each member of the team is on a spiritual
                            path of discovery. We feel that we could help empower others through this collective to
                            understand how to live life in tune with cosmic rhythm and balance....
                        </p>
                        <a href="/team" class="btn btn-style-2">Meet Our Team</a>

                    </div>
                    <div class="col-lg-6"></div>

                </div>

            </div>

        </div>

        <div class="page-section">

            <div class="container wide3">

                <div class="content-element6">
                    <div class="align-center">
                        <h2>Top Retreat & Wellness Destinations</h2>
                        <p class="text-size-medium">Explore the most popular exotic destinations with our retreats &
                            wellness programs</p>
                    </div>
                </div>

                <div class="challenges masonry flex-row item-col-2">

                    <div class="item-col full-size">

                        <div class="challenge-item">

                            <div class="bg-img" data-bg="/images/mandrem.jpg"></div>
                            <a href="#" class="link"></a>
                            <div class="inner">
                                <h3 class="item-title"><a href="/location-single/mandrem-village-resort">Mandrem Village Resort</a></h3>
                                <a href="/location-single/mandrem-village-resort" class="item-country">Goa, India</a>
                            </div>

                        </div>

                    </div>

                    <div class="item-col">

                        <div class="flex-row item-col-1">

                            <div class="item-col half-size">

                                <div class="challenge-item">

                                    <div class="bg-img" data-bg="/images/wildgrass.jpg"></div>
                                    <a href="#" class="link"></a>
                                    <div class="inner">
                                        <h3 class="item-title"><a href="/location-single/wild-grass-nature-resort">Wild Grass Nature</a></h3>
                                        <a href="/location-single/wild-grass-nature-resort" class="item-country">Sri Lanka</a>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="flex-row item-col-2">

                            <div class="item-col half-size">

                                <div class="challenge-item">

                                    <div class="bg-img" data-bg="/images/nepal.jpg"></div>
                                    <a href="#" class="link"></a>
                                    <div class="inner">
                                        <h3 class="item-title"><a href="/location-single/kaivayla-yoga">Kaivalya Yogo</a></h3>
                                        <a href="/location-single/kaivayla-yoga" class="item-country">Nepal</a>
                                    </div>

                                </div>

                            </div>

                            <div class="item-col half-size">

                                <div class="challenge-item" style="background-color: #a47d98 !important;">

                                    <div ></div>
                                    <a href="#" class="link"></a>
                                    <div class="inner">
                                        <h3 class="item-title"><a href="/locations">More Locations</a></h3>
                                        <a href="/locations" class="item-country"></a>
                                    </div>

                                </div>



                            </div>

                        </div>


                    </div>

                </div>

            </div>

        </div>

        <!-- Page section -->
{{--        <div class="page-section-bg  text-color-light" data-bg="/images/simon-unsplash.png">--}}

{{--            --}}{{--            <div class="img-col-left">--}}
{{--            --}}{{--                <div class="col-bg" data-bg="/themes/wellness/images/960x884_bg1.jpg"></div>--}}
{{--            --}}{{--            </div>--}}
{{--            --}}{{--            <div class="img-col-right">--}}
{{--            --}}{{--                <div class="col-bg" data-bg="/images/simon-unsplash.jpg"></div>--}}

{{--            --}}{{--            </div>--}}

{{--            <div class="container wide2">--}}

{{--                <div class="row align-content-center">--}}

{{--                    <div class="col-lg-5">--}}

{{--                        <div class="content-element4">--}}

{{--                            <div class="section-pre-title">Retreats & Wellness Programs</div>--}}
{{--                            <h2 class="section-title">Events</h2>--}}

{{--                        </div>--}}

{{--                        <div class="carousel-type-2">--}}

{{--                            <div class="owl-carousel flex-row entry-box style-2" data-max-items="2"--}}
{{--                                 data-item-margin="30">--}}

{{--                                <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->--}}
{{--                                <div class="entry">--}}

{{--                                    <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->--}}
{{--                                    <div class="thumbnail-attachment">--}}
{{--                                        <a href="#"><img src="/images/jay-unsplash-small.jpg" alt=""></a>--}}
{{--                                    </div>--}}

{{--                                    <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->--}}
{{--                                    <div class="entry-body">--}}

{{--                                        <h6 class="entry-title"><a href="#">Prevesa 3 Day Program</a></h6>--}}
{{--                                        <div class="our-info vr-type">--}}

{{--                                            <div class="info-item">--}}
{{--                                                <i class="licon-clock3"></i>--}}
{{--                                                <div class="wrapper">--}}
{{--                                                    <span>Saturday 27th July, 2019 for 3 Days</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="info-item">--}}
{{--                                                <i class="licon-map-marker"></i>--}}
{{--                                                <div class="wrapper">--}}
{{--                                                    <span>India</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                        <div class="entry-price">--}}

{{--                                            --}}{{--                                            <span class="product-price">Rs.100</span>--}}
{{--                                            <a href="/upcoming-events" class="btn btn-small">View Events</a>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                                <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->--}}
{{--                                <div class="entry">--}}

{{--                                    <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->--}}
{{--                                    <div class="thumbnail-attachment">--}}
{{--                                        <a href="#"><img src="/images/jay-unsplash-small.jpg" alt=""></a>--}}
{{--                                    </div>--}}

{{--                                    <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->--}}
{{--                                    <div class="entry-body">--}}

{{--                                        <h6 class="entry-title"><a href="#">Prerana - London Group Program</a></h6>--}}
{{--                                        <div class="our-info vr-type">--}}

{{--                                            <div class="info-item">--}}
{{--                                                <i class="licon-clock3"></i>--}}
{{--                                                <div class="wrapper">--}}
{{--                                                    <span>Monday 9th September, 2019 for 14 Nights & 15 Days</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="info-item">--}}
{{--                                                <i class="licon-map-marker"></i>--}}
{{--                                                <div class="wrapper">--}}
{{--                                                    <span>India</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                        <div class="entry-price">--}}

{{--                                            --}}{{--                                            <span class="product-price">Rs.100</span>--}}
{{--                                            <a href="/upcoming-events" class="btn btn-small">View Events</a>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                </div>--}}

{{--                            </div>--}}

{{--                            --}}{{--                            <a href="#" class="btn btn-style-2">More Events</a>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="col-lg-6 offset-lg-1"></div>--}}

{{--                </div>--}}

{{--            </div>--}}

{{--        </div>--}}

        <div class="page-section-bg parallax-section" data-bg="/images/ben-unsplash.jpg"  style="margin-top: 10px ;margin-bottom: 10px ;">

            <div class="container">

                <div class="content-element5">
                    <h3 class="align-center">What Our Clients Say</h3>
                </div>

                <div class="carousel-type-1">

                    <div class="owl-carousel testimonial-holder style-2" data-max-items="1" data-autoplay="true">

                        <!-- Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial ">

                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>

                                        I feel enriched by the content. The Faculty enabled me to learn new concepts.
                                    </p>

                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Iryna Oksiuk</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <!-- Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>

                                        I feel inspired to practice simple changes in my life. I was impressed by the
                                        quality of program materials.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Amit Dasgupta</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>

                                        The time and schedule were well planned and executed. I enjoyed my stay and
                                        accommodation.</p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mrs. Vidushi Jhunjhunwala</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        I enjoyed the food and beverages. I am excited to learn about other programs at
                                        Rtambhara.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Vineeta Lal</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        I found the complete program to be very inspirational. </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Shashi Kyal</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        I have learnt far more than I had expected in this short duration.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mrs. Sutapa Dasgupta</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->
                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        Overall a wonderful experience. The faculty & staff were very open and warm.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Devika</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        Rtambhara program is simply amazing.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Ajay Kumar J.M.</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        Liked the overall program, topics covered by all the speakers about yoga and
                                        meditation. </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. Sameer Mirchandani</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        Had an enjoyable experience.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Ms. Shruthi Surendran</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        Need of the hour. Congratulations Rtambhara!!
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Mr. M.R. Shashidhar</div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>
                                        The program of Rtambhara has been a great break from the monotonous professional
                                        duties and inspired me to look beyond .This program had been extremely
                                        rejuvenating for me.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Honorary Consul General of Czech Republic Mr. Pushpak
                                            Prakash
                                        </div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                        <div class="item-carousel">
                            <!-- Carousel Item -->
                            <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->
                            <div class="testimonial">


                                    {{--                                        <h5 class="title">The Best Part of My Day!</h5>--}}
                                    <p>

                                        When I entered I walked in as the Honorary Consul General of Peru but when I
                                        walk out I will be the brand ambassador for Rtambhara. Rtambhara is one of the
                                        ecosystem for holistic healing. Through Rtambhara we can upgrade ourselves. If
                                        you know yourself this nothing more you need to know and if you want to heal the
                                        soul, Rtambhara is the first step. Rtambhara wellness will be something bigger
                                        and higher. It’s a great path towards humanity.
                                    </p>


                                <div class="author-box">

                                    <span class="author-icon"><i class="svg"></i></span>
                                    <div class="author-info">
                                        <div class="author">Honorary Consul General of Peru Mr. Vikram Vishwanath
                                        </div>
                                        {{--                                            <a href="#" class="author-position">Santa Monica, CA</a>--}}
                                    </div>

                                </div>

                            </div>
                            <!-- /Carousel Item -->
                        </div>
                        <!-- /Slide -->

                    </div>

                </div>

            </div>

        </div>



        <div class="call-out parallax-section size-2" data-bg="/images/kelly-unsplash.jpg"
             style="margin-top: 10px ;margin-bottom: 10px">

            <div class="container">

                <div class="row align-items-center">

                    <div class="col-lg-6 offset-lg-6">

                        <h2 class="call-title">Deepen and Enhance The Quality of Your Way of Life</h2>
                        <p class="text-size-small">Find your tailored program to uncover, nurture and transcend</p>
                        <a href="/retreats-and-wellness" class="btn btn-style-3 btn-big">Find Out More</a>

                    </div>

                </div>

            </div>

        </div>


        {{--          <!-- Page section -->--}}
        {{--      <div class="page-section">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="content-element2">--}}

        {{--            <div class="section-pre-title">Book a Class</div>--}}
        {{--            <h2 class="section-title">Today’s Class Schedule</h2>--}}

        {{--          </div>--}}

        {{--          <div class="healcode">--}}

        {{--            <div class="healcode-header">--}}

        {{--              <a href="#" class="action_icon print_version"><i class="licon-printer"></i></a>--}}
        {{--              <a href="#" class="action_icon cart_version"><i class="licon-cart"></i></a>--}}
        {{--              <a href="#" class="action_icon my_account_version"><i class="licon-user-lock"></i></a>--}}

        {{--              <h5 class="healcode-title-text">Schedule for November 14</h5>--}}

        {{--              <div class="filters">--}}

        {{--                <div class="filters-select">--}}
        {{--                  <div class="row">--}}
        {{--                    <div class="col-sm-6">--}}

        {{--                      <div class="mad-custom-select">--}}
        {{--                        <select data-default-text="All Classes">--}}
        {{--                          <option value="Option 1">Option 1</option>--}}
        {{--                          <option value="Option 2">Option 2</option>--}}
        {{--                          <option value="Option 3">Option 3</option>--}}
        {{--                        </select>--}}
        {{--                      </div>--}}

        {{--                    </div>--}}
        {{--                    <div class="col-sm-6">--}}

        {{--                      <div class="mad-custom-select">--}}
        {{--                        <select data-default-text="All Instructors">--}}
        {{--                          <option value="Option 1">Option 1</option>--}}
        {{--                          <option value="Option 2">Option 2</option>--}}
        {{--                          <option value="Option 3">Option 3</option>--}}
        {{--                        </select>--}}
        {{--                      </div>--}}

        {{--                    </div>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--                <div class="filter_days">--}}

        {{--                  <div class="input-wrapper">--}}
        {{--                    <input type="checkbox" id="checkbox" name="checkbox">--}}
        {{--                    <label for="checkbox">Sun</label>--}}

        {{--                    <input type="checkbox" id="checkbox2" name="checkbox">--}}
        {{--                    <label for="checkbox2">Mon</label>--}}

        {{--                    <input type="checkbox" id="checkbox3" name="checkbox">--}}
        {{--                    <label for="checkbox3">Tue</label>--}}

        {{--                    <input type="checkbox" id="checkbox4" name="checkbox">--}}
        {{--                    <label for="checkbox4">Wed</label>--}}

        {{--                    <input type="checkbox" id="checkbox5" name="checkbox">--}}
        {{--                    <label for="checkbox5">Thu</label>--}}

        {{--                    <input type="checkbox" id="checkbox6" name="checkbox">--}}
        {{--                    <label for="checkbox6">Fri</label>--}}

        {{--                    <input type="checkbox" id="checkbox7" name="checkbox">--}}
        {{--                    <label for="checkbox7">Sat</label>--}}
        {{--                  </div>--}}

        {{--                </div>--}}

        {{--                <div class="filter_time_of_day">--}}

        {{--                  <div class="input-wrapper">--}}
        {{--                    <input type="checkbox" id="checkbox8" name="checkbox">--}}
        {{--                    <label for="checkbox8">Morning</label>--}}

        {{--                    <input type="checkbox" id="checkbox9" name="checkbox">--}}
        {{--                    <label for="checkbox9">Afternoon</label>--}}

        {{--                    <input type="checkbox" id="checkbox10" name="checkbox">--}}
        {{--                    <label for="checkbox10">Evening</label>--}}
        {{--                  </div>--}}

        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <div class="date_links">--}}

        {{--                <div class="week_links">--}}

        {{--                  <span class="healcode-previous previous_week">--}}
        {{--                    <a href="#">Previous</a>--}}
        {{--                  </span>--}}
        {{--                  <span class="healcode-today today">--}}
        {{--                    <a href="#">Today</a>--}}
        {{--                  </span>--}}
        {{--                  <span class="healcode-next next_week">--}}
        {{--                    <a href="#">Next</a>--}}
        {{--                  </span>--}}

        {{--                  <span class="healcode-jump-to-date">--}}

        {{--                    <i class="licon-calendar-empty"></i>--}}
        {{--                    <input type="text" placeholder="2017-11-14">--}}

        {{--                  </span>--}}

        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <div class="clearfix"></div>--}}

        {{--            </div>--}}

        {{--            <div class="table-type-1 schedule responsive-table">--}}
        {{--              <table>--}}
        {{--                <tr>--}}
        {{--                  <th>Tue November 14, 2017</th>--}}
        {{--                  <th>Class</th>--}}
        {{--                  <th>Instructor</th>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>9:15 AM - 10:15 AM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Hatha</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Vicky Johnson</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>9:30 AM - 10:30 AM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Gentle Yoga</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Bradley Grosh</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>9:30 AM - 10:30 AM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Pilates</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Stella Healy</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>10:45 AM - 12:00 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Restorative Yoga</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Stella Healy</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>12:30 PM - 1:30 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Yin Yoga </a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Alan Smith</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>1:00 PM - 2:00 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Hatha</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Vicky Johnson</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>4:00 PM - 5:00 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Gentle Yoga</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Bradley Grosh</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>5:30 PM - 6:30 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Pilates</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Stella Healy</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>7:00 PM - 8:00 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Restorative Yoga</a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Stella Healy</a></td>--}}
        {{--                </tr>--}}
        {{--                <tr>--}}
        {{--                  <td data-title="Tue November 14, 2017">--}}
        {{--                    <div class="flex-row justify-content-between">--}}

        {{--                      <div>8:30 PM - 9:30 PM</div>--}}
        {{--                      <a href="#" class="btn btn-small">Sign Up</a>--}}

        {{--                    </div>--}}
        {{--                  </td>--}}
        {{--                  <td data-title="Class"><a href="#" class="link-text">Yin Yoga </a></td>--}}
        {{--                  <td data-title="Instructor"><a href="#" class="link-text">Alan Smith</a></td>--}}
        {{--                </tr>--}}
        {{--              </table>--}}
        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section parallax-section text-color-light" data-bg="/themes/wellness/images/1920x1000_bg1.jpg">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="content-element4">--}}

        {{--            <div class="section-pre-title">Testimonials</div>--}}
        {{--            <h2 class="section-title">What They’re Saying</h2>--}}

        {{--          </div>--}}

        {{--          <div class="carousel-type-2">--}}

        {{--            <div class="owl-carousel testimonial-holder" data-max-items="3"  data-autoplay="true">--}}

        {{--              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->--}}
        {{--              <div class="testimonial bg1">--}}

        {{--                --}}
        {{--                  <p>“Donec in velit vel ipsum auctor pulvinar. Vestibulum iaculis lacinia est. Proin dictum elementum velit. Fusce euismod consequat ante. Lorem ipsum dolor sit amet.” </p>--}}
        {{--                  <div class="author-box">--}}

        {{--                    <span class="author-icon"><i class="svg"></i></span>--}}
        {{--                    <div class="author-info">--}}
        {{--                      <div class="author">John McCoist</div>--}}
        {{--                      <a href="#" class="author-position">Santa Monica, CA</a>--}}
        {{--                    </div>--}}

        {{--                  </div>--}}
        {{--                --}}

        {{--              </div>--}}
        {{--              <!-- /Carousel Item -->--}}

        {{--              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->--}}
        {{--              <div class="testimonial bg2">--}}

        {{--                --}}
        {{--                  <p>“Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula. Aliquam dapibus tincidunt metus.” </p>--}}
        {{--                  <div class="author-box">--}}

        {{--                    <span class="author-icon"><i class="svg"></i></span>--}}
        {{--                    <div class="author-info">--}}
        {{--                      <div class="author">Camala Haddon</div>--}}
        {{--                      <a href="#" class="author-position">Los Angeles, CA</a>--}}
        {{--                    </div>--}}

        {{--                  </div>--}}
        {{--                --}}

        {{--              </div>--}}
        {{--              <!-- /Carousel Item -->--}}

        {{--              <!-- - - - - - - - - - - - - - Testimonial - - - - - - - - - - - - - - - - -->--}}
        {{--              <div class="testimonial bg3">--}}

        {{--                --}}
        {{--                  <p>“Vestibulum sed ante. Donec sagittis euismod purus. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.”</p>--}}
        {{--                  <div class="author-box">--}}

        {{--                    <span class="author-icon"><i class="svg"></i></span>--}}
        {{--                    <div class="author-info">--}}
        {{--                      <div class="author">Patrick Pool</div>--}}
        {{--                      <a href="#" class="author-position">San Dirgo, CA</a>--}}
        {{--                    </div>--}}

        {{--                  </div>--}}
        {{--                --}}

        {{--              </div>--}}
        {{--              <!-- /Carousel Item -->--}}

        {{--            </div>--}}

        {{--            <a href="#" class="btn btn-style-2">More Testimonials</a>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section-bg">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="content-element4">--}}

        {{--            <div class="section-pre-title">Teachers</div>--}}
        {{--            <h2 class="section-title">Meet Our Talented Team</h2>--}}

        {{--          </div>--}}

        {{--          <div class="carousel-type-2">--}}

        {{--            <div class="team-holder owl-carousel" data-item-margin="30" data-max-items="5">--}}

        {{--              <!-- team element -->--}}
        {{--              <div class="team-item">--}}

        {{--                <a href="#" class="member-photo">--}}
        {{--                  <img src="/themes/wellness/images/289x312_photo1.jpg" alt="">--}}
        {{--                </a>--}}
        {{--                <div class="team-desc">--}}
        {{--                  <div class="member-info">--}}
        {{--                    <h5 class="member-name"><a href="#">Vicky Johnson</a></h5>--}}
        {{--                    <h6 class="member-position">Founder, Hatha Yoga Teacher</h6>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <!-- team element -->--}}
        {{--              <div class="team-item">--}}

        {{--                <a href="#" class="member-photo">--}}
        {{--                  <img src="/themes/wellness/images/289x312_photo2.jpg" alt="">--}}
        {{--                </a>--}}
        {{--                <div class="team-desc">--}}
        {{--                  <div class="member-info">--}}
        {{--                    <h5 class="member-name"><a href="#">Bradley Grosh</a></h5>--}}
        {{--                    <h6 class="member-position">Ashtanga Yoga Teacher</h6>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <!-- team element -->--}}
        {{--              <div class="team-item">--}}

        {{--                <a href="#" class="member-photo">--}}
        {{--                  <img src="/themes/wellness/images/289x312_photo3.jpg" alt="">--}}
        {{--                </a>--}}
        {{--                <div class="team-desc">--}}
        {{--                  <div class="member-info">--}}
        {{--                    <h5 class="member-name"><a href="#">Stella Healy</a></h5>--}}
        {{--                    <h6 class="member-position">Yin & Vinyasa Yoga Teacher</h6>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <!-- team element -->--}}
        {{--              <div class="team-item">--}}

        {{--                <a href="#" class="member-photo">--}}
        {{--                  <img src="/themes/wellness/images/289x312_photo4.jpg" alt="">--}}
        {{--                </a>--}}
        {{--                <div class="team-desc">--}}
        {{--                  <div class="member-info">--}}
        {{--                    <h5 class="member-name"><a href="#">Olivia Franklin</a></h5>--}}
        {{--                    <h6 class="member-position">Hatha Yoga Teacher</h6>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--              <!-- team element -->--}}
        {{--              <div class="team-item">--}}

        {{--                <a href="#" class="member-photo">--}}
        {{--                  <img src="/themes/wellness/images/289x312_photo5.jpg" alt="">--}}
        {{--                </a>--}}
        {{--                <div class="team-desc">--}}
        {{--                  <div class="member-info">--}}
        {{--                    <h5 class="member-name"><a href="#">Alan Smith</a></h5>--}}
        {{--                    <h6 class="member-position">Vinyasa Yoga Teacher</h6>--}}
        {{--                  </div>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <div class="call-out bg-section" data-bg="/themes/wellness/images/1920x286_bg1.jpg">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="row align-items-center">--}}

        {{--            <div class="col-lg-7 col-md-12">--}}

        {{--              <div class="align-center">--}}
        {{--                <h2 class="call-title var2">Class Packs &amp; Memberships</h2>--}}
        {{--                <p>Purchase now to save on yoga classes</p>--}}
        {{--                <a href="#" class="btn btn-style-6 btn-big">Check Them Out</a>--}}
        {{--              </div>--}}

        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section-bg half-bg-col with-phone-img">--}}

        {{--        <div class="img-col-left"><div class="col-bg phone" data-bg="/themes/wellness/images/445x468_phone.jpg"></div></div>--}}

        {{--        <div class="container">--}}

        {{--          <div class="row align-content-center">--}}

        {{--            <div class="col-md-6"></div>--}}
        {{--            <div class="col-md-6">--}}

        {{--              <h2>Download Our App</h2>--}}
        {{--              <p class="content-element4">Find and book your favorite yoga classes from anywhere <br> with our yoga app.</p>--}}
        {{--              <div class="store-btns">--}}

        {{--                <a href="#"><img src="/themes/wellness/images/google_btn.png" alt=""></a>--}}
        {{--                <a href="#"><img src="/themes/wellness/images/app_btn.png" alt=""></a>--}}

        {{--              </div>--}}

        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section-bg half-bg-col text-color-light">--}}

        {{--        <div class="img-col-left"><div class="col-bg" data-bg="/themes/wellness/images/960x884_bg2.jpg"></div></div>--}}
        {{--        <div class="img-col-right"><div class="col-bg" data-bg="/themes/wellness/images/960x884_bg1.jpg"></div></div>--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="row align-content-center">--}}

        {{--            <div class="col-lg-5">--}}

        {{--              <div class="content-element4">--}}

        {{--                <div class="section-pre-title">Events & workshops</div>--}}
        {{--                <h2 class="section-title">Upcoming Events</h2>--}}

        {{--              </div>--}}

        {{--              <div class="carousel-type-2">--}}

        {{--                <div class="owl-carousel flex-row entry-box style-2" data-max-items="2" data-item-margin="30">--}}

        {{--                  <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->--}}
        {{--                  <div class="entry">--}}

        {{--                    <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->--}}
        {{--                    <div class="thumbnail-attachment">--}}
        {{--                      <a href="#"><img src="/themes/wellness/images/237x156_img1.jpg" alt=""></a>--}}
        {{--                    </div>--}}

        {{--                    <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->--}}
        {{--                    <div class="entry-body">--}}

        {{--                      <h6 class="entry-title"><a href="#">Saluting the Sun - A Hands - On Assist Workshop</a></h6>--}}
        {{--                      <div class="our-info vr-type">--}}

        {{--                        <div class="info-item">--}}
        {{--                          <i class="licon-clock3"></i>--}}
        {{--                          <div class="wrapper">--}}
        {{--                            <span>Nov 18, 2017 @ 12:00 AM - 5:00 PM</span>--}}
        {{--                          </div>--}}
        {{--                        </div>--}}
        {{--                        <div class="info-item">--}}
        {{--                          <i class="licon-map-marker"></i>--}}
        {{--                          <div class="wrapper">--}}
        {{--                            <span>8901 Marmora Road, Glasgow, D04 89GR</span>--}}
        {{--                          </div>--}}
        {{--                        </div>--}}

        {{--                      </div>--}}

        {{--                      <div class="entry-price">--}}

        {{--                        <span class="product-price">$35</span>--}}
        {{--                        <a href="#" class="btn btn-small">Register</a>--}}

        {{--                      </div>--}}

        {{--                    </div>--}}

        {{--                  </div>--}}

        {{--                  <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->--}}
        {{--                  <div class="entry">--}}

        {{--                    <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->--}}
        {{--                    <div class="thumbnail-attachment">--}}
        {{--                      <a href="#"><img src="/themes/wellness/images/237x156_img2.jpg" alt=""></a>--}}
        {{--                    </div>--}}

        {{--                    <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->--}}
        {{--                    <div class="entry-body">--}}

        {{--                      <h6 class="entry-title"><a href="#">The Art of Awakening</a></h6>--}}
        {{--                      <div class="our-info vr-type">--}}

        {{--                        <div class="info-item">--}}
        {{--                          <i class="licon-clock3"></i>--}}
        {{--                          <div class="wrapper">--}}
        {{--                            <span>Nov 18, 2017 @ 12:00 AM - 5:00 PM</span>--}}
        {{--                          </div>--}}
        {{--                        </div>--}}
        {{--                        <div class="info-item">--}}
        {{--                          <i class="licon-map-marker"></i>--}}
        {{--                          <div class="wrapper">--}}
        {{--                            <span>8901 Marmora Road, Glasgow, D04 89GR</span>--}}
        {{--                          </div>--}}
        {{--                        </div>--}}

        {{--                      </div>--}}

        {{--                      <div class="entry-price">--}}

        {{--                        <span class="product-price free">Free</span>--}}
        {{--                        <a href="#" class="btn btn-small">Register</a>--}}

        {{--                      </div>--}}

        {{--                    </div>--}}

        {{--                  </div>--}}

        {{--                </div>--}}

        {{--                <a href="#" class="btn btn-style-2">More Events</a>--}}

        {{--              </div>--}}

        {{--            </div>--}}
        {{--            <div class="col-lg-6 offset-lg-1"></div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section-bg">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="content-element4">--}}

        {{--            <div class="section-pre-title">Yoga Products</div>--}}
        {{--            <h2 class="section-title">In Our Shop</h2>--}}

        {{--          </div>--}}

        {{--          <div class="carousel-type-2">--}}

        {{--            <div class="owl-carousel products-holder" data-item-margin="30" data-max-items="5">--}}

        {{--              <!-- Product -->--}}
        {{--              <div class="product">--}}

        {{--                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->--}}
        {{--                <figure class="product-image">--}}
        {{--                  <a href="#"><img src="/themes/wellness/images/289x286_img1.jpg" alt=""></a>--}}
        {{--                </figure>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->--}}

        {{--                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->--}}
        {{--                <div class="product-description">--}}

        {{--                  <a href="#" class="product-cat">Equipment</a>--}}

        {{--                  <h6 class="product-name"><a href="#">Non Slip Yoga Mat</a></h6>--}}

        {{--                  <div class="pricing-area">--}}

        {{--                    <div class="product-price">--}}
        {{--                      $19.95--}}
        {{--                    </div>--}}

        {{--                    <ul class="rating">--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li class="empty"><i class="licon-star"></i></li>--}}
        {{--                    </ul>--}}

        {{--                  </div>--}}

        {{--                  <a href="#" class="btn btn-small btn-style-3">Add To Cart</a>--}}

        {{--                </div>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->--}}

        {{--              </div>--}}

        {{--              <!-- Product -->--}}
        {{--              <div class="product">--}}

        {{--                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->--}}
        {{--                <figure class="product-image">--}}
        {{--                  <a href="#"><img src="/themes/wellness/images/289x286_img2.jpg" alt=""></a>--}}
        {{--                </figure>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->--}}

        {{--                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->--}}
        {{--                <div class="product-description">--}}

        {{--                  <a href="#" class="product-cat">Clothes</a>--}}

        {{--                  <h6 class="product-name"><a href="#">Yoga T-Shirt Namaste Love </a></h6>--}}

        {{--                  <div class="pricing-area">--}}

        {{--                    <div class="product-price">--}}
        {{--                      <span>$48.95</span> $38.95--}}
        {{--                    </div>--}}

        {{--                  </div>--}}

        {{--                  <a href="#" class="btn btn-small btn-style-3">Add To Cart</a>--}}

        {{--                </div>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->--}}

        {{--              </div>--}}

        {{--              <!-- Product -->--}}
        {{--              <div class="product">--}}

        {{--                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->--}}
        {{--                <figure class="product-image">--}}
        {{--                  <a href="#"><img src="/themes/wellness/images/289x286_img3.jpg" alt=""></a>--}}
        {{--                </figure>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->--}}

        {{--                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->--}}
        {{--                <div class="product-description">--}}

        {{--                  <a href="#" class="product-cat">Equipment</a>--}}

        {{--                  <h6 class="product-name"><a href="#">Light Hard Foam Yoga Blocks</a></h6>--}}

        {{--                  <div class="pricing-area">--}}

        {{--                    <div class="product-price">--}}
        {{--                      $11.35--}}
        {{--                    </div>--}}

        {{--                    <ul class="rating">--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                    </ul>--}}

        {{--                  </div>--}}

        {{--                  <a href="#" class="btn btn-small btn-style-3">Add To Cart</a>--}}

        {{--                </div>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->--}}

        {{--              </div>--}}

        {{--              <!-- Product -->--}}
        {{--              <div class="product">--}}

        {{--                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->--}}
        {{--                <figure class="product-image">--}}
        {{--                  <a href="#"><img src="/themes/wellness/images/289x286_img4.jpg" alt=""></a>--}}
        {{--                </figure>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->--}}

        {{--                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->--}}
        {{--                <div class="product-description">--}}

        {{--                  <a href="#" class="product-cat">Equipment</a>--}}

        {{--                  <h6 class="product-name"><a href="#">Yoga Belt</a></h6>--}}

        {{--                  <div class="pricing-area">--}}

        {{--                    <div class="product-price">--}}
        {{--                      $13.95--}}
        {{--                    </div>--}}

        {{--                    <ul class="rating">--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                    </ul>--}}

        {{--                  </div>--}}

        {{--                  <a href="#" class="btn btn-small btn-style-3">Add To Cart</a>--}}

        {{--                </div>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->--}}

        {{--              </div>--}}

        {{--              <!-- Product -->--}}
        {{--              <div class="product">--}}

        {{--                <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->--}}
        {{--                <figure class="product-image">--}}
        {{--                  <a href="#"><img src="/themes/wellness/images/289x286_img5.jpg" alt=""></a>--}}
        {{--                </figure>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->--}}

        {{--                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->--}}
        {{--                <div class="product-description">--}}

        {{--                  <a href="#" class="product-cat">Beauty</a>--}}

        {{--                  <h6 class="product-name"><a href="#">Yoga Blend Body Lotion</a></h6>--}}

        {{--                  <div class="pricing-area">--}}

        {{--                    <div class="product-price">--}}
        {{--                      $14.99--}}
        {{--                    </div>--}}

        {{--                    <ul class="rating">--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                      <li><i class="licon-star"></i></li>--}}
        {{--                    </ul>--}}

        {{--                  </div>--}}

        {{--                  <a href="#" class="btn btn-small btn-style-3">Add To Cart</a>--}}

        {{--                </div>--}}
        {{--                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->--}}

        {{--              </div>--}}

        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section parallax-section text-color-light" data-bg="/themes/wellness/images/1920x1000_bg2.jpg">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div class="row">--}}
        {{--            <div class="col-lg-7 col-md-12">--}}

        {{--              <div class="content-element5">--}}

        {{--                <div class="section-pre-title">have a question?</div>--}}
        {{--                <h2 class="section-title">Contact Us</h2>--}}

        {{--              </div>--}}

        {{--              <p class="content-element3">Feel free to send us any questions you may have. We are happy to answer them.</p>--}}

        {{--              <form id="contact-form" class="contact-form">--}}

        {{--                <div class="contact-item">--}}

        {{--                  <label>Your Name (required)</label>--}}
        {{--                  <input type="text" name="cf-name" required="">--}}

        {{--                </div>--}}

        {{--                <div class="contact-item">--}}

        {{--                  <label>Your Email (required)</label>--}}
        {{--                  <input type="email" name="cf-email" required="">--}}

        {{--                </div>--}}

        {{--                <div class="contact-item">--}}

        {{--                  <label>Subject</label>--}}
        {{--                  <input type="url" name="cf-subject">--}}

        {{--                </div>--}}

        {{--                <div class="contact-item">--}}

        {{--                  <label>Message</label>--}}
        {{--                  <textarea rows="4" name="cf-message"></textarea>--}}

        {{--                </div>--}}

        {{--                <div class="contact-item">--}}

        {{--                  <button type="submit" class="btn btn-style-3" data-type="submit">Submit</button>--}}

        {{--                </div>--}}

        {{--              </form>--}}

        {{--            </div>--}}
        {{--            <div class="col-lg-4 col-md-12 offset-lg-1">--}}

        {{--              <div class="our-info style-2 vr-type var2">--}}

        {{--                <div class="info-item">--}}
        {{--                  <i class="licon-map-marker"></i>--}}
        {{--                  <div class="wrapper">--}}
        {{--                    <span class="info-cat">Postal Address</span>--}}
        {{--                  </div>--}}
        {{--                  <h5 class="info-title">8901 Marmora Road, <br> Glasgow, D04 89GR.</h5>--}}
        {{--                  <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249" class="btn btn-small btn-style-6">Get Direction</a>--}}
        {{--                </div>--}}

        {{--                <div class="info-item">--}}
        {{--                  <i class="licon-telephone"></i>--}}
        {{--                  <div class="wrapper">--}}
        {{--                    <span class="info-cat">Phone Number</span>--}}
        {{--                  </div>--}}
        {{--                  <h5 class="info-title" content="telephone=no">+1 800 559 6580</h5>--}}
        {{--                </div>--}}

        {{--                <div class="info-item">--}}
        {{--                  <i class="licon-at-sign"></i>--}}
        {{--                  <div class="wrapper">--}}
        {{--                    <span class="info-cat">Email Address</span>--}}
        {{--                  </div>--}}
        {{--                  <h5 class="info-title">info@companyname.com</h5>--}}
        {{--                </div>--}}

        {{--                <div class="info-item">--}}
        {{--                  <i class="licon-clock3"></i>--}}
        {{--                  <div class="wrapper">--}}
        {{--                    <span class="info-cat">Opening Hours</span>--}}
        {{--                  </div>--}}
        {{--                  <h5 class="info-title">Monday - Saturday: <br> 8am - 9pm</h5>--}}
        {{--                </div>--}}

        {{--              </div>--}}

        {{--            </div>--}}
        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

        {{--      <!-- Google map -->--}}
        {{--      <div class="map-section">--}}

        {{--        <div id="googleMap" class="map-container"></div>--}}

        {{--      </div>--}}

        {{--      <!-- Page section -->--}}
        {{--      <div class="page-section">--}}

        {{--        <div class="container wide2">--}}

        {{--          <div id="instafeed" class="instagram-feed style-2" data-instagram="5">--}}

        {{--            <div class="insta-title">--}}

        {{--              <i class="icon-instagram-5"></i>--}}
        {{--              <h5>Follow Us <br> On Instagram</h5>--}}

        {{--            </div>--}}

        {{--          </div>--}}

        {{--        </div>--}}

        {{--      </div>--}}

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->


@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

    {{--    <div id="popup" class="popup">--}}

    {{--      <div class="popup-inner">--}}

    {{--        <button type="button" class="close-popup"></button>--}}

    {{--        <div class="pricing-tables-holder flex-row item-col-2">--}}

    {{--          <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->--}}
    {{--          <div class="pricing-col">--}}

    {{--            <div class="pricing-table style-2">--}}

    {{--              <div class="label">Introductory Offer</div>--}}

    {{--              <header class="pt-header">--}}

    {{--                <div class="section-pre-title style-2">Your First</div>--}}

    {{--                <h3 class="pt-title">5 Classes</h3>--}}

    {{--                <div class="section-pre-title style-2">Just</div>--}}

    {{--                <div class="pt-price">$29</div>--}}

    {{--                <div class="pt-disc">Students - $23</div>--}}

    {{--              </header><!--/ .pt-header -->--}}

    {{--              <footer class="pt-footer">--}}

    {{--                <a href="#" class="btn btn-style-3">Buy Now</a>--}}

    {{--                <p>Must use within 2 weeks</p>--}}

    {{--              </footer><!--/ .pt-footer -->--}}

    {{--            </div>--}}

    {{--          </div>--}}
    {{--          <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->--}}

    {{--          <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->--}}
    {{--          <div class="pricing-col">--}}

    {{--            <div class="pricing-table">--}}

    {{--              <div class="label">Limited Time Offer</div>--}}

    {{--              <header class="pt-header">--}}

    {{--                <h4 class="pt-title">Unlimited <br> membership</h4>--}}

    {{--                <div class="pt-price">$109</div>--}}

    {{--                <div class="section-pre-title style-2">per month</div>--}}

    {{--                <div class="pt-disc">Students - $90/mo</div>--}}

    {{--              </header><!--/ .pt-header -->--}}

    {{--              <footer class="pt-footer">--}}

    {{--                <a href="#" class="btn btn-style-3">Buy Now</a>--}}

    {{--                <p>Minimum 4 month auto-renew commitment</p>--}}

    {{--              </footer><!--/ .pt-footer -->--}}

    {{--            </div>--}}

    {{--          </div>--}}
    {{--          <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->--}}

    {{--        </div>--}}

    {{--      </div>--}}

    {{--    </div>--}}

    {{--    <div id="popup-sign" class="popup var3">--}}

    {{--      <div class="popup-inner">--}}

    {{--        <button type="button" class="close-popup"></button>--}}

    {{--        <h4 class="title">Sign Up For Free</h4>--}}
    {{--        <p>Already have an account? <a href="#" class="link-text var2 popup-btn-login">Login Here</a></p>--}}
    {{--        <a href="#" class="btn fb-btn btn-big">Sign Up With Facebook</a>--}}
    {{--        <p class="content-element2">OR</p>--}}
    {{--        <form class="content-element2">--}}

    {{--          <input type="text" placeholder="Enter Your Email Address">--}}
    {{--          <input type="text" placeholder="Password">--}}
    {{--          <a href="#" class="btn btn-style-3 btn-big">Join Free Now!</a>--}}

    {{--        </form>--}}

    {{--        <p class="text-size-small">By signing up you agree to <a href="#" class="link-text">Terms of Service</a></p>--}}

    {{--      </div>--}}

    {{--    </div>--}}

    {{--    <div id="popup-login" class="popup var3">--}}

    {{--      <div class="popup-inner">--}}

    {{--        <button type="button" class="close-popup"></button>--}}

    {{--        <h4 class="title">Login</h4>--}}
    {{--        <p>Don't have an account yet?<a href="#" class="link-text var2 popup-btn-sign">JOIN FOR FREE</a></p>--}}
    {{--        <a href="#" class="btn fb-btn btn-big"> Login With Facebook</a>--}}
    {{--        <p class="content-element2">OR</p>--}}
    {{--        <form class="content-element1">--}}

    {{--          <input type="text" placeholder="Enter Your Email Address">--}}
    {{--          <input type="text" placeholder="Password">--}}
    {{--          <a href="#" class="btn btn-style-3 btn-big">Login</a>--}}

    {{--          <div class="input-wrapper">--}}
    {{--            <input type="checkbox" id="checkbox11" name="checkbox">--}}
    {{--            <label for="checkbox">Remember me</label>--}}
    {{--          </div>--}}

    {{--        </form>--}}

    {{--        <p class="text-size-small"><a href="#" class="link-text">Forgot your password?</a></p>--}}

    {{--      </div>--}}

    {{--    </div>--}}

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/instafeed.min.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/revolution/js/jquery.themepunch.tools.min.js?ver=5.0"></script>
<script src="/themes/wellness/plugins/revolution/js/jquery.themepunch.revolution.min.js?ver=5.0"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
