<!doctype html>
<html lang="en">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

<!-- Basic Page Needs
================================================== -->

<title>Rtambhara Wellness</title>

<!--meta info-->
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

<link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/themes/wellness/css/fontello.css">
<link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
<link rel="stylesheet" href="/themes/wellness/css/style.css">
<link rel="stylesheet" href="/themes/wellness/css/responsive.css">
<link rel="stylesheet" href="/themes/wellness/css/custom.css">

<style>
    .img-title{
        width: 40%;
        padding: 2%;
        margin-bottom: 3%;
    }
</style>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap banner-3" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Our Program Details</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Our Program Details</li>
            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

        <div class="container">

            <div class="content-element">

                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        {{--                        <h3 class="title">Praveśa</h3>--}}
                        <img class="img-title" src="/images/program-titles/black/pravesa.png" alt="Praveśa" />
                        <div class="accordion">
                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title active">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Pravesa is an introductory program with Rtambhara Wellness. This program is
                                        designed to invite you on a journey of discovery enabling you to seek well
                                        being, wisdom, and spirituality.
                                    </p>
                                    <p>
                                        A two night and three day getaway, Pravesa is the perfect way to rejuvenate.
                                        Take a break from the stress of everyday life and adopt simple changes for a
                                        happier lifestyle. </p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        You can expect a memorable 3 days of curated talks and activities that will
                                        delve into Wellness, Happiness and Understanding one’s ego, mind and your
                                        "inner” self. Learn Yoga, Laughter therapy and Meditation to positively impact
                                        your physical and mental well being.</p>
                                    <p>
                                        Be prepared to cook up some delicious
                                        snacks from organic foods, literally from "Farm to Plate”. Visit a cow farm to
                                        understand why the cow is revered, and how much benefit one can get from Nature.
                                        And, be mesmerized by cultural programs every evening that will unleash your
                                        artistic and creative side.</p>
                                    <p>
                                        End the program with interactive sessions with the
                                        faculty to understand how you can continue on this journey. Take home valuable
                                        inputs that you can use for your daily living. And, walk away with the
                                        determination that would lead to "Transformation" and "Happier Living"</p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">
                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12">

{{--                        <h3 class="title">Prabōdha</h3>--}}

                        <img class="img-title" src="/images/program-titles/black/prabodha.png" alt="Prabōdha" style="margin-top: 2.5%"/>

                        <div class="accordion">

                            <!--accordion item-->
                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>Prabodha is a weeklong wellness getaway for those actively reflecting on their
                                        present lifestyle and seeking to make positive changes. Be it seeking answers on
                                        the purpose of life through Ancient Indian Vedic philosophy and practice, or
                                        yoga, Pranayama, mudras, and meditation, you will attain deeper perspective on
                                        how to lead a happier life.</p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        You will learn to gauge your wellness quotient, and to unlock ways to live a
                                        fuller life. The participants will engage in cultural and creative pursuits,
                                        forge meaningful connections whilst discovering one’s latent abilities and
                                        unlocking potential.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">
                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage,
                                        Excursions, and Guided group tours
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

            <div class="content-element">

                <div class="row">
                    <div class="col-md-6 col-sm-12">

{{--                        <h3 class="title">Pravara</h3>--}}
                        <img class="img-title" src="/images/program-titles/black/pravara.png" alt="Pravara" style="margin-top: 4%" />

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Pravara is a wellness getaway for those who would like to delve deeper into
                                        Ancient Indian Vedic philosophy, and are looking for a more meaningful
                                        understanding of the scriptures and practices which can lead them to decipher
                                        happiness within themselves. Get a deeper understanding of the spiritual realms
                                        of Sanatana Dharma and interact with Program Director, Shri Krishnaphani
                                        Kesiraju to attain a deeper perspective.
                                    </p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        Practice yoga, Pranayama, mudras, and meditation daily and connect your body and
                                        mind with your higher self. Additionally, learn about everyday wellness, diet,
                                        nutrition, and enroll in an Ayurvedic detox program that will rejuvenate you and
                                        help reduce ailments naturally. The participants will engage in cultural and
                                        creative pursuits, forge meaningful connections, whilst discovering one’s latent
                                        abilities and unlocking potential. In these two weeks, you will take away a
                                        deeper understanding of existence, gain perspective on the interconnectedness of
                                        life, and find that your life can unfold with a sense of joy.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">

                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage,
                                        Excursions, and Guided group tours
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>

                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12">

{{--                        <h3 class="title">Pravīna</h3>--}}
                        <img class="img-title" src="/images/program-titles/black/pravina.png" alt="Pravīna" />

                        <div class="accordion">

                            <!--accordion item-->

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Praveena is a wellness getaway for those in sync with Ancient Indian Vedic
                                        philosophy, looking for a more meaningful understanding of the scriptures and
                                        practices leading to personal transformation. Deep dive into the wisdom
                                        contained in the upanishads, vedas, and Bhagavad Gita, and interact with Program
                                        Director, Shri Krishnaphani Kesiraju to understand the meaning and purpose of
                                        life and your reason for being. Start everyday with yoga, pranayams, mudras and
                                        meditation. Learn advanced techniques on how to enhance your prana and connect
                                        with your Higher Self through Meditation. In addition, learn about wellness,
                                        Sattvic diet and nutrition. Understand the need for Mantras for each of your
                                        daily actions and learn how to purify yourself and your environment through
                                        Homas.
                                    </p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        The participants will engage in cultural and creative pursuits, forge meaningful
                                        connections, whilst discovering one’s latent abilities and unlocking potential.
                                        In this 22 day program, you will feel empowered to assimilate and share the
                                        knowledge, while understanding the joy of living at a higher and subtler level
                                        ofconsciousness. This program will empower you to experience life through a
                                        truly balanced existence, that is in harmony with the cosmic order.
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">

                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage,
                                        Excursions, and Guided group tours
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>

                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
            <div class="content-element">

                <div class="row">
                    <div class="col-md-6 col-sm-12">


{{--                        <h3 class="title">Prājña</h3>--}}
                        <img class="img-title" src="/images/program-titles/black/prajna.png" alt="Prājña" />

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Prajna is a wellness getaway best suited for the deeply spiritual. The month
                                        long program enables you to gain knowledge and wisdom contained in the
                                        upanishads, vedas, and Bhagavad Gita, and to interact with Program Director,
                                        Shri Krishnaphani Kesiraju to understand the meaning and purpose of life and
                                        your reason for being. Practice yoga, pranayama, mudras and meditation daily for
                                        a closer connect with your higher Self. In addition, learn about wellness,
                                        Sattvic diet and nutrition. Make your physical body the instrument for your
                                        higher purpose through regular deoxification, and if required, treat your
                                        ailments with Ayurvedic therapies specially formulated for your needs by
                                        certified Ayurvedic doctors.
                                    </p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        The participants will engage in cultural and creative pursuits, forge meaningful
                                        connections, whilst discovering one’s latent abilities and unlocking potential.
                                        In this 31 day program, you will feel empowered to assimilate and share the
                                        knowledge, while understanding the joy of living at a higher and subtler level
                                        of consciousness. This program will empower you to experience life through a
                                        truly balanced existence, that is in harmony with the cosmic order. Most
                                        importantly, you will now be equipped to share this knowledge with others and
                                        guide those around you to an awareness of their life purpose and journey, and
                                        how best to use their lives for raising individual and global consciousness!
                                    </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">

                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage,
                                        Excursions, and Guided group tours
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> <a href="/contact-us#contact">Price on request</a>
                                    </p>

                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>
{{--                    <div class="col-md-6 col-sm-12">--}}

{{--                        <h3 class="title">Tailored Corporate Programs</h3>--}}

{{--                        <div class="accordion">--}}

{{--                            <!--accordion item-->--}}


{{--                            <!--accordion item-->--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">About the Program</h5>--}}
{{--                                <div class="a-content">--}}
{{--                                    <p>--}}
{{--                                        Our corporate programs can be tailored on the basis of individual/group needs of--}}
{{--                                        the organisation and will be in the realm of Transactional and Training needs--}}
{{--                                        for the short term and Transformational for the long term.The idea is to empower--}}
{{--                                        people to be happier at the workplace, strengthen them emotionally,--}}
{{--                                        intellectually and finally, increase productivity.These specially curated--}}
{{--                                        programs will be structured based on mutual discussions between the key--}}
{{--                                        stakeholders in the company and the Rtambhara team.--}}
{{--                                    </p>--}}
{{--                                    <p>Contact us at <span><a href="mailto:info@rtambharawellness.com">info@rtambharawellness.com</a></span>--}}
{{--                                        for more details and a tailored quote.</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <!--accordion item-->--}}
{{--                            --}}{{--                            <div class="accordion-item">--}}
{{--                            --}}{{--                                <h5 class="a-title">What to Expect</h5>--}}
{{--                            --}}{{--                                <div class="a-content">--}}
{{--                            --}}{{--                                    <p>TBC</p>--}}
{{--                            --}}{{--                                </div>--}}
{{--                            --}}{{--                            </div>--}}
{{--                            --}}{{--                            <div class="accordion-item">--}}
{{--                            --}}{{--                                <h5 class="a-title">Costs & Inclusions</h5>--}}
{{--                            --}}{{--                                <div class="a-content">--}}
{{--                            --}}{{--                                    <p>TBC--}}
{{--                            --}}{{--                                    </p>--}}
{{--                            --}}{{--                                </div>--}}
{{--                            --}}{{--                            </div>--}}
{{--                            --}}{{--                            <div class="accordion-item">--}}
{{--                            --}}{{--                                <h5 class="a-title">Upcoming Events</h5>--}}
{{--                            --}}{{--                                <div class="a-content btn-center">--}}
{{--                            --}}{{--                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming--}}
{{--                            --}}{{--                                        Events</a>--}}
{{--                            --}}{{--                                </div>--}}
{{--                            --}}{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
                    <div class="col-md-6 col-sm-12">


                        <h3 class="title" style="margin-top: 6.5%">Special Program Details</h3>

                        <div class="accordion">

                            <!--accordion item-->


                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">About the Program</h5>
                                <div class="a-content">
                                    <p>
                                        Our special programs are curated wellness retreats. These retreat are designed
                                        to inspire you on a journey of discovery enabling you to seek well being, wisdom
                                        and spirituality.
                                    </p>
                                    <p>
                                        You can expect a memorable 14 nights and 15 days of self discovery! If you are
                                        on the quest for spiritual knowledge, there is no better way to get introduced
                                        to the “Sanatana Dharma” that will be unfolded in the days following your
                                        arrival.
                                    </p>
                                </div>
                            </div>

                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">What to Expect</h5>
                                <div class="a-content">
                                    <p>
                                        A typical day with our special programs would entail Yoga, Pranayama and
                                        Meditation by the beach or outdoors.</p>
                                    <p> You can expect to learn and
                                        dialogue about Wisdom from Ancient scriptures with Krishnaphani Kesiraju. You
                                        will engage in talks on life with Bina Mirchandani
                                    </p>
                                    <p>
                                        Trips to Spiritual Centers, Historic energy centers, and other experiences in
                                        local culture.</p>
                                    <p> Cultural events and evenings free to explore the
                                        city and its nuances. Regular Detox Reviews will be part of your day.
                                        Introspection on your growth and learnings will help contextualize your time
                                        with Rtambhara.
                                    </p>
                                </div>
                            </div>
                            <!--accordion item-->
                            <div class="accordion-item">
                                <h5 class="a-title">Other Activities</h5>
                                <div class="a-content">

                                    <div class="col-md-12">
                                        <h6>CREATIVITY - CONNECT WITH YOUR INNER SELF</h6>
                                        <p>Music, Dance, Art, Pottery, and Gardening</p>
                                        <h6>ENERGY CENTRE FIELD VISITS</h6>
                                        <p>Churches and Temples, Historic sites, and Nature walks
                                            and trips</p>
                                        <h6>THE GOAN EXPERIENCE</h6>
                                        <p>Farming and village experience in Goa, Goan Lunch in a Local
                                            Home, Goa’s famous Flea Markets, Goan culture, music and
                                            dance, Famous beaches of Goa, Dolphin Watching, and Spice
                                            plantation tour with lunch.</p>
                                        <h6>DIET & NUTRITION FOR PRANIC LIVING</h6>
                                        <p>Farm to plate – Organic Living, Eat living foods for enhancing
                                            Prana, and Plant microgreens at home.</p>
                                        <h6>YOUR HEALTH</h6>
                                        <p>Detox</p>
                                        <p>Individual health checks and monitoring</p>
                                        <p>Ayurveda - Diagnosis and medication</p>
                                        <p>Ayurvedic massages will be chargeable</p>
                                    </div>


                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Costs & Inclusions</h5>
                                <div class="a-content">
                                    {{--                                    <ul style="list-style: none">--}}
                                    <p>All programs , workshops , sight-seeing tours and activities
                                        as mentioned including consultation with Ayurveda
                                        specialist.</p>
                                    <p>
                                        Accommodation in a 4/5 star property on twin sharing +
                                        breakfast and lunch / dinner . Herbal coolers / tea all day .


                                    </p>
                                    <p>Welcome Kit</p>
                                    <p>Airport transfers</p>
                                    <p>
                                        Services of a professional tour guide on some days. Tickets
                                        costs for entry at sight-seeing places .

                                    </p>
                                    <p>
                                        Internal transport costs for sight seeing as per our tour plan .
                                    </p>
                                    {{--                                    </ul>--}}

                                </div>
                            </div>
                            <div class="accordion-item">
                                <h5 class="a-title">Upcoming Events</h5>
                                <div class="a-content btn-center">
                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming
                                        Events</a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
{{--            <div class="content-element">--}}

{{--                <div class="row">--}}
{{--                    <div class="col-md-6 col-sm-12">--}}


{{--                        <h3 class="title">Special Program Details</h3>--}}

{{--                        <div class="accordion">--}}

{{--                            <!--accordion item-->--}}


{{--                            <!--accordion item-->--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">About the Program</h5>--}}
{{--                                <div class="a-content">--}}
{{--                                    <p>--}}
{{--                                        Our special programs are curated wellness retreats. These retreat are designed--}}
{{--                                        to inspire you on a journey of discovery enabling you to seek well being, wisdom--}}
{{--                                        and spirituality.--}}
{{--                                    </p>--}}
{{--                                    <p>--}}
{{--                                        You can expect a memorable 14 nights and 15 days of self discovery! If you are--}}
{{--                                        on the quest for spiritual knowledge, there is no better way to get introduced--}}
{{--                                        to the “Sanatana Dharma” that will be unfolded in the days following your--}}
{{--                                        arrival.--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <!--accordion item-->--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">What to Expect</h5>--}}
{{--                                <div class="a-content">--}}
{{--                                    <p>--}}
{{--                                        A typical day with our special programs would entail Yoga, Pranayama and--}}
{{--                                        Meditation by the beach or outdoors.</p>--}}
{{--                                    <p> You can expect to learn and--}}
{{--                                        dialogue about Wisdom from Ancient scriptures with Krishnaphani Kesiraju. You--}}
{{--                                        will engage in talks on life with Bina Mirchandani--}}
{{--                                    </p>--}}
{{--                                    <p>--}}
{{--                                        Trips to Spiritual Centers, Historic energy centers, and other experiences in--}}
{{--                                        local culture.</p>--}}
{{--                                    <p> Cultural events and evenings free to explore the--}}
{{--                                        city and its nuances. Regular Detox Reviews will be part of your day.--}}
{{--                                        Introspection on your growth and learnings will help contextualize your time--}}
{{--                                        with Rtambhara.--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!--accordion item-->--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">Other Activities</h5>--}}
{{--                                <div class="a-content">--}}

{{--                                    <div class="col-md-12">--}}
{{--                                        <h6>CREATIVITY - CONNECT WITH YOUR INNER SELF</h6>--}}
{{--                                        <p>Music, Dance, Art, Pottery, and Gardening</p>--}}
{{--                                        <h6>ENERGY CENTRE FIELD VISITS</h6>--}}
{{--                                        <p>Churches and Temples, Historic sites, and Nature walks--}}
{{--                                            and trips</p>--}}
{{--                                        <h6>THE GOAN EXPERIENCE</h6>--}}
{{--                                        <p>Farming and village experience in Goa, Goan Lunch in a Local--}}
{{--                                            Home, Goa’s famous Flea Markets, Goan culture, music and--}}
{{--                                            dance, Famous beaches of Goa, Dolphin Watching, and Spice--}}
{{--                                            plantation tour with lunch.</p>--}}
{{--                                        <h6>DIET & NUTRITION FOR PRANIC LIVING</h6>--}}
{{--                                        <p>Farm to plate – Organic Living, Eat living foods for enhancing--}}
{{--                                            Prana, and Plant microgreens at home.</p>--}}
{{--                                        <h6>YOUR HEALTH</h6>--}}
{{--                                        <p>Detox</p>--}}
{{--                                        <p>Individual health checks and monitoring</p>--}}
{{--                                        <p>Ayurveda - Diagnosis and medication</p>--}}
{{--                                        <p>Ayurvedic massages will be chargeable</p>--}}
{{--                                    </div>--}}


{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">Costs & Inclusions</h5>--}}
{{--                                <div class="a-content">--}}
{{--                                    --}}{{--                                    <ul style="list-style: none">--}}
{{--                                    <p>All programs , workshops , sight-seeing tours and activities--}}
{{--                                        as mentioned including consultation with Ayurveda--}}
{{--                                        specialist.</p>--}}
{{--                                    <p>--}}
{{--                                        Accommodation in a 4/5 star property on twin sharing +--}}
{{--                                        breakfast and lunch / dinner . Herbal coolers / tea all day .--}}


{{--                                    </p>--}}
{{--                                    <p>Welcome Kit</p>--}}
{{--                                    <p>Airport transfers</p>--}}
{{--                                    <p>--}}
{{--                                        Services of a professional tour guide on some days. Tickets--}}
{{--                                        costs for entry at sight-seeing places .--}}

{{--                                    </p>--}}
{{--                                    <p>--}}
{{--                                        Internal transport costs for sight seeing as per our tour plan .--}}
{{--                                    </p>--}}
{{--                                    --}}{{--                                    </ul>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="accordion-item">--}}
{{--                                <h5 class="a-title">Upcoming Events</h5>--}}
{{--                                <div class="a-content btn-center">--}}
{{--                                    <a href="/upcoming-events" class="btn btn-big btn-style-3">View Our Upcoming--}}
{{--                                        Events</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->
</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
