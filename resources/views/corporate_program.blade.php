<!doctype html>
<html lang="en">

<!-- Google Web Fonts
================================================== -->

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

<!-- Basic Page Needs
================================================== -->

<title>Rtambhara Wellness</title>

<!--meta info-->
<meta charset="utf-8">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>


<link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
<!-- Vendor CSS
============================================ -->

<link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/themes/wellness/css/fontello.css">
<link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
<link rel="stylesheet" href="/themes/wellness/css/style.css">
<link rel="stylesheet" href="/themes/wellness/css/responsive.css">
<link rel="stylesheet" href="/themes/wellness/css/custom.css">


<style>
    .img-title {
        width: 40%;
    }
    #list >li{
        margin-right: 6%;
    }
    #list >li >p{
        margin-top: 2%;
        margin-bottom: 2%;
    }
</style>

</head>

<body class="corporate_program">

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap  banner-3" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Our Corporate Programs</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li>Our Corporate Programs</li>

            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
<div id="content" class="page-content-wrap">
    <!-- <div class="container img1">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img class="workFromHeart" src="/images/workFromHeart.png" alt="workFromHeart"/>
                <div class="top-left"><h3>Work From the heart</h3><span>A transformational work philosophy</span></div>
                <div class="containerImg1 bottom-left">
                    <img src="/images/logo_light-min_optimised.png" alt="logo"/>
                </div>
            </div>
        </div>
    </div> -->

    <div class="container img1 imgNew page-section-bg parallax-section" data-bg="/images/workFromHeart-min.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="top-left">
                <h3>Work From the heart</h3>
                <span>A transformational work philosophy</span>
            </div>
            <div class="containerImg1 bottom-left">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
    <div class="container img2">
        <div class="row">
            <div class="slide2 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <span class="logoDisplay"></span>

            <div class="containerImg2 centered">
                <p class="para1">
                    We believe that human resources are critical for any organization to succeed and must be nurtured carefully in order to achieve desired results. In the Digital Era, dependence on Human capital is more than ever, since companies are having to deal with heightened complexity, uncertainty and disruptive market forces.

                    To address the demanding challenges at the workplace, we believe that fresh approaches are necessary to bring about more impactful and lasting changes.
                </p>
            </div>
            <div class="containerImg2 bottom-left">
                    <img src="/images/logo_dark-min_optimised.png" alt="logo"/>
                </div>
            </div>
        </div>
    </div>
    <div class="container img3">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <img class="quantumShift" src="/images/quantumshift-min_optimised.png" alt="quantumShift"/>
                <!-- <div class="top-left"><h3>Work From the heart</h3><span>A transformational work philosophy</span></div> -->
                <div class="containerImg3 bottom-left">
                    <img src="/images/logo_dark-min_optimised.png" alt="logo"/>
                </div>
            </div>
            <div class="borderContainer col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <div class="container img3Text">
                    <h3>
                    What can you expect from Individuals who participate
                    in Rtambhara programs?
                    </h3>
                    <ul>
                        <li><span class="listStyle"></span>QUANTUM SHIFT IN PERCEPTION</li>
                        <li><span class="listStyle"></span>HIGHER LEVEL OF ENGAGEMENT</li>
                        <li><span class="listStyle"></span>PASSION AND COMPASSION</li>
                        <li><span class="listStyle"></span>WORKING WITH ‘ONENESS’</li>
                        <li><span class="listStyle"></span>INCREASED HAPPINESS QUOTIENT</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container img4">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img class="relevantPrograms" src="/images/relevantPrograms.png" alt="relevant programs"/>
                <div class="containerTxt bottom-left"><h6>WHAT WORKS BEST</h6><h5>CREATE RELEVANT PROGRAMS FROM OUR ROBUST OFFERINGS</h5></div>
                <div class="containerImg4 bottom-right">
                    <img src="/images/logo_dark-min_optimised.png" alt="logo"/>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container img4 imgNew page-section-bg parallax-section" data-bg="/images/relevantPrograms-min.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="containerTxt bottom-left">
                <h6>WHAT WORKS BEST</h6>
                <h5>CREATE RELEVANT PROGRAMS FROM OUR ROBUST OFFERINGS</h5>
            </div>
            <div class="containerImg4 bottom-right">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
    <div class="container img5"></div>
<!--     <div class="container img6">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img class="dimensionsWellness" src="/images/dimensionsWellness.png" alt="Dimension Wellness"/>
                <div class="containerTxt top-left">
                    <h3>INTRODUCTORY</h3>
                </div>
                <div class="containerTxt centered">
                    <h5>9 DIMENSIONS OF WELLNESS</h5>
                    <h6>At Rtambhara, apart for the 7 dimensions of wellness (physical, emotional, intellectual, social, occupational, financial and environmental), we work with the spiritual and the transcendental dimensions. It is important to understand these dimensions and find your personal balance.</h6>
                </div>
                <div class="containerImg6 bottom-left">
                    <img src="/images/logo_light-min_optimised.png" alt="logo"/>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container img6 imgNew page-section-bg parallax-section" data-bg="/images/dimensionsWellness-min.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="containerTxt top-left">
                <h3>INTRODUCTORY</h3>
            </div>
            <div class="containerTxt centered">
                <h5>9 DIMENSIONS OF WELLNESS</h5>
                <h6>At Rtambhara, apart for the 7 dimensions of wellness (physical, emotional, intellectual, social, occupational, financial and environmental), we work with the spiritual and the transcendental dimensions. It is important to understand these dimensions and find your personal balance.</h6>
            </div>
            <div class="containerImg6 bottom-left">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
<!--     <div class="container img8">
        <div class="row">
            <div class="page-section-bg parallax-section" data-bg="/images/spiritualWellbeing1.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
                <div class="container">
                    <div class="content-element5">
                        <h3 class="align-center">What Our Clients Say</h3>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container img7 imgNew page-section-bg parallax-section" data-bg="/images/workplaceWellbeing-min.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="containerTxt top-left">
                <h3>WORKPLACE WELLBEING</h3>
            </div>
            <div class="row textContainer vdivide">
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>YOUR HAPPINESS QUOTIENT (SESSION1,2,3 &4)</h5>
                    <h6>Understand what happiness means and how you can raise your happiness quotient for more productivity and more meaningful relationships.</h6>
                </div>
                <div class="align-center col-md-3 col-lg-3 col-xs-12 col-sm-12">
                    <h5>ENLIGHTENED LEADERSHIP</h5>
                    <h6>In this information age with a greater dependence on individual competence, learn to understand your team and lead them from the heart, with empathy and compassion, ensuring they work to their potential. Learn to lead by example.</h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>YOU GOT IT! YOU LOST IT!</h5>
                    <h6>Learn to accept and manage change in every aspect of your professional and personal life, and use it to your advantage. Learn to view change as an opportunity. Remember "all change happens for a reason!”</h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>POSITIVE COMMUNICATION</h5>
                    <h6>Communication is critical to relationship building. Learn how you can communicate to build and solidify relationships.</h6>
                </div>
                <div class="align-center col-md-3 col-lg-3 col-xs-12 col-sm-12">
                    <h5>UNLOCK YOUR CREATIVITY</h5>
                    <h6>Creativity comes from within. Any external stimuli can inspire you, but the creativity lies within. Also understand how creativity can bring you happiness and fulfillment.</h6>
                </div>
            </div>
            <div class="containerImg7 bottom-left">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
    <div class="container img8 imgNew page-section-bg parallax-section" data-bg="/images/spiritualWellbeing1-min.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="containerTxt top-left">
                <h3>SPIRITUAL WELLBEING</h3>
            </div>
            <div class="align-center containerTxt centered">
                <h5>Why Spirituality?</h5>
                <h6>Spirituality at the workplace is understanding the true meaning and purpose of life, resulting in:
                    <ol>
                        <li>1.Health and wellbeing at the individual level</li>
                        <li>2.Harmony in relationships at the community level</li>
                        <li>3.Happiness through connecting at the universal level</li>
                    </ol>
                </h6>
            </div>
            <div class="containerImg8 bottom-left">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
    <div class="container img9 imgNew page-section-bg parallax-section" data-bg="/images/spiritualWellbeing2.png"  style="margin-top: 10px ;margin-bottom: 10px ;">
        <div class="row">
            <div class="containerTxt top-left">
                <h3>SPIRITUAL WELLBEING</h3>
            </div>
            <div class="row textContainer col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="align-center col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>PASSION AND COMPASSION</h5>
                    <h6>Passion is the energy flow at the individual level with predominance of mind as its source, and compassion is the energy flow at the cosmic level with predominance of heart as its source.</h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>CONNECT TO YOUR CORE</h5>
                    <h6>Identifying and connecting to the centre of one’s own being which is the source of manifestation linking matter, life and consciousness.</h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>MEDITATION ON THE MOVE</h5>
                    <h6>Understanding that it is not a short duration exercise but a dynamic state of mind that works as the backdrop of all activities.
                    </h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>FULLNESS OF BEING</h5>
                    <h6>You are a complete being…Any sense of inadequacy, which perpetuates comes from a lack of understanding this “Fullness”</h6>
                </div>
                <div class="align-center col-md-2 col-lg-2 col-xs-12 col-sm-12">
                    <h5>FROM SELF TO SELF</h5>
                    <h6>Understanding the meaning and purpose of life, as a journey we undertake in this manifested world to finally realize our “True Self.”</h6>
                </div>
                <div class="align-center col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
            </div>
            <div class="containerImg9 bottom-left">
                <img src="/images/logo_light-min_optimised.png" alt="logo"/>
            </div>
        </div>
    </div>
    <div class="container img10">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <img class="feedback" src="/images/feedback-min_optimised.png" alt="feedback"/>
                <div class="containerImg10 bottom-left">
                    <img src="/images/logo_dark-min_optimised.png" alt="logo"/>
                </div>
            </div>
            <div class="borderContainer col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <div class="container img10Text">
                    <h5 class="align-center">FEEDBACK AFTER THE PROGRAM</h5>
                    <h5>Assesing if the areas of concern suggessted by the participants at the onset have been addressed<br/>
                    Any future interaction required online.If so, specific area of concern and further learning.
                    </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="challenges flex-row item-col-2">
            <div class="item-col">
                <div class="challenge-item">
                    <div class="bg-img" data-bg="/images/kelly-unsplash_optimised.jpg"></div>
                    <a href="#" class="link"></a>
                    <div class="inner">
                        <h3 class="item-title"><a href="/corporate-program-details">SPOORTHI<br> BE INSPIRED</a>
                        </h3>
                        <div class="our-info style-3">
                        <div class="info-item">
                            <i class="licon-play-circle"></i>
                            <div class="wrapper">
                                <a href="/corporate-program-details">1 day</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-col">
                <div class="challenge-item">
                    <div class="bg-img" data-bg="/images/kelly-unsplash_optimised.jpg"></div>
                    <a href="#" class="link"></a>
                    <div class="inner">
                        <h3 class="item-title"><a href="/corporate-program-details">SPANDANA<br> BE RESONANT</a>
                        </h3>
                        <div class="our-info style-3">
                        <div class="info-item">
                            <i class="licon-play-circle"></i>
                            <div class="wrapper">
                                <a href="/corporate-program-details">2 days</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-col">
                <div class="challenge-item">
                    <div class="bg-img" data-bg="/images/kelly-unsplash_optimised.jpg"></div>
                    <a href="#" class="link"></a>
                    <div class="inner">
                        <h3 class="item-title"><a href="/corporate-program-details">SAMVEDANA<br> BE EMPATHETIC</a>
                        </h3>
                        <div class="our-info style-3">
                        <div class="info-item">
                            <i class="licon-play-circle"></i>
                            <div class="wrapper">
                                <a href="/corporate-program-details">3 days</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

@include('layouts.footer')

<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
</html>
