<!doctype html>
<html lang="en">
<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->

    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <style>
        .tribe-events-tooltip .btn {
            padding: 8px 30px 6px;
            display: inline-block;
            font-size: 10px;
            text-transform: uppercase;
            border-radius: 30px;
            background: #45b29d;
            color: #fff;
            font-weight: normal;
            text-align: center;
            line-height: 12px;
            letter-spacing: 0.8px;
            margin-left: 5px;
        }
    </style>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->
@include('layouts.header')

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap" style="margin-top: 10%">

        <div class="container">

            <h1 class="page-title">Upcoming Events</h1>

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li><a>Events</a></li>
            </ul>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

        <div class="container">

            {{--            <div class="content-element4">--}}

            {{--                <div class="tribe-events-bar">--}}

            {{--                    <div class="row">--}}
            {{--                        <div class="col-lg-10">--}}

            {{--                            <form>--}}
            {{--                                <div class="row">--}}
            {{--                                    <div class="col-sm-4 col-no-space">--}}
            {{--                                        <label>Events From</label>--}}
            {{--                                        <input type="text" placeholder="2018-12">--}}
            {{--                                    </div>--}}
            {{--                                    <div class="col-sm-8 col-no-space">--}}
            {{--                                        <label>Search</label>--}}
            {{--                                        <button type="submit" class="btn btn-style-3 btn-big f-right">Find Event--}}
            {{--                                        </button>--}}
            {{--                                        <div class="wrapper">--}}
            {{--                                            <input type="text" placeholder="Search">--}}
            {{--                                        </div>--}}
            {{--                                    </div>--}}
            {{--                                </div>--}}
            {{--                            </form>--}}

            {{--                        </div>--}}
            {{--                        <div class="col-lg-2">--}}

            {{--                            <label>View as</label>--}}
            {{--                            <div class="mad-custom-select">--}}
            {{--                                <select data-default-text="List">--}}
            {{--                                    <option value="Month">Month</option>--}}
            {{--                                    <option value="Week">Week</option>--}}
            {{--                                    <option value="Map">Map</option>--}}
            {{--                                    <option value="Photo">Photo</option>--}}
            {{--                                </select>--}}
            {{--                            </div>--}}

            {{--                        </div>--}}
            {{--                    </div>--}}

            {{--                </div>--}}

            {{--            </div>--}}

            <h5 class="title">Events For {{$current_month}} {{$current_year}}</h5>

            <div class="responsive-table content-element4">
                <table class="tribe-events-calendar var2">
                    <thead>
                    <tr>

                        @foreach($dayNames as $dayName)
                            <th>{{$dayName}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dayEvents as $week )
                        <tr>
                            @foreach($week as $item)
                                <td data-title="{{$item['day']}}" id="cell-{{$item['day']}}">
                                    <div class="tribe-events-daynum" id="date-{{$item['day']}}">
                                        <a href="#" id="date-a-{{$item['day']}}">{{$item['day']}}</a>
                                    </div>
                                    @if($item['program'])
                                        <div class="tribe-events-category-tech-events has-program" data-duration="{{$item['program']->duration}}" data-id="{{$item['day']}}" >
                                            <a href='/event-single/{{$item['program']->id}}' id="paragraph-{{$item['day']}}">{{$item['program']->title}}</a>
                                            <span>
                                                {{$item['program']->venue}}
                                            </span>
{{--                                            <span id="span-{{$item['day']}}">--}}
{{--                                                {{$item['program']->venue}}--}}
{{--                                            </span>--}}
                                            <div class="tribe-events-tooltip">
                                                <header>
                                                    <h6>{{$item['program']->title}}</h6>
                                                    <time datetime="{{date('Y-m-d' ,strtotime($item['program']->program_date))}}">{{$item['program']->program_date}}
                                                        for {{$item['program']->duration}} days
                                                    </time>
                                                    {{--                                                    <time datetime="2019-07-27">Saturday 27th July, 2019 for 3 Days</time>--}}
                                                    <a href='/event-single/{{$item['program']->id}}' class="btn btn-style-2 ">Read more</a>
                                                </header>
                                                <div class="clearfix">
                                                    <img src="/images/jay-unsplash.jpg" alt="" class="alignleft">
                                                    <p>
                                                        {{$item['program']->description}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="tribe-events-category-tech-events has-program" >
                                            <a data-id="{{$item['day']}}" id="blank-{{$item['day']}}" style="visibility: hidden; pointer-events: none">
                                            </a>
                                            <span id="span-{{$item['day']}}">

                                            </span>
                                        </div>

                                    @endif

                                </td>

                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>

            <div class="page-nav">
                <div class="flex-item"><a href="/upcoming-events?{{$previous_month_url}}"
                                          class="info-btn prev-btn">{{$previous_month}}</a></div>
                {{--                <div class="flex-item"><a href="#" class="btn btn-style-2">+ Export Events</a></div>--}}
                <div class="flex-item"><a href="/upcoming-events?{{$next_month_url}}"
                                          class="info-btn next-btn">{{$next_month}}</a></div>
            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
    @include('layouts.footer')

</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>

</body>
<script>

    $( ".has-program" ).each(function( index ) {

        var id = $( this ).data('id');

        // console.log(  "id: " + $( this ).data('id') );
        // console.log('Para : ' + $('#paragraph-'+id).text());

        // console.log(  "duratio : " + $( this ).data('duration') );
        // $('#cell-'+$( this ).data('id')).css('background','red');
        var duration = $( this ).data('duration');

        for(var j=0; j<duration;j++){
            // console.log('painting : ' + (id+j));
            // $('#cell-'+(id+j)).css('background','red');
            $('#blank-'+(id + j)).text($('#paragraph-'+id).text());
            $('#span-'+(id + j)).text('Day ' + (j+1));

            $('#blank-'+(id + j)).css('visibility','visible');


            $('#date-'+(id + j))
                .css({'background':'#754B75'});
            $('#date-a-'+(id + j))
                .css({'color':'#FFFFFF'});

        }

    });

</script>
</html>
