<!doctype html>
<html lang="en" xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CPrata" rel="stylesheet">

    <!-- Basic Page Needs
    ================================================== -->


    <title>Rtambhara Wellness</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="program_id" content="{{$program->id}}">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" type="image/ico" href="/themes/wellness/images/favicon.png"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">


    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="/themes/wellness/font/demo-files/demo.css">

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="/themes/wellness/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/themes/wellness/css/fontello.css">
    <link rel="stylesheet" href="/themes/wellness/css/owl.carousel.css">
    <link rel="stylesheet" href="/themes/wellness/css/style.css">
    <link rel="stylesheet" href="/themes/wellness/css/responsive.css">
    <link rel="stylesheet" href="/themes/wellness/css/custom.css">

    <script src="https://unpkg.com/vue/dist/vue.min.js"></script>
    {{--    <script src="https://unpkg.com/vue-stripe-checkout/build/vue-stripe-checkout.js"></script>--}}


    <style>
        h4.title {
            padding-top: 2%;
            padding-bottom: 1%;
            margin-bottom: 1% !important;
        }

        .popup .popup-inner {
            position: relative;
            margin-top: 1%;
            width: 930px;
            display: inline-block;
            padding: 26px;
            background: #fff;
        }

        .single-post .custom-list li .product-price a {
            color: goldenrod;
        }

        [class*="alert"]:before {
            font-family: 'linearicons';
            font-size: 30px;
            color: #fff;
            position: absolute;
            top: 25px;
            left: 30px;
            display: none;
        }

        #billing .contact-item {
            margin-bottom: 1% !important;
        }

        #billing input:not([type="submit"]), textarea {
            height: 35px;
        }
    </style>

    <script src="/js/cal.js"></script>
    <script>
        function dismiss(el) {
            el.parentNode.style.display = 'none';
        };
    </script>

</head>

<body>

<div class="loader"></div>

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f-right"><a href="#" class="button button-type-3 button-orange">Accept Cookies</a><a href="#" class="button button-type-3 button-grey-light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

@include('layouts.header')


<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap no-title" style="margin-top: 10%">

        <div class="container">

            <ul class="breadcrumbs">

                <li><a href="/">Home</a></li>
                <li href="/upcoming-events">Events</li>
                <li>{{$program->title}}</li>

            </ul>

        </div>


    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="app" class="page-content-wrap">

        <div class="container">

            <div class="content-element">

                <div>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Great! </strong>{!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                    onclick="dismiss(this);">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <?php Session::forget('success');?>

                    @endif


                    @if ($message = Session::get('error'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Oops! </strong>{!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                    onclick="dismiss(this);">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <?php Session::forget('error');?>

                    @endif


                </div>


                <div class="content-element4">
                    <div class="page-nav">
                        <div class="flex-item"><a href="/upcoming-events" class="info-btn prev-btn">All Events</a></div>
                        <div class="flex-item">

                            <div class="btn-wrap">
                                <button class="btn btn-big  btn-style-2"
                                        onclick="ical_download('{{$program->title}}', 'rthambara.ics', '{{$start_date}} {{$timezone}}', '{{$end_date}} {{$timezone}}')">
                                    +
                                    iCal Export
                                </button>


                                @if(!$expired)
                                    <span class="product-price"> <button
                                            class="btn popup-btn btn-big btn-style-3" id="price-request">
                                        <i class="icon-money-2 mr-2"></i>                                        Available on Request</button></span>
                                @endif
                                {{--                                <a href="#" class="btn btn-style-2">+ iCal Export</a>--}}
                            </div>

                        </div>
                    </div>
                </div>

                <div class="content-element5">
                    <div class="entry-box single-post">

                        <div class="entry">

                            <div class="content-element4">
                                <div class="entry-body">

                                    <h1 class="entry-title">{{$program->title}}</h1>
                                    <div class="our-info vr-type">

                                        <div class="info-item">
                                            <i class="licon-clock3"></i>
                                            <div class="wrapper">
                                                <span><a
                                                        onclick="ical_download('{{$program->title}}', 'rthambara.ics', '{{$start_date}} {{$timezone}}', '{{$end_date}} {{$timezone}}')">
                                                        {{ date("jS F Y", strtotime($program->program_date))}}
                                                        @if($program->duration)
                                                            for {{$program->duration}} Days
                                                        @endif
                                                    </a></span>
                                                (<a href="/upcoming-events">See all</a>)
                                                {{--                                                <span class="product-price fas fa-shopping-cart"> <button @click.prevent="checkout"--}}
                                                {{--                                                                                     class="btn popup-btn btn-big btn-style-3">100</button></span>--}}


                                                {{--                                                <span class="product-price"> <button--}}
                                                {{--                                                        class="btn popup-btn btn-big btn-style-3"--}}
                                                {{--                                                        @click.prevent="showModal"><i--}}
                                                {{--                                                            class="icon-cart mr-2">100 INR</i></button></span>--}}

                                                <div>
                                                    {{--                                                    <vue-stripe-checkout--}}
                                                    {{--                                                        ref="checkoutRef"--}}
                                                    {{--                                                        :image="image"--}}
                                                    {{--                                                        :name="name"--}}
                                                    {{--                                                        :description="description"--}}
                                                    {{--                                                        :currency="currency"--}}
                                                    {{--                                                        :amount="finalAmount"--}}
                                                    {{--                                                        @done="done"--}}
                                                    {{--                                                        @opened="opened"--}}
                                                    {{--                                                        @closed="closed"--}}
                                                    {{--                                                    ></vue-stripe-checkout>--}}

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <input type="hidden">
                            <div class="content-element2">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12"><img src="/images/jay-unsplash-banner.jpg"
                                                                          style="width: 100%" alt=""></div>
                                    {{--                    <div class="col-lg-4 col-md-12"><div id="googleMap" class="map-container"></div></div>--}}
                                </div>

                            </div>
                            {!! html_entity_decode($program->body) !!}
                            <div class="content-element3">

                                <div class="row">
                                    <div class="col-md-4 col-sm-12">

                                        <h6 class="event-title">Details</h6>

                                        <ul class="custom-list">

                                            <li><span>Date:</span><a
                                                    onclick="ical_download('{{$program->title}}', 'rthambara.ics', '{{$start_date}} {{$timezone}}', '{{$end_date}} {{$timezone}}')">
                                                    {{ date("jS F Y", strtotime($program->program_date))}}</a></li>
                                            {{--                                            <li><span>Time:</span> 3:30 PM - 5:00 PM</li>--}}
                                            @if(!$expired)
                                                {{--                                                <li><span>Cost:</span> <span class="product-price">--}}
                                                {{--                                                    <a href="#" @click.prevent="showModal">--}}
                                                {{--                                                        <i class="icon-cart mr-2">{{$program->cost}} INR</i></a></span>--}}
                                                {{--                                                </li>--}}
                                            @endif
                                            @if($program->level)
                                                <li><span>Level:</span> <a class="link-text">{{$program->level}}</a>
                                                </li>
                                            @endif
                                            {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                            {{--                                            </li>--}}

                                        </ul>

                                    </div>
                                    {{--                                    <div class="col-md-4 col-sm-12">--}}

                                    {{--                                        <h6 class="event-title">Organizer</h6>--}}

                                    {{--                                        <ul class="custom-list">--}}

                                    {{--                                            <li><span>Organizer Name:</span> Company Name</li>--}}
                                    {{--                                            <li content="telephone=no"><span>Phone:</span> (932) 733-3390</li>--}}
                                    {{--                                            <li><span>Email:</span> <a href="#"--}}
                                    {{--                                                                       class="link-text">info@companyname.com</a></li>--}}
                                    {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                    {{--                                            </li>--}}

                                    {{--                                        </ul>--}}

                                    {{--                                    </div>--}}
                                    <div class="col-md-4 col-sm-12">

                                        <h6 class="event-title">Venue</h6>

                                        <ul class="custom-list">

                                            @if($program->venue)
                                                <li><span>Venue Name:</span>{{$program->venue}}</li>
                                            @endif
                                            @if($program->address)
                                                <li><span>Address:</span>{{$program->address}}</li>
                                            @endif
                                            {{--                                            <li><span>Email:</span> <a href="#"--}}
                                            {{--                                                                       class="link-text">info@companyname.com</a></li>--}}
                                            {{--                                            <li><span>Website:</span> <a href="#" class="link-text">http://www.companyname.com</a>--}}
                                            {{--                                            </li>--}}

                                        </ul>

                                    </div>
                                </div>

                            </div>

                            {{--                            <div class="share-wrap">--}}

                            {{--                                <span class="share-title">Share this:</span>--}}
                            {{--                                <ul class="social-icons var2 share">--}}

                            {{--                                    <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-pinterest"><i class="icon-pinterest"></i></a></li>--}}
                            {{--                                    <li><a href="#" class="sh-mail"><i class="icon-mail"></i></a></li>--}}

                            {{--                                </ul>--}}

                            {{--                            </div>--}}

                        </div>

                    </div>

                    <div id="my-modal" class="popup" style="position: absolute !important;">

                        <div class="popup-inner">

                            <button type="button" class="close-popup"></button>

                            <div class="pricing-tables-holder flex-row item-col-1">

                                <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->
                                <div class="pricing-col" id="billing">

                                    <div class="pricing-table style-2" style="padding-top: 1%">

                                        <header class="pt-header">
                                            <div class="col-md-12 col-sm-12">

                                                <h5>Price Request</h5>
                                                {{--                                                <div class="alert alert-success" style="text-align: center">--}}
                                                {{--                                                    Online bookings via Card and PayPal will be made available at the--}}
                                                {{--                                                    soonest.<br>--}}
                                                {{--                                                    For now, please contact us at <a--}}
                                                {{--                                                        href="mailto:info@rtambharawellness.com">info@rtambharawellness.com.</a>--}}
                                                {{--                                                </div>--}}
                                                <form class="contact-form style-2" @submit.prevent="sendRequest">

                                                    <input type="hidden" name="program_id" value="{{$program->id}}">
                                                    <div class="contact-item">

                                                        <div class="row">
                                                            <div class="col-sm-6">

                                                                <label class="required">First Name</label>
                                                                <input name="first_name" type="text"
                                                                       v-model="form.firstName" required>

                                                            </div>
                                                            <div class="col-sm-6">

                                                                <label class="required">Last Name</label>
                                                                <input name="last_name" type="text"
                                                                       v-model="form.lastName" required>

                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="contact-item">

                                                        <label class="required">Address Line 1</label>
                                                        <input name="address" type="text" required
                                                               v-model="form.address1">

                                                    </div>

                                                    <div class="contact-item">

                                                        <label class="required">Address Line 2</label>
                                                        <input name="address_2" type="text" required
                                                               v-model="form.address2">

                                                    </div>

                                                    <div class="contact-item">

                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label class="required">City</label>
                                                                <input name="city" type="text" required
                                                                       v-model="form.city">
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <label class="required">Country</label>
                                                                <input name="country" type="text" required
                                                                       v-model="form.country">
                                                            </div>
                                                        </div>


                                                    </div>


                                                    <div class="contact-item">

                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label class="required">Post Code</label>
                                                                <input name="post_code" type="text" required
                                                                       v-model="form.postCode">

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label class="required">Phone</label>
                                                                <input name="phone" type="tel" required
                                                                       v-model="form.phone" v-on:input="validatePhone">
                                                                <span id="phone-error" style="color: #ed490c"></span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="contact-item">

                                                        <label class="required">Email</label>
                                                        <input name="email" type="email" required v-model="form.email">

                                                    </div>

                                                    <div class="contact-item">
                                                        {{--                                                        <button class="btn btn-style-3 " @click.prevent="closeModal">--}}
                                                        {{--                                                            <i class="fa fa-paypal">100INR</i>--}}
                                                        {{--                                                        </button>--}}

                                                        <span class="product-price">
                                                            <button class="btn popup-btn btn-big btn-style-3"
                                                                    type="submit">
{{--                                                                        <i class="icon-paypal mr-10">{{$program->cost}} INR</i>--}}
                                                                Request
                                                            </button>
                                                        </span>
                                                    </div>


                                                    <div v-if="computedResponseMessage.length > 0"
                                                         style="margin-top: 10px !important;">
                                                        <div class="alert alert-success">
                                                            <p>@{{computedResponseMessage}}</p>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>
                                        </header><!--/ .pt-header -->

                                    </div>

                                </div>
                                <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Pricing Table - - - - - - - - - - - - - - - - -->
                            {{--                <div class="pricing-col">--}}

                            {{--                    <div class="pricing-table">--}}

                            {{--                        <div class="label">Limited Time Offer</div>--}}

                            {{--                        <header class="pt-header">--}}

                            {{--                            <h4 class="pt-title">Unlimited <br> membership</h4>--}}

                            {{--                            <div class="pt-price">$109</div>--}}

                            {{--                            <div class="section-pre-title style-2">per month</div>--}}

                            {{--                            <div class="pt-disc">Students - $90/mo</div>--}}

                            {{--                        </header><!--/ .pt-header -->--}}

                            {{--                        <footer class="pt-footer">--}}

                            {{--                            <a href="#" class="btn btn-style-3">Buy Now</a>--}}

                            {{--                            <p>Minimum 4 month auto-renew commitment</p>--}}

                            {{--                        </footer><!--/ .pt-footer -->--}}

                            {{--                    </div>--}}

                            {{--                </div>--}}
                            <!-- - - - - - - - - - - - - - End of Pricing Tables - - - - - - - - - - - - - - - - -->

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

    @include('layouts.footer')


</div>

<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!-- JS Libs & Plugins
============================================ -->
<script src="/themes/wellness/js/libs/jquery.modernizr.js"></script>
<script src="/themes/wellness/js/libs/jquery-2.2.4.min.js"></script>
<script src="/themes/wellness/js/libs/jquery-ui.min.js"></script>
<script src="/themes/wellness/js/libs/retina.min.js"></script>
<script src="/themes/wellness/plugins/jquery.scrollTo.min.js"></script>
<script src="/themes/wellness/plugins/jquery.localScroll.min.js"></script>
<script src="/themes/wellness/plugins/isotope.pkgd.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
<script src="/themes/wellness/plugins/mad.customselect.js"></script>
<script src="/themes/wellness/plugins/jquery.queryloader2.min.js"></script>
<script src="/themes/wellness/plugins/owl.carousel.min.js"></script>

<!-- JS theme files
============================================ -->
<script src="/themes/wellness/js/plugins.js"></script>
<script src="/themes/wellness/js/script.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.common.js"></script>--}}
<script>

    document.addEventListener("DOMContentLoaded", function(){

        console.log('ready');
        document.getElementById("price-request").addEventListener("click", function() {
            console.log('Clicked');
            showModal();
        });
    });



    // $(function () {
    //
    //     $('#price-request').click(function () {
    //         console.log('Clicked');
    //         showModal();
    //     })
    //
    // });

    function showModal() {
        // console.log("modal opened");
        var modal = document.getElementById("my-modal");
        modal.style.display = "block";
    }


    function closeModal() {
        var modal = document.getElementById("my-modal");
        modal.style.display = "none";
        // console.log("modal closed");
        // this.checkout();
    }

    var vm = new Vue({
        el: '#app',
        data: {

            form: {
                programId: '',
                firstName: '',
                lastName: '',
                address1: '',
                address2: '',
                city: '',
                country: '',
                postCode: '',
                phone: '',
                email: '',
            },
            responseMessage: '',

        },
        computed: {

            computedResponseMessage: function() {
                return this.responseMessage;
            }
        },
        methods: {
            validatePhone: function() {
                var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

                if (this.form.phone !== '' && !re.test(String(this.form.phone).toLowerCase())) {

                    document.getElementById('phone-error').innerHTML = "invalid phone number";
                    return false;

                } else {
                    document.getElementById('phone-error').innerHTML = "";
                    return true;

                }

            },
            sendRequest:function() {

                this.form.programId = this.getMeta("program_id");

                var vm = this;
                if (!this.validatePhone()) {
                    return false;
                }
                axios.post('/api/request_price', this.form)
                    .then(function (resp) {
                        console.log(resp.data);
                        vm.responseMessage = resp.data.message;
                    })

            },

            getMeta: function(metaName) {
                var metas = document.getElementsByTagName('meta');

                for (var i = 0; i < metas.length; i++) {
                    if (metas[i].getAttribute('name') === metaName) {
                        return metas[i].getAttribute('content');
                    }
                }

                return '';
            },

        }
    });

</script>
<script>

    // Vue.use(VueStripeCheckout, 'pk_test_jvbkvHL3HF4XhFVbyP9axp4o00PRxXGS23');


    // var vm = new Vue({
    //     el: '#app',
    //     data() {
    //         return {
    //             full_nam: 'M F',
    //             program_id: '1',
    //             status: 'Closed',
    //             tokenFromPromise: {},
    //             tokenFromEvent: {},
    //             sampleCard: '4242 4242 4242 4242',
    //             image: '/images/logo.png',
    //             name: 'Rtahmbhara Wellness',
    //             description: '',
    //             currency: 'INR',
    //             amount: 100,
    //             quantity: '1',
    //             email: 'a@b.com',
    //             first_name: '',
    //             last_name: '',
    //             address: '',
    //             phone: '',
    //
    //
    //         }
    //     },
    //
    //     computed: {
    //         finalAmount() {
    //             return this.amount * 100;
    //         }
    //     },
    //     mounted() {
    //         this.program_id = this.getMeta("program_id");
    //         // console.log(this.program_id);
    //     },
    //     methods: {
    //         getMeta(metaName) {
    //             var metas = document.getElementsByTagName('meta');
    //
    //             for (var i = 0; i < metas.length; i++) {
    //                 if (metas[i].getAttribute('name') === metaName) {
    //                     return metas[i].getAttribute('content');
    //                 }
    //             }
    //
    //             return '';
    //         },
    //
    //         showModal() {
    //             // console.log("modal opened");
    //             var modal = document.getElementById("my-modal");
    //             modal.style.display = "block";
    //         },
    //         closeModal() {
    //             var modal = document.getElementById("my-modal");
    //             modal.style.display = "none";
    //             // console.log("modal closed");
    //             this.checkout();
    //         },
    //
    //
    //         async checkout() {
    //             this.tokenFromPromise = await this.$refs.checkoutRef.open();
    //         },
    //
    //
    //         done(token) {
    //             this.tokenFromEvent = token;
    //
    //             console.log('-----------------------');
    //             console.log(token.token.id);
    //
    //             var data = {
    //                 'token': token.token,
    //                 'program_id': this.program_id,
    //                 'email': this.email,
    //                 'quantity': this.quantity,
    //                 'first_name': this.first_name,
    //                 'last_name': this.last_name,
    //                 'address': this.address,
    //                 'phone': this.phone,
    //             };
    //
    //
    //             axios.post('/api/bookings', data)
    //                 .then(function (resp) {
    //
    //                     console.log(resp);
    //
    //                 })
    //
    //         },
    //         opened() {
    //             this.status = 'Opened';
    //         },
    //         closed() {
    //             this.status = 'Closed';
    //         },
    //         submit(token) {
    //             console.log('token', token);
    //             console.log('Submit this token to your server to perform a stripe charge, or subscription.');
    //         },
    //     }
    // });
</script>
</body>
</html>
