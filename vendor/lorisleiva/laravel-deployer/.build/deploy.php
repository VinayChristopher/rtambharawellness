<?php

namespace Deployer;

require 'recipe/laravel-deployer.php';

/*
 * Includes
 */

/*
 * Options
 */

set('strategy', 'basic');
set('application', 'Laravel');
set('repository', 'git@bitbucket.org:VinayChristopher/rtambharawellness.git');
set('php_fpm_service', 'php7.3-fpm');

/*
 * Hosts and localhost
 */

host('rtambharawellness.com')
    ->set('deploy_path', '/home/forge/rtambharawellness.com')
    ->user('forge');

/*
 * Strategies
 */

/*
 * Hooks
 */

after('hook:ready', 'artisan:storage:link');
after('hook:ready', 'artisan:view:clear');
after('hook:ready', 'artisan:cache:clear');
after('hook:ready', 'artisan:config:cache');
after('hook:ready', 'artisan:migrate');
after('hook:done', 'fpm:reload');
after('hook:rollback', 'fpm:reload');