<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Postmark\\' => array($vendorDir . '/wildbit/swiftmailer-postmark/src'),
    'Pimple' => array($vendorDir . '/pimple/pimple/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
);
