<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProgramPriceRequest extends Notification
{
    use Queueable;

    protected $program;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($program, $user)
    {
        $this->program = $program;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
            ->greeting('Hello!')
            ->subject('Rthambara Wellness Payment Notifications')
            ->line('You have a new price request')
            ->line('Program :' . $this->program->title)
            ->line('Venue :' . $this->program->venue)
            ->line('Program Date ' . $this->program->program_date)
            ->line('Participant :' . $this->user->name)
            ->line('Address 1  :' . $this->user->address)
            ->line('Address 2 :' . $this->user->address_2)
            ->line('City :' . $this->user->city)
            ->line('Country :' . $this->user->country)
            ->line('Post Code :' . $this->user->post_code)
            ->line('Phone :' . $this->user->phone)
//            ->action('View the invoice details here', $this->receipt_url)
            ->line('Please note that this mailbox is not monitored.')
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
