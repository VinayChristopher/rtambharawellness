<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserBookingNotification extends Notification
{
    use Queueable;

    protected $receipt_url;
    protected $program;
    protected $extra_emails;

    /**
     * Create a new notification instance.
     *
     * @param $receipt_url
     * @param $ticket
     */
    public function __construct($program, $receipt_url, $extra_emails)
    {
        $this->program = $program;
        $this->receipt_url = $receipt_url;
        $this->extra_emails = $extra_emails;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello! ' . $notifiable->name)
            ->subject('Rthambara Welness Payment Notifications')
            ->line('You have a new booking confirmation.')
            ->line('Purchased Ticket: ' . $this->program->title)
//            ->action('View the invoice details here', $this->receipt_url)
            ->line('Please note that this mailbox is not monitored.')
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
