<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminBookingNotification extends Notification
{

    use Queueable;

    protected $receipt_url;
    protected $program;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @param $receipt_url
     * @param $ticket
     */
    public function __construct($program, $receipt_url, $user)
    {
        $this->program = $program;
        $this->receipt_url = $receipt_url;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello!')
            ->subject('Rthambara Wellness Payment Notifications')
            ->line('You have a new booking')
            ->line('Participant :' . $this->user->name)
            ->line('Address 1  :' . $this->user->address)
            ->line('Address 2 :' . $this->user->address_2)
            ->line('City :' . $this->user->city)
            ->line('Country :' . $this->user->country)
            ->line('Post Code :' . $this->user->post_code)
            ->line('Phone :' . $this->user->phone)
            ->line('Purchased Ticket: ' . $this->program->title)
//            ->action('View the invoice details here', $this->receipt_url)
            ->line('Please note that this mailbox is not monitored.')
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
