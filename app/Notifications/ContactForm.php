<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactForm extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Contact Form Details')
            ->greeting('Hello !')
            ->line('Contact Form Details')
            ->line('From: ' . $this->data['cf-name'])
            ->line('Email: ' . $this->data['cf-email'])
            ->line('Subject: ' . $this->data['cf-subject'])
            ->line('Inquiry Type: ' . $this->data['cf-type'])
            ->line('Program: ' . $this->data['cf-program'])
            ->line('Venue: ' . $this->data['cf-venue'])
            ->line('Participants: ' . $this->data['cf-participants'])
            ->line('Message: ' . $this->data['cf-message']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
