<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CaptchaController extends Controller
{
    public function create()
    {
        return view('contact-us');
    }

    public function contact(Request $request)
    {
        $request->validate([
            'cf-name' => 'required',
            'cf-subject' => 'required',
            'cf-email' => 'required|email',
            'cf-message' => 'required',
            'captcha' => 'required|captcha'
        ]);
        // $validator

        // $rules = [
        //     'captcha' => 'required|captcha',
        //     'cf-email' => 'required|email',
        //     'cf-name' => 'required',
        //     'cf-subject' => 'required',
        //     'cf-message' => 'required',
        // ];
        // $validator = validator()->make(request()->all(), $rules);

        // if($validator->fails()){
        //     return redirect('contact-us_test')->withErrors($validator);
        // }

        $email = config("services.contact.email");
        $user = new User;
        $user->email = $email;
        $data = $request->all();

        $user->notify(new \App\Notifications\ContactForm($data));
        
        return 1;
        // return redirect('contact-us_test')->withErrors($validator);
    }
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }
}
