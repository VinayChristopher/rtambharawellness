<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Booking;
use App\Notifications\UserBookingNotification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class BookingController extends Controller
{
    public function index()
    {
        $bookings = Booking::latest()->get();

        return response(['data' => $bookings], 200);
    }

    public function store(BookingRequest $request)
    {


        Log::info($request->all());

        $token = $request->input('token');
        $email = $token['email'];
        $quantity = $request->input('quantity');
        $program_id = $request->input('program_id');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $phone = $request->input('phone');
        $address = $request->input('address');

        $program = \App\Program::findOrFail($program_id);


        Stripe::setApiKey(config('services.stripe.key'));

        $customer = Customer::create(array(
            'email' => $token['email'],
            'source' => $token['id']
        ));

        $charge = Charge::create(array(
            'customer' => $customer->id,
            'amount' => $program->cost * 100 * $quantity,
            'currency' => 'INR'
        ));


//    dump($charge);

        if ($charge && $charge->status == 'succeeded') {


            $user = User::firstOrCreate([
                'email' => $token['email']
            ], [
                'name' => $first_name . " " . $last_name,
                'password' => Str::random(10),
                'method' => 'default'
            ]);


            \App\Booking::create([
                'user_id' => $user->id, //later
                'booker_email' => $token['email'],
                'quantity' => $quantity,
                'program_id' => $program_id,
                'payment_method' => 'stripe',
                'customer_id' => $customer->id,
                'charged_amount' => $charge->amount,
                'transaction_id' => $charge->id,
                'receipt_url' => $charge->receipt_url,
                'status' => 'APPROVED',
            ]);

            $admin = new User;
            $admin->email = config('services.contact.email');


            $admin->notify(new \App\Notifications\AdminBookingNotification($program, $charge, $user));
            $user->notify(new UserBookingNotification($program, $charge, $extra_emails));
        }

        return $charge->status ?? 'failed';


//        $booking = Booking::create($request->all());
//
//        return response(['data' => $booking ], 201);

    }

    public function show($id)
    {
        $booking = Booking::findOrFail($id);

        return response(['data', $booking], 200);
    }

    public function update(BookingRequest $request, $id)
    {
        $booking = Booking::findOrFail($id);
        $booking->update($request->all());

        return response(['data' => $booking], 200);
    }

    public function destroy($id)
    {
        Booking::destroy($id);

        return response(['data' => null], 204);
    }
}
