<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{


    public function index($location)
    {
        $l = Location::where('key', '=', $location)->first();

        if (!isset($l)) {
            return abort('404');
        }

        return view('location_single', ['location' => $l]);

    }

    public function summary()
    {
        $locations = Location::all();
        return view('locations', ['locations' => $locations]);
    }
}
