<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Notifications\UserBookingNotification;
use App\Program;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payee;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

class PaypalController extends Controller
{
    private $_api_context;

    public function __construct()
    {

        /** PayPal api context **/

        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            config('services.paypal.client_id'),
            config('services.paypal.secret')

        ));
        $this->_api_context->setConfig(config('services.paypal.settings'));

    }

    public function payWithpaypal(Request $request)
    {


        $program_id = $request->input('program_id');
        $email = $request->input('email');
//        $quantity = $request->input('quantity');
        $program_id = $request->input('program_id');
        $extra_emails = $request->input('extra_emails');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $phone = $request->input('phone');
        $address = $request->input('address');

        $address_2 = $request->input('address_2');
        $city = $request->input('city');
        $country = $request->input('country');
        $post_code = $request->input('post_code');


        $program = \App\Program::findOrFail($program_id);

        Session::put('program_id', $program_id);

        $user = User::firstOrCreate([
            'email' => $email
        ], [
            'name' => $first_name . " " . $last_name,
            'password' => Str::random(10),
            'method' => 'default'
        ]);

        $user->address = $address;
        $user->phone = $phone;
        $user->address_2 = $address_2;
        $user->city = $city;
        $user->country = $country;
        $user->post_code = $post_code;
        $user->save();

        $booking = \App\Booking::create([
            'user_id' => $user->id, //later
            'booker_email' => $email,
            'quantity' => 1,
            'program_id' => $program_id,
            'payment_method' => 'paypal',
            'customer_id' => 'paypal_customer',
            'charged_amount' => $program->cost,
            'transaction_id' => 'paypal_transaction',
            'receipt_url' => 'paypal_url',
            'status' => 'PENDING',
        ]);


        $payerInfo = new PayerInfo();
        $payerInfo->setEmail(config('services.contact.invoice'));

        $payer = new Payer();
        $payer->setPaymentMethod('paypal')
        ->setPayerInfo($payerInfo);


        $item_1 = new Item();

        $item_1->setName($program->title)/** item name **/
        ->setCurrency('INR')
            ->setQuantity(1)
            ->setPrice($program->cost);
        /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('INR')
            ->setTotal($program->cost);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setCustom($booking->id)
            ->setDescription('Rtambahara Wellness Ticket Purchase');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status'))/** Specify return URL **/
        ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {

            $payment->create($this->_api_context);

        } catch (PayPalConnectionException  $ex) {

            if (Config::get('app.debug')) {

                Session::put('error', 'Connection timeout');
                return redirect('/');

            } else {

                Session::put('error', 'Some error occur, sorry for inconvenient');
                return redirect('/');

            }

        }

        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {

            /** redirect to paypal **/
            return Redirect::away($redirect_url);

        }

        Session::put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');

    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            Session::put('error', 'Payment failed');
            $program_id = Session::get('program_id');
            return redirect("/event-single/{$program_id}");

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        Log::info($result);

        if ($result->getState() == 'approved') {


            $transactions = $result->getTransactions();

            foreach ($transactions as $transaction) {
                $bookingId = $transaction->getCustom();
                $booking = Booking::findOrFail($bookingId);
                $booking->status = 'APPROVED';
                $booking->save();
            }
            $admin = new User;
            $admin->email = config('services.contact.invoice');


            $user = User::find($booking->user_id);
            $admin->notify(new \App\Notifications\AdminBookingNotification(Program::find($booking->program_id), '', $user));
            $user->notify(new UserBookingNotification(Program::find($booking->program_id), '', []));

            Session::put('success', 'Payment success');
            $program_id = Session::get('program_id');
            return redirect("/event-single/{$program_id}");


        }

        Session::put('error', 'Payment failed');
        $program_id = Session::get('program_id');
        return redirect("/event-single/{$program_id}");

    }

}
