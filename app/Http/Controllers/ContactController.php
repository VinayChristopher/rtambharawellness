<?php

namespace App\Http\Controllers;

use App\Notifications\NewsletterSubscription;
use App\Notifications\ProgramPriceRequest;
use App\Notifications\ContactForm;
use App\Program;
use App\Subscription;
use App\User;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function refreshCaptcha()
    {
        return response()->json(['captcha' => captcha_src('flat')]);
    }

    public function newsletter(Request $request)
    {
        $request->validate([
            'newsletter-email' => 'required|email'
        ]);

        $newsletterEmail = request('newsletter-email');

        Subscription::create([
            'email' => $newsletterEmail,
            'status' => 'subscribed'
        ]);

        $email = config("services.contact.email");
        $user = new User;
        $user->email = $newsletterEmail;

        $user->notify(new NewsletterSubscription($email));

        return '1';
    }

    public function contact(Request $request)
    {
        $request->validate([
            'cf-name' => 'required',
            'cf-subject' => 'required',
            'cf-email' => 'required|email',
            'cf-message' => 'required',
            'captcha' => 'required|captcha'

        ]);

        $email = config("services.contact.email");
        $user = new User;
        $user->email = $email;
        $data = $request->all();
        
        $user->notify(new \App\Notifications\ContactForm($data)); 
        //return redirect('contact-us')->with('status', 'Contacted Successfully');
        return 1;
        /* $rules = [
            'captcha' => 'required|captcha',
            'cf-email' => 'required|email',
            'cf-name' => 'required',
            'cf-subject' => 'required',
            'cf-message' => 'required',
        ];

        $validator = validator()->make(request()->all(), $rules);

        if($validator->fails()){
            //return redirect('contact-us')->withErrors($validator);
            Session::put('flash_message', 'Form didn\'t submit');
            return redirect()->back();
        }
        else{
            $email = config("services.contact.email");
            $user = new User;
            $user->email = $email;
            $data = $request->all();
            $user->notify(new \App\Notifications\ContactForm($data));
            //return redirect('contact-us')->with('status', 'Contacted Successfully');
            Session::put('flash_message', 'Form submitted');
            return redirect()->back();
        } */
    }

    public function price_request(Request $request)
    {

        $request->validate([
            'programId' => 'required',
            'email' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'address1' => 'required',
            'address2' => '',
            'city' => 'required',
            'country' => 'required',
            'postCode' => 'required',
            'phone' => 'required',
        ]);

        $user = new User;
        $user->email = request('email');
        $user->name = request('firstName') . ' ' . request('lastName');
        $user->address = request('address1');
        $user->address2 = request('address2');
        $user->city = request('city');
        $user->country = request('country');
        $user->post_code = request('postCode');
        $user->phone = request('phone');


        $program = Program::findOrFail(request('programId'));

        $admin = new User;
        $admin->email = config('services.contact.invoice');

        $admin->notify(new ProgramPriceRequest($program, $user));

        return ['message' => 'Thank you. We will contact you with the price details soon.'];
    }
}
