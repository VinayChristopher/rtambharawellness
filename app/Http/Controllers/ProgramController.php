<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramRequest;
use App\Program;

class ProgramController extends Controller
{
    public function index()
    {
        $programs = Program::latest()->get();

        return response(['data' => $programs ], 200);
    }

    public function store(ProgramRequest $request)
    {
        $program = Program::create($request->all());

        return response(['data' => $program ], 201);

    }

    public function show($id)
    {
        $program = Program::findOrFail($id);

        return response(['data', $program ], 200);
    }

    public function update(ProgramRequest $request, $id)
    {
        $program = Program::findOrFail($id);
        $program->update($request->all());

        return response(['data' => $program ], 200);
    }

    public function destroy($id)
    {
        Program::destroy($id);

        return response(['data' => null ], 204);
    }
}
