<?php

namespace App;



use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $guarded = ['id'];
    protected $casts = [
        'program_date' => 'datetime'
    ];

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }


    public static function forDate($date)
    {
        return optional(Program::whereDate('program_date', $date))->first();
    }

    public function isExpired(){

        $now = Carbon::now();
        return $this->program_date < $now;

    }

}
