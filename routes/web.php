<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

Route::post('/contact-us', 'ContactController@contact');
Route::get('/refreshcaptcha', 'ContactController@refreshCaptcha');


Route::get('/coming-soon', function () {
    return view('coming_soon');
});

Route::get('/', function () {
    return view('index');
});
Route::get('/about-us', function () {
    return view('about_us');
});

Route::get('/team', function () {
    return view('team');
});
Route::get('/retreats-and-wellness', function () {
    return view('retreats_and_wellness');
});
Route::get('/corporate-program', function () {
    return view('corporate_program');
});
Route::get('/contact-us', function () {
    return view('contact-us');
});
Route::get('/program-details', function () {
    return view('program_details');
});
Route::get('/corporate-program-details', function () {
    return view('corporate_program_details');
});
Route::get('/location-single/{location}', 'LocationController@index');
Route::get('/location-single-test', function (){
    return view('location_single_test');
});
Route::get('/event-single/{id}', function ($id) {

    $program = Program::findOrFail($id);

    $start_date_ical = date('D M d Y H:i:s ', strtotime($program->program_date));
    $end_date_ical = $start_date_ical;
    if ($program->duration) {
        $end_date_ical = date('D M d Y H:i:s ', strtotime($program->program_date . "+" . $program->duration . " days"));
    }
    $timezone = 'GMT+5:30';
    $expired = $program->isExpired();

    $data = [];
    $data['program'] = $program;
    $data['start_date'] = $start_date_ical;
    $data['end_date'] = $end_date_ical;
    $data['timezone'] = $timezone;
    $data['expired'] = $expired;

    return view('events_single_1', $data);
});
Route::get('/event-single-test', function () {
    return view('events_single_test');
});
Route::get('/locations', 'LocationController@summary');
Route::get('/past-events-gallery', function () {
    return view('gallery');
});


// route for processing payment
Route::post('paypal', 'PaypalController@payWithpaypal');
// route for check status of the payment
Route::get('status', 'PaypalController@getPaymentStatus')->name('status');


Route::get('/upcoming-events', function (Request $request) {

    $now = \Carbon\Carbon::now();

    if ($request->has('month')) {
        $month = $request->input('month');
    } else {
        $month = $now->format('m');
    }

    if ($request->has('year')) {
        $year = $request->input('year');

    } else {
        $year = $now->format('Y');
    }

    $first_day = new \DateTime("01-$month-$year"); // <== instance from another API
    $today = \Carbon\Carbon::instance($first_day);

    $tempDateWeek = \Carbon\Carbon::createFromDate($today->year, $today->month, 1);
    $tempDate = \Carbon\Carbon::createFromDate($today->year, $today->month, 1);

    $dayNames = [];

    for ($i = 0; $i < 7; $i++) {
        $dayNames[] = $tempDateWeek->dayName;
        $tempDateWeek->addDay();
    }

    $dayEvents = [];

    do {


        for ($i = 0; $i < 5; $i++) {

            $dayEvents[$i] = [];

            for ($j = 0; $j < 7; $j++) {

                $dayEvents[$i][] = [

                    'day' => $tempDate->day,
                    'date' => $tempDate,
                    'program' => \App\Program::forDate($tempDate->format('Y-m-d'))
                ];

                $tempDate->addDay();
            }


        }

    } while ($tempDate->month == $today->month);


    $prev = $today->copy()->subMonth();
    $prev_month = $prev->format('m');
    $prev_month_name = $prev->format('F');
    $prev_year = $prev->format('Y');

    $next = $today->copy()->addMonth();
    $next_month = $next->format('m');
    $next_month_name = $next->format('F');
    $next_year = $next->format('Y');

    $current_year = $today->format('Y');
    $current_month = $today->format('F');


    $data = [
        'dayNames' => $dayNames, 'dayEvents' => $dayEvents, 'previous_month' => $prev_month_name, 'next_month' => $next_month_name,
        'previous_month_url' => "month=$prev_month&year=$prev_year",
        'next_month_url' => "month=$next_month&year=$next_year",
        'current_year' => $current_year,
        'current_month' => $current_month
    ];

    return view('events_month', $data);
});

Route::get('/home', 'HomeController@index')->name('home');

Route::any('captcha-test_check', function() {
    if (request()->getMethod() == 'POST') {
        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            echo '<p style="color: #ff0000;">Incorrect!</p>';
        } else {
            echo '<p style="color: #00ff30;">Matched :)</p>';
        }
    }

    $form = '<form method="post" action="captcha-test_check">';
    $form .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
    $form .= '<p>' . captcha_img('flat') . '</p>';
    $form .= '<p><input type="text" name="captcha"></p>';
    $form .= '<p><button type="submit" name="check">Check</button></p>';
    $form .= '</form>';
    return $form;
});
