<?php

use App\Notifications\ProgramPriceRequest;
use App\Program;
use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('subscriptions', 'SubscriptionController');
Route::apiResource('bookings', 'BookingController');
Route::apiResource('programs', 'ProgramController');

Route::post('/request_price', 'ContactController@price_request');
Route::post('/newsletter', 'ContactController@newsletter');
