<?php

use App\Program;
use Illuminate\Database\Seeder;

class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Program::create([
            'title' => 'Pravesa - Rejuvenate Program',
            'cost' => '100',
            'program_date' => '2019-07-27 08:00:00',
            'duration' => '3',
            'description' => 'Pravesa is an introductory program with Rtambhara Wellness.
                              This program is designed to invite you on a journey of discovery
                               enabling you to seek well being, wisdom, and spirituality.',
            'level'=>'Beginner',
            'address'=>'Devanahalli, Bengaluru',
            'venue'=>'The School of Ancient Wisdom',
            'body' =>'<div class="row">


                                <div class="col-md-6 " style="margin-bottom: 5%">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="title">Intro</h4>
                                            <p>Pravesa is an introductory program with Rtambhara Wellness. This program
                                                is
                                                designed
                                                to invite you on a journey of discovery enabling you to seek well being,
                                                wisdom,
                                                and
                                                spirituality.</p>

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10%">
                                        <div class="col-md-12">
                                            <h4 class="title">About the Program</h4>
                                            <p>A two night and three day getaway, Pravesa is the perfect way to
                                                rejuvenate. Take
                                                a break from the stress of everyday life and adopt simple changes for a
                                                happier
                                                lifestyle.</p>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 " style="margin-bottom: 5%">
                                    <div class="row">
                                        <h4 class="title">What to Expect</h4>
                                        <p>You can expect a memorable 3 days of curated talks and activities that will
                                            delve
                                            into Wellness, Happiness and Understanding one’s ego, mind and your "inner”
                                            self. Learn Yoga, Laughter therapy and Meditation to positively impact your
                                            physical and mental well being.</p>
                                        <p>
                                            Be prepared to cook up some delicious snacks from organic foods, literally
                                            from
                                            "Farm to Plate”. Visit a cow farm to understand why the cow is revered, and
                                            how
                                            much benefit one can get from Nature. And, be mesmerized by cultural
                                            programs
                                            every evening that will unleash your artistic and creative side.
                                        </p>
                                        <p>
                                            End the program with interactive sessions with the faculty to understand how
                                            you
                                            can continue on this journey. Take home valuable inputs that you can use for
                                            your daily living. And, walk away with the determination that would lead to
                                            "Transformation" and "Happier Living"
                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-bottom: 5%">
                                    <div class="row">
                                        <h4 class="title">Costs & Inclusions</h4>
                                        <p>The pilot program for Pravesa is complimentary for friends, family and
                                            ambassadors of Rtambhara Wellness. The program will be at The School of
                                            Ancient
                                            Wisdom in Devanahalli, Bengaluru. Accommodation, satvik food and beverages,
                                            and
                                            program materials are included.</p>

                                    </div>
                                </div>
                            </div>'
        ]);

        //sept 13-15
        Program::create([
            'title' => 'Pravesa - Rejuvenate Program',
            'cost' => '100',
            'program_date' => '2019-09-13 08:00:00',
            'duration' => '3',
            'description' => 'Pravesa is an introductory program with Rtambhara Wellness.
                              This program is designed to invite you on a journey of discovery
                               enabling you to seek well being, wisdom, and spirituality.',
            'level'=>'Beginner',
            'address'=>'Devanahalli, Bengaluru',
            'venue'=>'The School of Ancient Wisdom',
            'body' =>'<div class="row">


                                <div class="col-md-6 " style="margin-bottom: 1%">

                                    <div class="row" >
                                        <div class="col-md-12">
                                            <h4 class="title">About the Program</h4>
                                               <p>Pravesa is an introductory program with Rtambhara Wellness. This program
                                                is
                                                designed
                                                to invite you on a journey of discovery enabling you to seek well being,
                                                wisdom,
                                                and
                                                spirituality.</p>
                                            <p>A two night and three day getaway, Pravesa is the perfect way to
                                                rejuvenate. Take
                                                a break from the stress of everyday life and adopt simple changes for a
                                                happier
                                                lifestyle.</p>

                                        </div>

                                    </div>
                                    <div class="row">
                                     
                                <div class="col-md-12" style="margin-top: 5%">
                                    <h4 class="title">Costs & Inclusions</h4>
                                    <div class="a-content">
                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> Price on request
                                    </p>
                                    </div>
                                </div>
                                     </div>
                                </div>
                                <div class="col-md-6 " style="margin-bottom: 1%">
                                    <div class="row">
                                        <h4 class="title">What to Expect</h4>
                                        <p>You can expect a memorable 3 days of curated talks and activities that will
                                            delve
                                            into Wellness, Happiness and Understanding one’s ego, mind and your "inner”
                                            self. Learn Yoga, Laughter therapy and Meditation to positively impact your
                                            physical and mental well being.</p>
                                        <p>
                                            Be prepared to cook up some delicious snacks from organic foods, literally
                                            from
                                            "Farm to Plate”. Visit a cow farm to understand why the cow is revered, and
                                            how
                                            much benefit one can get from Nature. And, be mesmerized by cultural
                                            programs
                                            every evening that will unleash your artistic and creative side.
                                        </p>
                                        <p>
                                            End the program with interactive sessions with the faculty to understand how
                                            you
                                            can continue on this journey. Take home valuable inputs that you can use for
                                            your daily living. And, walk away with the determination that would lead to
                                            "Transformation" and "Happier Living"
                                        </p>
                                    </div>
                                </div>
                            </div>'
        ]);

        //oct 11-13
        Program::create([
            'title' => 'Pravesa - Rejuvenate Program',
            'cost' => '100',
            'program_date' => '2019-10-11 08:00:00',
            'duration' => '3',
            'description' => 'Pravesa is an introductory program with Rtambhara Wellness.
                              This program is designed to invite you on a journey of discovery
                               enabling you to seek well being, wisdom, and spirituality.',
            'level'=>'Beginner',
            'address'=>'Devanahalli, Bengaluru',
            'venue'=>'The School of Ancient Wisdom',
            'body' =>'<div class="row">


                                <div class="col-md-6 " style="margin-bottom: 1%">

                                    <div class="row" >
                                        <div class="col-md-12">
                                            <h4 class="title">About the Program</h4>
                                               <p>Pravesa is an introductory program with Rtambhara Wellness. This program
                                                is
                                                designed
                                                to invite you on a journey of discovery enabling you to seek well being,
                                                wisdom,
                                                and
                                                spirituality.</p>
                                            <p>A two night and three day getaway, Pravesa is the perfect way to
                                                rejuvenate. Take
                                                a break from the stress of everyday life and adopt simple changes for a
                                                happier
                                                lifestyle.</p>

                                        </div>

                                    </div>
                                    <div class="row">
                                     
                                <div class="col-md-12" style="margin-top: 5%">
                                    <h4 class="title">Costs & Inclusions</h4>
                                    <div class="a-content">
                                    <p>
                                        <strong>Inclusions:</strong> Stay, Program materials, Food and Beverage
                                    </p>
                                    <p>
                                        <strong>Cost:</strong> Price on request
                                    </p>
                                    </div>
                                </div>
                                     </div>
                                </div>
                                <div class="col-md-6 " style="margin-bottom: 1%">
                                    <div class="row">
                                        <h4 class="title">What to Expect</h4>
                                        <p>You can expect a memorable 3 days of curated talks and activities that will
                                            delve
                                            into Wellness, Happiness and Understanding one’s ego, mind and your "inner”
                                            self. Learn Yoga, Laughter therapy and Meditation to positively impact your
                                            physical and mental well being.</p>
                                        <p>
                                            Be prepared to cook up some delicious snacks from organic foods, literally
                                            from
                                            "Farm to Plate”. Visit a cow farm to understand why the cow is revered, and
                                            how
                                            much benefit one can get from Nature. And, be mesmerized by cultural
                                            programs
                                            every evening that will unleash your artistic and creative side.
                                        </p>
                                        <p>
                                            End the program with interactive sessions with the faculty to understand how
                                            you
                                            can continue on this journey. Take home valuable inputs that you can use for
                                            your daily living. And, walk away with the determination that would lead to
                                            "Transformation" and "Happier Living"
                                        </p>
                                    </div>
                                </div>
                            </div>'
        ]);
    }
}
