<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Location::create([
            'key' => 'mandrem-village-resort',
            'heading' => 'Mandrem Village Resort - Goa, India',
            'main_image' => '/images/locations/mandrem/goa4.jpg',
            'url' => 'https://mandremvillageresort.com/',
            'image_prefix' => '/images/locations/mandrem/goa',
            'image_count' => 5,
            'body' => '
                            <div>
                                <p class="content-element3">
                                    Featuring a seasonal outdoor swimming pool and a garden, Mandrem Village Resort is
                                    located in Mandrem and is a beach front resort. Located in the Mandrem Beach
                                    district,
                                    the property provides guests with access to a restaurant. All rooms are equipped
                                    with a
                                    balcony with a garden view.</p>
                                <p class="content-element3">
                                    The rooms are fitted with a flat-screen TV with
                                    satellite channels, a kettle, a shower, free toiletries and a desk. Every room is
                                    equipped with a private bathroom. All rooms are equipped with a seating area.</p>
                                <p class="content-element3">
                                    A continental breakfast is served daily at the property.</p>
                                <p class="content-element3">
                                    The resort offers a
                                    sun terrace.</p>
                                <p class="content-element3">
                                    Speaking English and Hindi, staff at the 24-hour front desk can
                                    help you plan your stay.
                                </p>
                                <p class="content-element3">
                                    Calangute is 15 km from Mandrem Village Resort, while
                                    Panaji is 22 km away. Goa International Airport is 34 km from the property.
                                </p>
                                <p class="content-element3">
                                    Couples particularly like the location — they rated it 9.0 for a two-person trip.
                                </p>
                                <p class="content-element3">

                                    This property is also rated for the best value in Mandrem! Guests are getting more
                                    for their money when compared to other properties in this city.
                                </p>
                                <p class="content-element3">
                                    We speak your language!
                                </p>
                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list">

                                                <li>Restaurants</li>
                                                <li>SPA</li>
                                                <li>Leisure & Activities</li>
                                                <li>Pet Friendly</li>
                                                <li>Yogo & Naturopathy and Alas!</li>
                                                <li>There is no WiFi in Paradise!</li>
                                                <li>Website: <a href="https://mandremvillageresort.com/" target="_blank" class="link-text">https://mandremvillageresort.com</a>
                                                </li>

                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6><p>Mandrem Village, Junaswada, Mandrem Beach,
                                                North
                                                Goa, Mandrem, Goa 403527</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'wild-grass-nature-resort',
            'heading' => 'Wild Grass Nature Resort, Sri Lanka',
            'main_image' => '/images/locations/wild-grass/sl1.jpg',
            'url' => 'https://www.wildgrass.lk',
            'image_prefix' => '/images/locations/wild-grass/sl',
            'image_count' => 14,
            'body' => '                            <div>
                                <p class="content-element3">Wild Grass Nature Resort is located in the Cultural Triangle
                                    of Sri Lanka, close to the UNESCO world heritage sites of Sigiriya Lion Rock and
                                    Dambulla Cave Temples.</p>
                                <p class="content-element3">Comprising just 8 villas, Wild Grass is set amidst 25 acres
                                    of unfenced forestland that is frequently visited by rabbits, deer, wild boar,
                                    monkeys and over 40 species of birds as well as the occasional wild elephant.</p>
                                <p class="content-element3">It is the ideal location for nature lovers, whether families
                                    with children, couples or honeymooners, who wish to enjoy their holiday disturbed
                                    only by the sounds of nature, while enjoying the ancient history and marvels of the
                                    Cultural Triangle.</p>
                                <p class="content-element3">The unique feature of Wild Grass remains its exceptional
                                    natural beauty; our villas promote the sense of being at home amidst an abundance of
                                    space and absolute privacy – the ultimate luxuries afforded to the traveler seeking
                                    a perfect rest as recommended by insightguides.com and Sri Lanka Unbound.</p>

                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list">

                                                <li>Eco-Friendly Villas</li>
                                                <li>Solar Energy </li>
                                                <li>Vegetable Garden</li>
                                                <li>Restaurants</li>
                                                <li>Tours</li>
                                                <li>Safaris</li>
                                                <li>Website: <a href="https://www.wildgrass.lk/"
                                                                target="_blank" class="link-text">https://www.wildgrass.lk</a>
                                                </li>

                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>Kumbukkadanwala Farmhouse Road, (Govigammanaya Main Road), Kumbukkadanwala,
                                            <br>
                                                Sigiriya, Sri Lanka
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'school-of-ancient-wisdom',
            'heading' => 'School of Ancient Wisdom - Bangalore, India',
            'main_image' => '/images/locations/ancient-wisdom/soa1.jpg',
            'url' => 'http://www.theschoolofancientwisdom.org/',
            'image_prefix' => '/images/locations/ancient-wisdom/soa',
            'image_count' => 12,
            'body' => '                            <div>
                                <p class="content-element3">
                                    The School of Ancient Wisdom is a centre for the development of the human potential
                                    in the tradition of mystery schools. It teaches the esoteric principles of ageless,
                                    ancient wisdom. The School allows aspirants to progress at their own pace, with the
                                    freedom to interpret the truth in the teachings, as they see it.
                                </p>
                                <p class="content-element3">
                                    The centre is dedicated to realizing and preserving the ageless wisdom of the
                                    country, which provides a holistic view of the world and a vision for individuals to
                                    move towards a self-transformation process. The school aims to be a guiding force
                                    for dynamic living, where wisdom is above all and commercial motives don’t exist. We
                                    hope to steer mankind on the path towards right action, knowledge and illumination.
                                </p>

                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list">

                                                <li>Various courses devoted to the spread of occult truth and the development of the higher virtues of life</li>
                                                <li>Group Meditation</li>
                                                <li>Yoga</li>
                                                <li>Library</li>
                                                <li>Bookshop</li>
                                                <li>Research Centre for Holistic Therapy</li>
                                                <li>Ayurvedic Doctor</li>
                                                <li>Website: <a href="http://www.theschoolofancientwisdom.org/"
                                                                target="_blank" class="link-text">http://www.theschoolofancientwisdom.org</a>
                                                </li>

                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>I.V.C. Road, Bangalore Rural, Bangalore, Karnataka 562110
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'mahua-vann',
            'heading' => 'Mahua Vann - Nagpur, India',
            'main_image' => '/images/locations/mahua-vann/garden_pond.jpg',
            'url' => 'http://www.mahuaresorts.com/index.htm',
            'image_prefix' => '/images/locations/mahua-vann/nag',
            'image_count' => 12,
            'body' => '                            <div>
                                <p class="content-element3">
                                    Mahua Vann is a true haven of tranquility in a pristine wilderness setting where you
                                    experience nature amidst the presence of our permanent feathered residents. It
                                    assures you an authentic experience in complete harmony with the local community
                                    habitat as well as the indigenous flora and fauna.
                                </p>
                                <p class="content-element3">
                                    The scattered layout of the cottages, all individually styled to suit diverse
                                    tastes, give a sense of space and privacy. Though luxurious, the cottages are in
                                    sync with the forest feel and designed in an environment friendly and regional
                                    style, using local building materials and practices adopted by people in the
                                    neighboring villages. The dedication and love that has gone into the making of this
                                    handmade resort is amply visible from the moment you walk in, in way of the mud
                                    painted walls or the planting of thoughtfully chosen saplings that regenerate the
                                    forest around us.
                                </p>
                                <p class="content-element3">
                                    A seasonal river, ‘nullah’ that flows along the side of the Resort, forms a sangam
                                    point towards one end, where a large sandy patch strewn with rocks and boulders is
                                    ideal for barbecues and bonfires. This river is also a natural watering hole that
                                    lures smaller wildlife, however, for sightings of India’s famed Tigers, and
                                    specifically the Gaurs and Dhols, Mahua Vann provides excursions into the Pench
                                    National Park.
                                </p>
                                <p class="content-element3">
                                    Less than 10 minutes from the Pench Tiger Reserve, V Resorts Mahua Vann Pench is a
                                    boutique eco-resort spread across a large estate and located 88 km from the nearest
                                    city, Nagpur. The luxury resort houses rooms across three categories with each of
                                    the rooms tastefully decorated and equipped with modern amenities yet maintaining an
                                    earthen touch.
                                </p>
                                <p class="content-element3">
                                    With a rivulet crossing right through the resort, and plenty of lounging options on
                                    offer including nature walks, cycling tours, in-house swimming pool & spa, along
                                    with a library & entertainment room, one can be forgiven for unwinding more at the
                                    resort than in the wilderness that surrounds it. The rooms at the resort are spread
                                    out across categories with every block holding 1-4 rooms and located 20-50 meters
                                    from one another, connected via mud paths lined up with a range of diverse flora.
                                </p>

                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list" style="columns: 2;margin-bottom: 0;  -webkit-columns: 2;-moz-columns: 2;">
                                                <li>Village Walks</li>
                                                <li>Local Market</li>
                                                <li>BBQ</li>
                                                <li>Putting Greens</li>
                                                <li>Bullock Cart Rides</li>
                                                <li>Tours</li>
                                                <li>Safaris</li>
                                                <li>Swimming Pool & SPA</li>
                                                <li>Library</li>
                                                <li>Entertainment Room</li>
                                                <li>Cycling</li>
                                                <li>Yoga</li>
                                                <li >Pottery</li>


                                            </ul>


                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p> Pench National Park. Village- Kuppitola, Distt-Seoni, Madhya Pradesh,India</p>

                                        </div>
                                    </div>

                             <div class="row">
                             <div class="col-md-12">
                            <ul class="custom-list" style="">
                                                <li>Website: <a href="http://www.mahuaresorts.com/index.htm"
                                                                target="_blank" class="link-text">http://www.mahuaresorts.com/index.htm</a>
                                                </li>
                                            </ul>
                                </div>
                                </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'shathayu',
            'heading' => 'Shathayu - Bengaluru, India',
            'main_image' => '/images/locations/shathayu/soa3.jpg',
            'url' => 'https://shathayuretreat.com/',
            'image_prefix' => '/images/locations/shathayu/soa',
            'image_count' => 8,
            'body' => '                            <div>
                                <p class="content-element3">
                                    Shathayu plays a key role in promoting holistic wellness
                                    which encompasses five major dimensions of complete well-being – Mental, Physical,
                                    Spiritual, Social and Lifestyle. We offer complete health and wellness solutions
                                    under one roof. Our premium Ayurveda, Yoga and Naturopathy retreat with
                                    international standards offers a truly perennial dreamland like ambience overlooking
                                    a hill on 3 sides and a lake on the other.
                                </p>
                                <p class="content-element3">
                                    Blending the tradition and culture to take wellness pleasure in a lush green
                                    landscape we have designed 20 plus rooms along with yoga hall, amphitheatre,
                                    medicinal and spiritual garden. One can relax with our various Ayurvedic and
                                    Naturopathy therapies and revive yourself with Yoga.
                                </p>
                                <p class="content-element3">
                                    As we are at just a 25 minutes drive from the Bengaluru International Airport, it
                                    lessens your travel time sparing more time for health and happiness.
                                </p>
                                <p class="content-element3">
                                    With the tag line HEAL TOGETHER LIVE FULLER the concept of the Shathayu Retreat
                                    revolves around “Optimum Healthcare” in creating the kind of healthy society that we
                                    all yearn for. We strongly believe that being healthy extends beyond the realms of
                                    physical wellbeing. Our focus is on Optimum Health that encompasses physical,
                                    psychological and spiritual wellness.
                                </p>
                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list"
                                                style="columns: 2;margin-bottom: 0;  -webkit-columns: 2;-moz-columns: 2;">
                                                <li>Yoga & Naturopathy</li>
                                                <li>Various Wellness & Detox Programs</li>
                                                <li>Accommodation</li>
                                                <li>Corporate Wellness Programs</li>
                                                <li>Meditation</li>
                                                <li>Swimming</li>
                                                <li>Acupuncture</li>
                                                <li>Adventure</li>
                                                <li>Culinary Experiences</li>
                                                <li>Local Art & Photography</li>

                                            </ul>
                                            <ul class="custom-list" style="">
                                                <li>Website: <a href="https://shathayuretreat.com/"
                                                                target="_blank" class="link-text">https://shathayuretreat.com</a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>Shathayu Ayurveda Yoga Retreat, S.No.261, Avathi, Devanahalli, Bangalore
                                                – 562110, Karnataka, INDIA
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'devaaya',
            'heading' => 'Devaaya - Goa, India',
            'main_image' => '/images/locations/devaaya/dev25.jpg',
            'url' => 'http://www.devaaya.com',
            'image_prefix' => '/images/locations/devaaya/dev',
            'image_count' => 101,
            'body' => '                            <div>
                                <p class="content-element3">
                                    Devaaya, an Ayurveda and Nature Cure Centre, is situated on an island off the coast
                                    of Goa. Surrounded by the healing waters of the Mandovi river, this magical isle
                                    will immediately soothe your nerves and still your mind.
                                </p>
                                <p class="content-element3">
                                    Devaaya is brought to you by the trusted Alcon-Victor Group. Experienced in
                                    hospitality and healthcare with several properties in Goa like Novotel Dona Sylvia,
                                    Radisson, Viva, the Victor Hospital and the Victor Medical and Research Foundation.
                                    You are assured of the best service and quality healthcare from us.
                                </p>
                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list"
                                                style="columns: 2;margin-bottom: 0;  -webkit-columns: 2;-moz-columns: 2;">
                                                <li>Ayurvedic Treatments</li>
                                                <li>Various Wellness Packages</li>
                                                <li>Training Courses</li>
                                                <li>Naturopathy</li>
                                                <li>Detox & Rejuvenate</li>
                                                <li>Yoga & Meditation</li>
                                                <li>Group Classes & Workshops</li>
                                                <li>Accommodation</li>
                                                <li>Swimming Pool</li>
                                                <li>Gym & Physiotherapy</li>
                                                <li>Daily Activity Programs</li>
                                                <li>Dentistry</li>
                                                <li>Restaurants</li>

                                            </ul>
                                            <ul class="custom-list" style="">
                                                <li>Website: <a href="http://www.devaaya.com/"
                                                                target="_blank" class="link-text">http://www.devaaya.com</a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>Ayurveda & Nature Cure Centre Divar Island, Goa 403 403, India.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);
        \App\Location::create([
            'key' => 'kaivayla-yoga',
            'heading' => 'Kaivayla Yoga, Nepal',
            'main_image' => '/images/locations/kaivayla-yoga/nepal_main.jpg',
            'url' => 'https://vaidya-group.com/the-kaivalya-villas-and-retreat-kakani.html',
            'image_prefix' => '/images/locations/kaivayla-yoga/nepal',
            'image_count' => 10,
            'body' => '                            <div>
                                <p class="content-element3">
                                    KAIVALYA YOGA is established aiming to achieve Supreme Understanding spiritually. We
                                    are using total efforts to find the goal – ultimate realization. The word Kaivalya
                                    is ultimate realization as well, the wholeness. Kaivalya Yoga is created to balance
                                    life with as both legs walking; spiritual journey with worldly walks, does not
                                    matter what and wherever we are, synchronizing and connecting seeing that total
                                    being.
                                </p>
                                <p class="content-element3">
                                    Kaivalya Yoga is a company of very spiritual practitioners and Masters along this,
                                    well known spiritual and ritual researchers in different ethnical communities of
                                    Nepal.
                                </p>
                                <p class="content-element3">
                                    Kaivalya Yoga arranges and organizes Yoga and Meditation courses for beginners,
                                    intermediate and advanced practitioners separately with well known instructors
                                    experienced of 15-20 years.
                                </p>
                                <p class="content-element3">
                                    Also we are authorized, registered and experienced with Yoga Alliance US, every
                                    month we organize Hatha Yoga and Asthanga Yoga Teacher Training.
                                </p>
                                <p class="content-element3">
                                    We set Meditation classes for very spiritual seekers as Vipassana Meditation,
                                    Buddhist Meditation, Osho’s Meditation, Sanatana and Vedic Meditation.
                                </p>
                                <p class="content-element3">
                                    We are specialized in Nada Yoga, we have very special Professional Spiritual Sound
                                    Healing Courses in different levels as well Tibetan way of Sound Healing and Singing
                                    Bowl Courses.
                                </p>
                                <p class="content-element3">
                                    We have other unique programs as Traditional Usui Reiki Ryoho, Shamanic Tours,
                                    Cultural Tours and Researches.
                                </p>

                                <div class="content-element3">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">

                                            <h6 class="event-title">What We Offer</h6>

                                            <ul class="custom-list"
                                                style="columns: 2;margin-bottom: 0;  -webkit-columns: 2;-moz-columns: 2;">
                                                <li>Yoga</li>
                                                <li>Meditation</li>
                                                <li>Teacher Training</li>
                                                <li>Spiritual Tours</li>
                                                <li>Hiking</li>
                                                <li>Research,Study & Practice</li>
                                                <li>Philosophical Study</li>

                                            </ul>
                                            <ul class="custom-list" style="">
                                                <li>Website: <a href="https://vaidya-group.com/the-kaivalya-villas-and-retreat-kakani.html"
                                                                target="_blank"
                                                                class="link-text">https://vaidya-group.com/the-kaivalya-villas-and-retreat-kakani.html</a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-md-4 col-sm-12">

                                            <h6 class="event-title">Venue</h6>
                                            <p>KAIVALYA YOGA Kathmandu 44600</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
',
        ]);

//\App\Location::create([
        //            'key' => '',
        //            'heading' => '',
        //            'main_image' => '',
        //            'url' => '',
        //            'image_prefix' => '',
        //            'image_count' => 0,
        //            'body' => ''
        //        ]);

    }
}
