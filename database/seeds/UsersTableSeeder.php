<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'irfanmm96@gmail.com',
            'name' => 'Mohamed Irfan',
            'email_verified_at' => now(),
            'password' => bcrypt('secret')
        ]);
        User::create([
            'email' => 'fawzanm@gmail.com',
            'name' => 'Mohamed Fawzan',
            'email_verified_at' => now(),
            'password' => bcrypt('secret')
        ]);

    }
}
