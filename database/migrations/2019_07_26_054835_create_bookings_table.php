<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('booker_email');
            $table->unsignedInteger('quantity')->default(1);
            $table->string('user_id')->default(0);
            $table->string('program_id')->default(0);
            $table->string('extra_emails')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('customer_id');
            $table->string('charged_amount');
            $table->string('transaction_id');
            $table->string('receipt_url');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
