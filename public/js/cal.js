//where the magic happens
//strat date -> '11/15/2016 2:00pm'
//end date -> '11/15/2016 3:00pm'
function ical_download(event_title, file_name, start_date, end_date) {

    //name of event in iCal
    this.eventName = event_title;

    //name of file to download as
    this.fileName = file_name;

    //start time of event in iCal
    this.dateStart = new Date(start_date);

    //end time of event in iCal
    this.dateEnd = new Date(end_date);


    //helper functions

    //iso date for ical formats
    this._isofix = function (d) {
        var offset = ("0" + ((new Date()).getTimezoneOffset() / 60)).slice(-2);

        if (typeof d == 'string') {
            return d.replace(/\-/g, '') + 'T' + offset + '0000Z';
        } else {
            return d.getFullYear() + this._zp(d.getMonth() + 1) + this._zp(d.getDate()) + 'T' + this._zp(d.getHours()) + "0000Z";
        }
    }

    //zero padding for data fixes
    this._zp = function (s) {
        return ("0" + s).slice(-2);
    }
    this._save = function (fileURL) {
        if (!window.ActiveXObject) {
            var save = document.createElement('a');
            save.href = fileURL;
            save.target = '_blank';
            save.download = this.fileName || 'unknown';

            var evt = new MouseEvent('click', {
                'view': window,
                'bubbles': true,
                'cancelable': false
            });
            save.dispatchEvent(evt);

            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }

        // for IE < 11
        else if (!!window.ActiveXObject && document.execCommand) {
            var _window = window.open(fileURL, '_blank');
            _window.document.close();
            _window.document.execCommand('SaveAs', true, this.fileName || fileURL)
            _window.close();
        }
    }


    var now = new Date();
    // var start=new Date('Fri Jul 27 2019 08:00:00 GMT+0530');
    // var end=new Date('Fri Jul 29 2019 16:00:00 GMT+0530');
    // console.log(start);
    var ics_lines = [
        "BEGIN:VCALENDAR",
        "VERSION:2.0",
        "PRODID:-//Addroid Inc.//iCalAdUnit//EN",
        "METHOD:REQUEST",
        "BEGIN:VEVENT",
        "UID:event-" + now.getTime() + "@addroid.com",
        "DTSTAMP:" + this._isofix(now),
        "DTSTART:" + this._isofix(this.dateStart),
        "DTEND:" + this._isofix(this.dateEnd),
        "DESCRIPTION:" + this.eventName,
        "SUMMARY:" + this.eventName,
        "LAST-MODIFIED:" + this._isofix(now),
        "SEQUENCE:0",
        "END:VEVENT",
        "END:VCALENDAR"
    ];

    var dlurl = 'data:text/calendar;base64,' + btoa(ics_lines.join('\r\n'));

    try {
        this._save(dlurl);
    } catch (e) {
        console.log(e);
    }


}


//convertDate('11/15/2016 2:00pm');

function convertDate(date) {

    // # valid js Date and time object format (YYYY-MM-DDTHH:MM:SS)
    var dateTimeParts = date.split(' ');

    // # this assumes time format has NO SPACE between time and am/pm marks.
    if (dateTimeParts[1].indexOf(' ') == -1 && dateTimeParts[2] === undefined) {

        var theTime = dateTimeParts[1];

        // # strip out all except numbers and colon
        var ampm = theTime.replace(/[0-9:]/g, '');

        // # strip out all except letters (for AM/PM)
        var time = theTime.replace(/[[^a-zA-Z]/g, '');

        if (ampm == 'pm') {

            time = time.split(':');

            // # if time is 12:00, don't add 12
            if (time[0] == 12) {
                time = parseInt(time[0]) + ':' + time[1] + ':00';
            } else {
                time = parseInt(time[0]) + 12 + ':' + time[1] + ':00';
            }

        } else { // if AM

            time = time.split(':');

            // # if AM is less than 10 o'clock, add leading zero
            if (time[0] < 10) {
                time = '0' + time[0] + ':' + time[1] + ':00';
            } else {
                time = time[0] + ':' + time[1] + ':00';
            }
        }
    }
    // # create a new date object from only the date part
    var dateObj = new Date(dateTimeParts[0]);

    // # add leading zero to date of the month if less than 10
    var dayOfMonth = (dateObj.getDate() < 10 ? ("0" + dateObj.getDate()) : dateObj.getDate());

    // # parse each date object part and put all parts together
    var yearMoDay = dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1) + '-' + dayOfMonth;

    // # finally combine re-formatted date and re-formatted time!
    var date = new Date(yearMoDay + 'T' + time);

    return date;
}
